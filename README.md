# PeDViS-Simulator

This repository includes the PeDViS simulator package, along with the input and output files presented in Atamer Balkan et al. (2023).

### 1. Running the simulator

To run the simulator, follow these steps:

 1. Go to `input` folder and unzip `experiments.zip`. Choose the experiment setting you want to simulate (e.g., DCB-01, PYH-02, DCD-03) from the `experiments` folder, and copy the six input files in `input/experiments/XXX-NN` folder to `data` folder. To set an example, one set of input files already exists in `data` folder. More details about the experiments and input files are given in **2. Input files and experiments** section.

 2. (Optional) Compile the NOMAD C-files to improve the performance. To do this, install `gcc`. On Windows, [winlibs](http://winlibs.com/) is recommended (make sure to select the same architecture (x86 or x86_64) as the Python you are using). On Mac or Linux, `gcc` can be installed using a package manager (i.e. [brew](https://brew.sh/) on Mac, APT on Debian/Ubuntu, etc). To compile, go to `NOMAD/C` and run:
    ```
    gcc -c -Wall -Werror -fpic calcPedForces.c
    gcc -shared -o calcPedForces.so calcPedForces.o
    gcc -c -Wall -Werror -fpic convergeCostMatrix.c
    gcc -shared -o convergeCostMatrix.so convergeCostMatrix.o
    gcc -c -Wall -Werror -fpic getInteractionData.c
    gcc -shared -o getInteractionData.so getInteractionData.o
    ```
    **NOTE on Mac, replace `-shared` by `-dynamiclib`**, and run:

    ```
    gcc -c -Wall -Werror -fpic calcPedForces.c
    gcc -dynamiclib -o calcPedForces.dylib calcPedForces.o
    gcc -c -Wall -Werror -fpic convergeCostMatrix.c
    gcc -dynamiclib -o convergeCostMatrix.dylib convergeCostMatrix.o
    gcc -c -Wall -Werror -fpic getInteractionData.c
    gcc -dynamiclib -o getInteractionData.dylib getInteractionData.o
    ```

    You should now have several *.o and *.so/*.dylib files in that folder.

 3. You can now run `simulator.py`. The first parameter should be `local`, the second parameter is the number of replications for Pedestrian and Mobility model, the third parameter is the number of replications for the epidemiological model QVEmod, and the fourth parameter is the seed for the random number generator. To regenerate the same number of experiments in the article, the simulator should run with the following parameters:
    ```
    cd pedvis-simulator-main
    pip3 install -r requirements.txt
    python3 simulator.py local 5 24 1
    ```
 4. After some time, the data folder (`data`) should contain the output of the simulator. To set an example, one set of output files already exists in `data/001/001` folder. More details about the output files are given in **3. Output files** section.

### 2. Input files and experiments

Input files for each experiment setting in Atamer Balkan et al. (2023) are located in `input/experiments` folder.

The detailed settings of each experiment are provided in `experiment_settings.xlsx` file under `input/experiments` folder. The experiments with **experimentID**s starting with **PHY** represent the simulation experiments in section **3.1. Impact of Redesign of Physical Space**; those starting with **DCB** and **DCD** represent the simulation experiments in section **3.2. Impact of Service Duration and Service Capacity**; and those starting with **SSB** represent the simulation experiments in section **3.3. Impact of Service Schemes**.

Each experiment folder includes six different input files to run the simulator:
- `scenario.json` file includes the key settings of the simulation scenario: the expected number of agent groups that visit the venue, the expected duration of their visits, the coordinates of infrastructural elements in the venue (entrance, exit, toilet, tables, etc.), and other key settings such as whether there is a mask mandate in the venue or visitors comply physical distancing rules or not.
- `model_without_agents.json` file mainly acts a template containing the key parameters related to environment, and excluding the scripts of the agents. It includes the dimensions of the space, parameters related to interaction of the virus in the environment, and the specifications of fixtures and infrastructural elements in the venue.
- `infrastructure.xml`, `simulation_scenario.xml` and `pedestrian_parameter_sets.xml` are the key inputs of NOMAD; they include the detailed coordinates of the infrastructural elements in the venue, the parameters of the simulation scenario, and the parameters for the human movement in the venue, respectively.
- `config.ini` includes the user-defined parameter files that suppress the default parameter values in the model.

In order to run a particular simulation experiment, check the experiment details provided in `experiment_settings.xlsx` file, choose the experiment setting you want to simulate (e.g., DCB-01, PYH-02, DCD-03) from the `experiments` folder, and copy the six input files in `input/experiments/XXX-NN` folder to `data` folder, and follow the instructions in **1. Running the simulator** section.

### 3. Output files
Each simulation experiment creates a set of output files within the data folder (`data`), following the folder hierarchy of replications for the Pedestrian and Mobility model and the epidemiological model QVEmod, respectively. At the top level under the `data` folder, `totalRisk.csv` is created, showing the infection risk generated in each replication of that particular simulation experiment. In addition to the `totalRisk.csv` file at the top level, 12 replication-specific output files are created within separate folders for each replication. For example, the outputs in `data/001/001` folder represent the results of the 1st replication for the Pedestrian and Mobility model and 1st replication for the epidemiological model QVEmod. Among the output files created, the summary file `totalRisk.csv` at the top level and 4 replication-specific files are actively used in the analysis and visualization presented in Atamer Balkan et al. (2023):

- `totalRisk.csv` lists the infection risk of each replication in the corresponding simulation experiment setting. The values listed correspond to R(event) value of each replication (Atamer Balkan et al., 2023).
- `model.json` is the output of the Pedestrian and Mobility model and the input of the epidemiological model QVEmod, hence serves as a bridge between the two models. It includes the agent-based script (Figure 3, Atamer Balkan et al., 2023) and provides a detailed description of the simulation model. It keeps the entry and leave times of each agent, so the visit duration for E(Revenue) is calculated by using the scripts in `model.json`. It also encompasses environmental parameters like dimensions, decay rates, and air exchange rates. The script section defines agents with attributes such as viral load, emission rates, and scripted behaviors including movements, entries, and exits. The file also contains fixture information, detailing properties like transfer efficiency and surface characteristics.
- `agent_exposure.csv` shows the exposure rate to viral particles by each agent through aerosols and droplets (r(s)(inhalation-aerosols) and r(s)(inhalation-droplets)), and also accumulation of viral particles on agents' hands (V(s)(hand)) (Atamer Balkan et al., 2023).
- `agents.json` shows the distribution of the number of newly infected individuals in that particular replication of the simulation experiment.
- `groups.json` shows the percentage of infection risk covering the susceptible agents within the same sitting group of the infectious agent (w(event)(inside)) and the infection risk out of the group (w(event)(outside) = 1 - w(event)(inside)) (Atamer Balkan et al., 2023).

The output files that correspond to the experiment results presented in Atamer Balkan (2023) are provided in `results` directory. To investigate the results of each experiment, unzip `results/XXX-NN.zip` file. Following the similar folder hierarchy, for example, `results/DCB-01/001/001` folder includes the simulation run results of **DCB-01** experiment's 1st replication for the Pedestrian and Mobility model and 1st replication for the epidemiological model QVEmod. Recall that, for each simulation experiment, we generated 120 replications (5 for mobility behavior with the Pedestrian and Mobility model x 24 for the assignment of the infectious agent with the epidemiological model QVEmod) (Atamer Balkan et al., 2023).

The content of the other 8 output files that can be created by the simulator but not actively used in the analysis in Atamer Balkan et al. (2023) are listed below:
- `aerosol_contamination.csv`, `droplet_contamination.csv` and `surface_contamination.csv` show the contamination level of each grid cell (x,y) in aerosols, droplets and surfaces on each time step, which correspond to V_aerosols (x,y), V_droplets (x,y), and V_fomites (x,y) (Atamer Balkan et al., 2023).
- `aerosol_cumulative_exposure_differentials.csv`, `droplet_cumulative_exposure_differentials.csv` and `surface_cumulative_exposure_differentials.csv` show the non-zero contamination level of grid cells (x,y) at the end of the simulation run.
- `agent_exposure_with_positions.csv` provides the similar information with `agent_exposure.csv`, but also including the (x,y) positions of each agent.
- `trajectories.csv` file shows the location of each active agent for each time step of the Pedestrian and Mobility model, NOMAD.

### 4. Results and analysis

The results generated by simulation experiments are analyzed using `analysis/experiment_analysis.R`. This script first reads `model.json` files of simulation experiments, tracks the entry and leave times of each agent and calculates the visit duration, and then calculates E(Revenue) for each replication. Subsequently, the script reads the `totalRisk.csv` files for each experiment setting and creates the data frame `experimentSummary` summarizing the features of the experiment setting and the corresponding results. As the final output, the script creates `experimentAnalysisSummary.csv` file including both the infection risk R(event) and the expected revenue E(Revenue) for each replication.

The simulation results summarized in `experimentAnalysisSummary.csv` are visualized in Figures 7-12 in Atamer Balkan et al. (2023). 

### 5. Model adaptation

PeDViS can be used to characterize the infection risks in other types of indoor spaces with different human movement and behavior characteristics (Atamer Balkan et al., 2023). Up to now, the model has been adapted to events, festival terrains (Wang, 2021), office spaces (Deijkers, 2022), retail stores, supermarkets, changing rooms, locker rooms, bars, theaters, and different educational settings including classrooms. Customizing PeDViS for diverse indoor settings is indeed possible but not a straightforward plug-and-play process, and it necessitates some level of work in adjusting both the model inputs and the architecture. The model can also be adapted to different SARS-CoV-2 variants and other respiratory viruses by adjusting the model accordingly. Here, we present a non-exhaustive overview of relevant model sections, files and parameters for model adaptation.

**Model adaptation to different indoor settings:**
- The environment layout (defined in `scenario.json` and `infrastructure.xml` files) needs to be adjusted accordingly. For instance, when the model is adapted to a retail store setting rather than a restaurant, tables and chairs should be excluded from the model so that the visitors do not spend their time by sitting but walking within space. The store also needs to accommodate relevant infrastructural elements, mainly store racks as ‘obstacles’ so that neither the agents nor the viral particle can move through the racks.
- The activity choice and scheduling module requires larger adaptation when applied in different contexts. Currently, the model features a tailor-made activity choice and scheduling module for both customers and personnel in a restaurant environment. For any other context, one should follow either of the following two approaches:
   - Write a custom activity choice and scheduling module (based on an existing activity model or a newly created one) and add it to the simulator. This       enables dynamic activity scheduling and choice behaviour in the model. That is, pedestrian can update their schedule during the simulation based on the current state of the model. This is the more advanced option.
   - Create the activity schedules for each pedestrian by hand (on existing activity choice and scheduling model) and add these to the input files. This only allows for static behaviour. That is, the pedestrians follow their given schedule exactly during the simulation regardless of the circumstances.

Deijkers (2022) and Wang (2021) demonstrate the application of the first approach for office environments and festival grounds, respectively.

**Model adaptation to different SARS-CoV-2 variants and other respiratory viruses:**
- When required, the virus emission rate of the infectious individual can be adjusted by updating the `emissionRate` parameter in `simulator.py` file. As detailed in Atamer Balkan and Chang (2023), emission rate by the infectious agent is set to 10^6 viral particles per hour. To simulate a case where the infectious agent spreads viral particles with different emission rate, then the `emissionRate` parameter needs to be set to accordingly.
- For model adaptation, the route-specific exposure parameters (k_aerosols, k_droplets and k_fomites) may need to be adjusted accordingly. These parameters are based on the efficiency of each transmission route (c_aerosols, c_droplets, c_fomites, where k_route=D_inf/c_route and details on D_inf and c_route are provided in Atamer Balkan and Chang et al. (2023)). The c_route values for each transmission route correspond to `airProportion`, `dropletProportion` and `surfaceProportion` parameters in `simulator.py` file, and are all set to 0.1. In adapting the model to different respiratory viruses, `airProportion`, `dropletProportion` and `surfaceProportion` parameters can be readjusted considering the assumptions on the efficiency of different transmission routes.
- The functional form of the dose-response curve can be reconsidered when the model is adapted to other respiratory viruses. PeDViS currently uses an exponential dose-response function which is explicitly calculated by `risks` variable in `simulator.py` file, and can be adapted to any relevant functional form.
- Different types of viruses interact differently with their environment; therefore, virus spread-related parameters in decay and deposition processes need to be adjusted accordingly. In the input file `model_without_agents.json`, the parameter `decay_rate_air` represents the hourly decay rate of viral particles present in aerosols, `droplet_to_surface_transfer_rate` represents the hourly deposition rate of particles in droplets onto surfaces, and `surface_decay_rate`, defined for each infrastructural element, represents the hourly decay rate of viral particles on that particular surface type based on its material (Atamer Balkan & Chang et al., 2023). These parameters can be adjusted based on the respiratory virus under consideration.

### Reference article
Atamer Balkan, B., Sparnaaij, M., Duives, D., Huang, Y., & ten Bosch, Q. (2023). How to Operate an Indoor Venue During a Pandemic: Mitigating SARS-CoV-2 Infection Risk while Maintaining Viable Economic Activity (under review in European Journal of Operations Research)

### Other references
Atamer Balkan, B., Chang, Y., Sparnaaij, M., Wouda, B., Boschma, D., Liu, Y., ... & ten Bosch, Q. (2023). The multi-dimensional challenges of controlling respiratory virus transmission in indoor spaces: Insights from the linkage of a microscopic pedestrian simulation and SARS-CoV-2 transmission model. medRxiv. https://doi.org/10.1101/2021.04.12.21255349.

Deijkers, E. (2022). Comparing various measures to prevent covid-19 spread in offices through simulation modelling (Master thesis, Delft University of Technology). http://resolver.tudelft.nl/uuid:6f11dc81-0260-4dd4-bb98-cd74275a3c42.

Wang, X. (2021). Quantifying transmission risks of SARS-CoV-2 in pedestrian interactions at large events (Master thesis, Delft University of Technology). http://resolver.tudelft.nl/uuid:2b4d3e48-28da-4e1f-bb21-0c828d99bd26

