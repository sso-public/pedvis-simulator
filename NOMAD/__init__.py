
import ctypes
from pathlib import Path
import platform


_package_path = Path(__file__).parent.resolve()
_c_path = _package_path.joinpath('C')

if platform.system() == 'Darwin':
    ext = 'dylib'
else:
    ext = 'so'

getInteractionData_so_file = _c_path.joinpath('getInteractionData.' + ext)
calcPedForces_so_file = _c_path.joinpath('calcPedForces.' + ext)
convergeCostMatrix_so_file = _c_path.joinpath('convergeCostMatrix.' + ext)

try:
    ctypes.CDLL(str(getInteractionData_so_file))
    canUseGetInteractionCompiledCode = True
except Exception as err:
    print("WARN: not using compiled C class getInteractionData: {0}".format(err))
    canUseGetInteractionCompiledCode = False

try:
    ctypes.CDLL(str(calcPedForces_so_file))
    canUseCalcPedForcesCompiledCode = True
except Exception as err:
    print("WARN: not using compiled C class calcPedForces: {0}".format(err))
    canUseCalcPedForcesCompiledCode = False

try:
    ctypes.CDLL(str(convergeCostMatrix_so_file))
    canUseConvergeCostMatrixCompiledCode = True
except Exception as err:
    print("WARN: not using compiled C class convergeCostMatrix: {0}".format(err))
    canUseConvergeCostMatrixCompiledCode = False

import numpy as np
NOMAD_RNG = np.random.default_rng()
