""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

from _collections_abc import dict_values
from copy import copy
from math import sqrt

from shapely.geometry.linestring import LineString
from shapely.geometry.multilinestring import MultiLineString
from shapely.geometry.point import Point

import NOMAD
from NOMAD.shapely_subclass import ShapelyGeomClassWrapper
import numpy as np


def doListCeck(objectList):
    if isinstance(objectList, dict):
        objectList = list(objectList.values())
    elif isinstance(objectList, dict_values):
        objectList = list(objectList)
    elif not isinstance(objectList, (list, tuple)):
        objectList = [objectList,]
    if isinstance(objectList, tuple):
        objectList = list(objectList)

    return objectList

# ==============================================================================================
# ==============================================================================================

class SpawnableLine(ShapelyGeomClassWrapper): 
    
    def __init__(self, ID, lineCoords):
        ShapelyGeomClassWrapper.__init__(self, ID)
        if not isSimpleLine(lineCoords):
            raise ValueError('A line source can only be a simple line which can be described by the equation y = ax + b!')

        self._geometry = LineString(lineCoords)
        
        self.lineGeometry = LineString(lineCoords)
        self.baseLineSegments = []
        self.lineBase_x = lineCoords[0][0]
        self.lineBase_y = lineCoords[0][1]
        self.lineVec_x = lineCoords[1][0] - lineCoords[0][0]
        self.lineVec_y = lineCoords[1][1] - lineCoords[0][1]
        self.lineLength2 = self.lineVec_x*self.lineVec_x + self.lineVec_y*self.lineVec_y
  
    def createBaseLineSegments(self, parameters, gridManager, walkLevelID):
        self._maxPedRadius = parameters.MAX_PED_RADIUS
        self._maxPedRadius2 = parameters.MAX_PED_RADIUS2
        self._maxPedDiameter = parameters.MAX_PED_DIAMETER
        
        bufferedLine = self._geometry.buffer(self._maxPedRadius)
        obstacles2check = []
        obstaclesChecked = []
        gridCells = gridManager.getGridCellsCoveringPolygon(bufferedLine, walkLevelID)
        for gridCell in gridCells:
            for obstacle in gridCell.obstaclesInCell:
                if obstacle in obstaclesChecked:
                    continue
                obstaclesChecked.append(obstacle)
                if obstacle.within(bufferedLine) or obstacle.intersects(bufferedLine):
                    obstacles2check.append(obstacle)

        for obstacle in obstacles2check:
            bufferedObstacle = obstacle.geometry.buffer(self._maxPedRadius)
            self.lineGeometry = self.lineGeometry.difference(bufferedObstacle)

        if self.lineGeometry.is_empty:
            raise Exception('The source {} has no valid points to spawn pedestrians!'.format(self.ID))

        if isinstance(self.lineGeometry, MultiLineString):
            baseLineSegments = list(self.lineGeometry)
        else:
            baseLineSegments = [self.lineGeometry]

        for lineSegment in baseLineSegments:
            _, _, buffer_x, buffer_y = getLineVectorAndBuffer(lineSegment, self._maxPedRadius)
            x_0 = lineSegment.coords[0][0] - buffer_x
            x_1 = lineSegment.coords[1][0] + buffer_x
            y_0 = lineSegment.coords[0][1] - buffer_y
            y_1 = lineSegment.coords[1][1] + buffer_y
            self.baseLineSegments.append(LineString(((x_0, y_0), (x_1, y_1))))        

    def getAvailableLineSegments(self, maxPedRadius, maxPedRadius2, maxPedDiameter):
        lineSegments = copy(self.baseLineSegments)
        for gridCell in self.gridCellsInNeighborhood:
            for ped in gridCell.pedsInCell:
                minDist2 = getMinDist2Line2(ped.pos_x, ped.pos_y, self.lineBase_x, self.lineBase_y, self.lineVec_x, self.lineVec_y, self.lineLength2)
                if minDist2 >= maxPedRadius2:
                    continue

                newLineSegments = []
                pedGeom = Point((ped.pos_x, ped.pos_y)).buffer(ped.radius)
                for lineSegment in lineSegments:
                    lineSegmentDiff = lineSegment.difference(pedGeom)
                    if lineSegmentDiff == lineSegment:
                        newLineSegments.append(lineSegment)
                        continue
                    if isinstance(lineSegmentDiff, LineString):
                        lineSegmentDiff = [lineSegmentDiff]
                    for lineSegementDiffPart in lineSegmentDiff:
                        if lineSegementDiffPart.length >= maxPedDiameter:
                            newLineSegments.append(lineSegementDiffPart)

                lineSegments = newLineSegments

        segmentCount = len(lineSegments)
        lineSegmentArray = np.zeros((segmentCount, 6), dtype=float)

        # Crop lines and put them in the array
        for ii in range(segmentCount):
            lineVec_x, lineVec_y, buffer_x, buffer_y = getLineVectorAndBuffer(lineSegments[ii], maxPedRadius)

            lineSegmentArray[ii,0] = lineSegments[ii].coords[0][0] + buffer_x
            lineSegmentArray[ii,1] = lineSegments[ii].coords[0][1] + buffer_y
            lineSegmentArray[ii,2] = lineVec_x - 2*buffer_x
            lineSegmentArray[ii,3] = lineVec_y - 2*buffer_y
            lineSegmentArray[ii,4] = lineSegments[ii].length - maxPedDiameter
            lineSegmentArray[ii,5] = lineSegments[ii].length/maxPedDiameter

        returnInd = np.arange(segmentCount)
        NOMAD.NOMAD_RNG.shuffle(returnInd)
        return lineSegmentArray[returnInd,:]
    
    def getPlacementLocations(self, ped2placeCount):
        lineSegmentArray = self.getAvailableLineSegments(self._maxPedRadius, self._maxPedRadius2, self._maxPedDiameter)
        availableLocationCount = int(np.sum(np.floor(lineSegmentArray[:,5])))
        maxPeds2placeCount = min(availableLocationCount, ped2placeCount)

        locations = self.getPlacementLocationsInSegments(lineSegmentArray, maxPeds2placeCount, self._maxPedDiameter)

        return locations, maxPeds2placeCount
    
    def getRandomPlacementLocationsInSegments(self, lineSegmentArray, placementCount, maxPedDiameter):
        lineSegmentCount = lineSegmentArray.shape[0]
        pedsPerSegment = np.zeros(lineSegmentCount, dtype=int)
        if lineSegmentCount == 1:
            pedsPerSegment[0] = placementCount
        else:
            spacePerPedPerSegement = lineSegmentArray[:,4].copy()
            for _ in range(placementCount):
                segmentInd = np.argmax(spacePerPedPerSegement)
                pedsPerSegment[segmentInd] += 1
                spacePerPedPerSegement[segmentInd] = (lineSegmentArray[segmentInd,4] - pedsPerSegment[segmentInd]*maxPedDiameter)/(pedsPerSegment[segmentInd] + 1)

        locations = []
        for ii in range(lineSegmentCount):
            if pedsPerSegment[ii] == 1:
                locations.append(getRandomLocOnLineSegment(lineSegmentArray[ii,0], lineSegmentArray[ii,1], lineSegmentArray[ii,2], lineSegmentArray[ii,3]))
            elif pedsPerSegment[ii] > 1:
                effectiveLengthFactor = (lineSegmentArray[ii,4] - maxPedDiameter*(pedsPerSegment[ii] - 1))/lineSegmentArray[ii,4]
                lineVec_x = lineSegmentArray[ii,2]*effectiveLengthFactor/pedsPerSegment[ii]
                lineVec_y = lineSegmentArray[ii,3]*effectiveLengthFactor/pedsPerSegment[ii]

                maxDiameterfactor = maxPedDiameter/lineSegmentArray[ii,4]
                maxPedDiameter_x = maxDiameterfactor*lineSegmentArray[ii,2]
                maxPedDiameter_y = maxDiameterfactor*lineSegmentArray[ii,3]

                lineBase_x = lineSegmentArray[ii,0]
                lineBase_y = lineSegmentArray[ii,1]

                for _ in range(pedsPerSegment[ii]):
                    locations.append(getRandomLocOnLineSegment(lineBase_x, lineBase_y, lineVec_x, lineVec_y))
                    lineBase_x += lineVec_x + maxPedDiameter_x
                    lineBase_y += lineVec_y + maxPedDiameter_y

        return locations
    
# ==============================================================================================

def isSimpleLine(coords):
    if len(coords) != 2:
        return False

    return True

def getLineVectorAndBuffer(lineSegment, bufferSize):
    lineVec_x = lineSegment.coords[1][0] - lineSegment.coords[0][0]
    lineVec_y = lineSegment.coords[1][1] - lineSegment.coords[0][1]

    buffer_x = lineVec_x*bufferSize/lineSegment.length
    buffer_y = lineVec_y*bufferSize/lineSegment.length
    
    return lineVec_x, lineVec_y, buffer_x, buffer_y

def polygonIsRectangle(coords):
    if not (len(coords) == 4 or (len(coords) == 5 and coords[0] == coords[-1])):
        return False

    sideLengths = []
    pointIndices = [0,1,2,3,0]
    for ii in range(4):
        p1 = coords[pointIndices[ii]]
        p2 = coords[pointIndices[ii+1]]
        sideLengths.append(sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2))

    return abs(sideLengths[0] - sideLengths[2]) < 1e-10 and abs(sideLengths[1] - sideLengths[3]) < 1e-6

def getRectangleFromLine(lineCoords, buffer, sidesTouchObstacles=(False, False)):
    pass

def getRandomLocOnLineSegment(lineBase_x, lineBase_y, lineVec_x, lineVec_y):
    multiFactor = NOMAD.NOMAD_RNG.uniform()
    return (lineBase_x + lineVec_x*multiFactor, lineBase_y + lineVec_y*multiFactor)

def getMinDist2Line(point_x, point_y, lineBase_x, lineBase_y, lineVec_x, lineVec_y, lineLength2):
    return sqrt(getMinDist2Line2(point_x, point_y, lineBase_x, lineBase_y, lineVec_x, lineVec_y, lineLength2))

def getMinDist2Line2(point_x, point_y, lineBase_x, lineBase_y, lineVec_x, lineVec_y, lineLength2):
    closestPoint_x, closestPoint_y = getClosestPointOnLine(point_x, point_y, lineBase_x, lineBase_y, lineVec_x, lineVec_y, lineLength2)

    return (point_x - closestPoint_x)**2 + (point_y - closestPoint_y)**2

def getClosestPointOnLine(point_x, point_y, lineBase_x, lineBase_y, lineVec_x, lineVec_y, lineLength2):
    t0 = (lineVec_x*(point_x - lineBase_x) + lineVec_y*(point_y - lineBase_y))/lineLength2
    t0 = 0 if t0 < 0 else t0
    t0 = 1 if t0 > 1 else t0

    return lineBase_x + lineVec_x*t0, lineBase_y + lineVec_y*t0

def getAnalyticalMean(numpyDistr, numpyDistrArgs):
    rg = NOMAD.NOMAD_RNG
    if isDistrEqualTo(numpyDistr, rg.normal) or \
        isDistrEqualTo(numpyDistr, rg.lognormal):
        if len(numpyDistrArgs):
            return numpyDistrArgs[0]
        else:
            return 0 # Default mean as defined in numpy
    
    if isDistrEqualTo(numpyDistr, rg.poisson):
        if len(numpyDistrArgs):
            return numpyDistrArgs[0]
        else:
            return 1.0 # Default mean as defined in numpy
    
    if isDistrEqualTo(numpyDistr, rg.triangular):
        return (numpyDistrArgs[0] + numpyDistrArgs[1] + numpyDistrArgs[2])/3
    
    if isDistrEqualTo(numpyDistr, rg.uniform):
        low = 0 # # Default as defined in numpy
        high = 1 # # Default as defined in numpy
        
        if len(numpyDistrArgs):
            low = numpyDistrArgs[0]
            if len(numpyDistrArgs) > 1:
                high = numpyDistrArgs[1]
        return (low + high)/2
    
    raise Exception(f'The "{numpyDistr.__name__}" distribution is not supported!')

def isDistrEqualTo(distr, distrOther):
    return distr.__name__ == distrOther.__name__
    