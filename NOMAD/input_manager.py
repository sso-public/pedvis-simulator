""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

from copy import copy
from dataclasses import dataclass, asdict
from math import inf
from typing import List

from NOMAD.activities import LineDestination, PolygonDestination, Sink, Waypoint, \
    PointDestination, CentroidPolygonDestination, FixedWaitingTimeActivity, \
    FixedWaitingTimeNonInteractingActivity, EndAtTimeWaitingActivity, \
    ListActivityPattern, SpawnableLineDestination
from NOMAD.activity_scheduler import SERVE_ACTIVITY_COUNT, createRestaurantScheduler
from NOMAD.constants import GRID_CELL_SIZE_NM, PED_MANAGER_NM, ACC_CALC_FCN_NM, \
    PED_CLASS_NM
from NOMAD.demand_manager import DemandManager, RectangleSource, \
    ConstantDemandPattern, VariableDemandPattern, SimpleRandonLineSource, \
    DiscreteDemandPattern
from NOMAD.nomad_model import NomadModel, createNomadModel
from NOMAD.nomad_pedestrian_parameters import PedParameterSet
from NOMAD.obstacles import LineObstacle, PolygonObstacle, CircleObstacle, \
    EllipseObstacle
from NOMAD.output_manager import BasicFileOutputManager, InMemoryOutputManager, \
    DummyOutputManager, DebugOutputManager
from NOMAD.walk_level import WalkLevel, WalkableArea


# ===============================================================================
# The data classes that form the basis of the input for NOMAD
# Any input structure should create these data classes and from these data classes
# create the NomadModel instance
# ===============================================================================
@dataclass
class DistrValueInput():
    value: float = None
    min: float = None
    max: float = None
    numpyDistr: callable = None
    numpyDistrArgs: tuple = None

# ===============================================================================
# ===============================================================================
@dataclass
class WalkableAreaInput():
    ID: str
    coords: tuple

def createWalkableAreas(walkLevelInput):
    walkableAreas = []
    for walkableAreaInput in walkLevelInput.walkableAreas:
            walkableAreas.append(WalkableArea(walkableAreaInput.ID, walkableAreaInput.coords))

    return walkableAreas

# ===============================================================================
# ===============================================================================
@dataclass
class ObstacleInput():
    ID: str
    seeThrough: bool

def createObstacles(walkLevelInput):
    obstacles = []
    for obstacleInput in walkLevelInput.obstacles:
        obstacles.append(OBS_CONFIG_CLASS_2_CREATION_FCN[obstacleInput.__class__](obstacleInput))

    return obstacles

# ===============================================================================
@dataclass
class LineObstacleInput(ObstacleInput):
    coords: tuple

def createLineObstacle(obstacleInput):
    return LineObstacle(obstacleInput.ID, obstacleInput.coords, obstacleInput.seeThrough)

# ===============================================================================
@dataclass
class PolygonObstacleInput(ObstacleInput):
    coords: tuple

def createPolygonObstacle(obstacleInput):
    return PolygonObstacle(obstacleInput.ID, obstacleInput.coords, obstacleInput.seeThrough)

# ===============================================================================
@dataclass
class CircleObstacleInput(ObstacleInput):
    centerCoord: tuple
    radius: float

def createCircleObstacle(obstacleInput):
    return CircleObstacle(obstacleInput.ID, obstacleInput.centerCoord, obstacleInput.radius, obstacleInput.seeThrough)

# ===============================================================================
@dataclass
class EllipseObstacleInput(ObstacleInput):
    centerCoord: tuple
    semiAxesValues: tuple

def createEllipseObstacle(obstacleInput):
    return EllipseObstacle(obstacleInput.ID, obstacleInput.centerCoord, obstacleInput.semiAxesValues, obstacleInput.seeThrough)

# ===============================================================================
OBS_CONFIG_CLASS_2_CREATION_FCN = {
    LineObstacleInput: createLineObstacle,
    PolygonObstacleInput: createPolygonObstacle,
    CircleObstacleInput: createCircleObstacle,
    EllipseObstacleInput: createEllipseObstacle
    }

# ===============================================================================
# ===============================================================================
@dataclass
class DestinationInput():
    ID: str
    coords: tuple
    groupID: str
    isVirtualObstacle: bool = False

def createDestinations(walkLevelInput):
    destinations = []
    for destinationInput in walkLevelInput.destinations:
        destinations.append(DESTINATION_CONFIG_CLASS_2_CREATION_FCN[destinationInput.__class__](destinationInput))

    return destinations

# ===============================================================================
@dataclass
class LineDestinationInput(DestinationInput): pass

def createLineDestination(destinationInput):
    return LineDestination(destinationInput.ID, destinationInput.coords,
                           groupID=destinationInput.groupID,
                           isVirtualObstacle=destinationInput.isVirtualObstacle)

# ===============================================================================
@dataclass
class PointDestinationInput(DestinationInput): pass

def createPointDestination(destinationInput):
    return PointDestination(destinationInput.ID, destinationInput.coords,
                           groupID=destinationInput.groupID,
                           isVirtualObstacle=destinationInput.isVirtualObstacle)

# ===============================================================================
@dataclass
class PolygonDestinationInput(DestinationInput): pass

def createPolygonDestination(destinationInput):
    return PolygonDestination(destinationInput.ID, destinationInput.coords,
                           groupID=destinationInput.groupID,
                           isVirtualObstacle=destinationInput.isVirtualObstacle)

# ===============================================================================
@dataclass
class CentroidPolygonDestinationInput(DestinationInput): pass

def createCentroidPolygonDestination(destinationInput):
    return CentroidPolygonDestination(destinationInput.ID, destinationInput.coords,
                           groupID=destinationInput.groupID,
                           isVirtualObstacle=destinationInput.isVirtualObstacle)
    
# ===============================================================================
@dataclass
class SpawnableLineDestinationInput(DestinationInput): pass

def createSpawnableLineDestination(destinationInput):
    return SpawnableLineDestination(destinationInput.ID, destinationInput.coords,
                           groupID=destinationInput.groupID,
                           isVirtualObstacle=destinationInput.isVirtualObstacle)
    
# ===============================================================================
DESTINATION_CONFIG_CLASS_2_CREATION_FCN = {
    LineDestinationInput: createLineDestination,
    PointDestinationInput: createPointDestination,
    PolygonDestinationInput: createPolygonDestination,
    CentroidPolygonDestinationInput: createCentroidPolygonDestination,
    SpawnableLineDestinationInput: createSpawnableLineDestination,
    }

# ===============================================================================
# ===============================================================================
@dataclass
class SourceInput():
    ID: str
    coords: tuple

def createSources(walkLevelInputs, walkLevels):
    sourcesPerWalkLevel = {}
    sources = {}
    for walkLevelInput in walkLevelInputs:
        sourcesPerWalkLevel[walkLevels[walkLevelInput.ID]] = []
        for sourceInput in walkLevelInput.sources:
            source = SOURCE_CONFIG_CLASS_2_CREATION_FCN[sourceInput.__class__](sourceInput)
            sourcesPerWalkLevel[walkLevels[walkLevelInput.ID]].append(source)
            sources[source.ID] = source

    return sources, sourcesPerWalkLevel

# ===============================================================================
@dataclass
class SimpleRandomLineSourceInput(SourceInput): pass

def createSimpleRandomLineSource(sourceInput):
    return SimpleRandonLineSource(sourceInput.ID, sourceInput.coords)

# ===============================================================================
@dataclass
class RectangleSourceInput(SourceInput):
    directionVec: tuple

def createRectangleSource(sourceInput):
    return RectangleSource(sourceInput.ID, sourceInput.coords, sourceInput.directionVec)

# ===============================================================================
SOURCE_CONFIG_CLASS_2_CREATION_FCN = {
    SimpleRandomLineSourceInput: createSimpleRandomLineSource,
    RectangleSourceInput: createRectangleSource
    }

# ===============================================================================
# ===============================================================================
@dataclass
class WalkLevelInput():
    ID: str
    walkableAreas: List[WalkableAreaInput]
    obstacles: List[ObstacleInput]
    destinations: List[DestinationInput]
    sources: List[SourceInput]

def createWalkLevels(walkLevelInputs):
    walkLevels = {}
    destinations = {}
    for walkLevelInput in walkLevelInputs:
        walkLevel = WalkLevel(walkLevelInput.ID)
        walkLevel.addWalkableAreas(createWalkableAreas(walkLevelInput))
        walkLevel.addObstacles(createObstacles(walkLevelInput))
        destinationsWalkLevel = createDestinations(walkLevelInput)
        for destination in destinationsWalkLevel:
            destinations[destination.ID] = destination

        walkLevel.addDestinations(destinationsWalkLevel)
        walkLevels[walkLevelInput.ID] = walkLevel

    return walkLevels, destinations

# ===============================================================================
# ===============================================================================
# ===============================================================================
# ===============================================================================

@dataclass
class ActivityInput():
    ID: str
    destinationID: str
    groupID: str

def createActivities(activityInputs, destinations):
    activities = {}
    for activityInput in activityInputs:
        activity = ACTIVITY_CONFIG_CLASS_2_CREATION_FCN[activityInput.__class__](activityInput, destinations)
        activities[activity.ID] = activity

    return activities

# ===============================================================================
@dataclass
class SinkInput(ActivityInput): pass

def createSink(activityInput, destinations):
    return Sink(activityInput.ID, destinations[activityInput.destinationID], groupID=activityInput.groupID)

# ===============================================================================
@dataclass
class WaypointInput(ActivityInput): pass

def createWaypoint(activityInput, destinations):
    return Waypoint(activityInput.ID, destinations[activityInput.destinationID], groupID=activityInput.groupID)

# ===============================================================================
@dataclass
class FixedWaitingTimeActivityInput(ActivityInput):
    name: str
    duration: float

def createFixedWaitingTimeActivity(activityInput, destinations):
    return FixedWaitingTimeActivity(activityInput.ID, destinations[activityInput.destinationID], activityInput.name, activityInput.duration, groupID=activityInput.groupID)

# ===============================================================================
@dataclass
class FixedWaitingTimeNonInteractingActivityInput(FixedWaitingTimeActivityInput): pass

def createFixedWaitingTimeNonInteractingActivity(activityInput, destinations):
    return FixedWaitingTimeNonInteractingActivity(activityInput.ID, destinations[activityInput.destinationID], activityInput.name, activityInput.duration, groupID=activityInput.groupID)

# ===============================================================================
@dataclass
class EndAtTimeWaitingActivityInput(ActivityInput):
    name: str
    endTime: float

def createEndAtTimeWaitingActivity(activityInput, destinations):
    return EndAtTimeWaitingActivity(activityInput.ID, destinations[activityInput.destinationID], activityInput.name, activityInput.endTime, groupID=activityInput.groupID)

# ===============================================================================
ACTIVITY_CONFIG_CLASS_2_CREATION_FCN = {
    SinkInput: createSink,
    WaypointInput: createWaypoint,
    FixedWaitingTimeActivityInput: createFixedWaitingTimeActivity
    }

# ===============================================================================
# ===============================================================================
@dataclass
class ActivityPatternInput():
    ID: str

def createActivityPatterns(activityPatternInputs, activities):
    activityPatterns = {}

    for activityPatternInput in activityPatternInputs:
        ID = activityPatternInput.ID
        
        activityPatterns[ID] = ACTIVITY_PATTERN_CONFIG_CLASS_2_CREATION_FCN[activityPatternInput.__class__](ID, activities, activityPatternInput)

    return activityPatterns

# ===============================================================================
@dataclass
class ListActivityPatternInput(ActivityPatternInput):
    activityIDs: tuple
    
def createListActivityPattern(ID, activities, activityPatternInput):
        activityList = []
        for activityID in activityPatternInput.activityIDs:
            activityList.append(activities[activityID])
            
        return ListActivityPattern(ID, activityList)
    
# ===============================================================================    
ACTIVITY_PATTERN_CONFIG_CLASS_2_CREATION_FCN = {
    ListActivityPatternInput: createListActivityPattern,
    }    

# ===============================================================================
# ===============================================================================
@dataclass
class DemandPatternInput():
    ID: str
    sourceID: str
    activityPatternID: str
    parameterSetID: str

def createDemandPatterns(demandPatternInputs, pedParameterSets, activityPatterns, sources):
    demandPatterns = {}

    for demandPatternInput in demandPatternInputs:
        ID = demandPatternInput.ID
        creationFcn, addArgs, addKwargs = DEMAND_PATTERN_CONFIG_CLASS_2_CREATION_FCN[demandPatternInput.__class__](demandPatternInput)

        source = sources[demandPatternInput.sourceID]
        pedParamSet = pedParameterSets[demandPatternInput.parameterSetID]
        activityPattern = activityPatterns[demandPatternInput.activityPatternID]

        demandPatterns[ID] = creationFcn(ID, source, pedParamSet, activityPattern,
                                         *addArgs, **addKwargs)

    return demandPatterns

# ===============================================================================
@dataclass
class ConstantDemandPatternInput(DemandPatternInput):
    flowPerSecond: float
    startTime: float = 0
    endTime: float = inf

def getConstantDemandPatternInfo(demandPatternInput):
    addArgs = (demandPatternInput.flowPerSecond, )
    addKwargs = {
        'startTime':demandPatternInput.startTime,
        'endTime':demandPatternInput.endTime}

    return ConstantDemandPattern, addArgs, addKwargs

# ===============================================================================
@dataclass
class VariableDemandPatternInput(DemandPatternInput):
    timeFlowArray: tuple
    startTime: float = 0
    endTime: float = inf

def getVariableDemandPatternInfo(demandPatternInput):
    addArgs = (demandPatternInput.timeFlowArray, )
    addKwargs = {
        'startTime':demandPatternInput.startTime,
        'endTime':demandPatternInput.endTime}

    return VariableDemandPattern, addArgs, addKwargs

# ===============================================================================
@dataclass
class DiscreteDemandPatternInput(DemandPatternInput):
    discretePattern: tuple

def getDiscreteDemandPatternInfo(demandPatternInputs):
    addArgs = (demandPatternInputs.discretePattern, )
    addKwargs = {}

    return DiscreteDemandPattern, addArgs, addKwargs

# ===============================================================================
DEMAND_PATTERN_CONFIG_CLASS_2_CREATION_FCN = {
    ConstantDemandPatternInput: getConstantDemandPatternInfo,
    VariableDemandPatternInput: getVariableDemandPatternInfo,
    DiscreteDemandPatternInput: getDiscreteDemandPatternInfo,
    }

# ===============================================================================
# ===============================================================================
@dataclass
class SchedulerInput():
    sourceID: str
    sinkID: str

# ===============================================================================
@dataclass
class RestaurantStaffSchedulerInput():
    baseAreaID: str
    staffCount: int
    pedParamSetID: str
    servingsCount: int = SERVE_ACTIVITY_COUNT
    neighborhoods: tuple = ()

@dataclass
class RestaurantSchedulerInput(SchedulerInput):
    demandPattern: tuple
    sittingDestinationGroupIDs: tuple
    visitDurationDistr: DistrValueInput
    inGroupEntryDistr: DistrValueInput
    guestPedParamSetDistr: tuple
    useEntranceTimeSlot: bool
    entranceTimeSlotDuration: float = None
    toiletDestinationIDs: tuple = None
    toiletVisitProbability: float = None
    toiletVisitDurationDistr: DistrValueInput = None
    coatRackDestinationID: str = None
    coatRackVisitDurationDistr: DistrValueInput = None
    registerDestinationID: str = None
    registerVisitDurationDistr: DistrValueInput = None
    useTablesOnlyOnce: bool = False
    staffScheduler: RestaurantStaffSchedulerInput = None

# ===============================================================================
# ===============================================================================
@dataclass
class DemandManagerInput():
    activities: List[ActivityInput]
    activityPatterns: List[ActivityPatternInput] = None
    demandPatterns: List[DemandPatternInput] = None
    scheduler: SchedulerInput = None

def createDemandManager(simulationInput, walkLevels, destinations):
    activities = createActivities(simulationInput.demandManager.activities, destinations)

    demandManager = DemandManager(activities)

    pedParameterSets = createPedParameterSets(simulationInput.pedParameterSets)
    sources, sourcesPerWalkLevel = createSources(simulationInput.walkLevels, walkLevels)
    for walkLevel, sourcesList in sourcesPerWalkLevel.items():
        demandManager.addSources(sourcesList, walkLevel)
        walkLevel.addSources(sourcesList)

    if isinstance(simulationInput.demandManager.scheduler, RestaurantSchedulerInput): 
        schedulerInput = simulationInput.demandManager.scheduler
        scheduler = createRestaurantScheduler(schedulerInput, destinations, pedParameterSets, sources, activities, walkLevels)
        scheduler.initialize()
        demandPatterns = scheduler.createDemandPatterns()
        if scheduler.staffDynamicScheduler is not None:
            demandManager.dynamicSchedulers.append(scheduler.staffDynamicScheduler)
    elif simulationInput.demandManager.scheduler is None:
        activityPatterns = createActivityPatterns(simulationInput.demandManager.activityPatterns, activities)
        demandPatterns = createDemandPatterns(simulationInput.demandManager.demandPatterns, pedParameterSets, activityPatterns, sources)
    else:
        raise Exception('Unknown scheduler')
        
    demandManager.addDemandPatterns(demandPatterns)

    return demandManager

# ===============================================================================
# ===============================================================================
@dataclass
class OutputManagerInput(): pass

@dataclass
class BasicFileOutputManagerInput(OutputManagerInput):
    outputPd: str
    baseFilename: str
    bufferSize: int = None

@dataclass
class DebugOutputManagerInput(BasicFileOutputManagerInput): pass

@dataclass
class InMemoryOutputManagerInput(OutputManagerInput):
    saveTimeStep: float = None

@dataclass
class DummyOutputManagerInput(OutputManagerInput): pass

def createOutputManager(simulationInput):
    if isinstance(simulationInput.outputManager, (BasicFileOutputManagerInput, DebugOutputManagerInput)):
        outputPd = simulationInput.outputManager.outputPd
        baseFilename = simulationInput.outputManager.baseFilename
        if simulationInput.outputManager.bufferSize is not None:
            addArgs = (simulationInput.outputManager.bufferSize, )
        else:
            addArgs = tuple()

        if isinstance(simulationInput.outputManager, DebugOutputManagerInput):
            return DebugOutputManager(outputPd, baseFilename, *addArgs)
            
        return BasicFileOutputManager(outputPd, baseFilename, *addArgs)
    elif isinstance(simulationInput.outputManager, InMemoryOutputManagerInput):
        saveTimeStep = simulationInput.outputManager.saveTimeStep

        return InMemoryOutputManager(saveTimeStep)
    elif isinstance(simulationInput.outputManager, DummyOutputManagerInput):
        return DummyOutputManager()
    else:
        raise Exception('Unknown output manager type ""!'.format(simulationInput.outputManager.type))

# ===============================================================================
# ===============================================================================
@dataclass
class ParameterSetInput():
    value: float = None
    min: float = None
    max: float = None
    numpyDistr: callable = None
    numpyDistrArgs: tuple = None

@dataclass
class PedParameterInput():
    ID: str
    baseSet: str = None
    preferredSpeed: ParameterSetInput = None
    tau: ParameterSetInput = None
    a_0: ParameterSetInput = None
    r_0: ParameterSetInput = None
    a_1: ParameterSetInput = None
    r_1: ParameterSetInput = None
    kappa_0: ParameterSetInput = None
    kappa_1: ParameterSetInput = None
    a_W: ParameterSetInput = None
    d_shy: ParameterSetInput = None
    r_infl: ParameterSetInput = None
    ie_f: ParameterSetInput = None
    ie_b: ParameterSetInput = None
    cPlus_0: ParameterSetInput = None
    cMinus_0: ParameterSetInput = None
    t_A: ParameterSetInput = None
    radius: ParameterSetInput = None
    noise: ParameterSetInput = None

def createPedParameterSets(pedParameterSetInputs):
    pedParameterSets = {}
    pedParameterSetsDicts = {}

    pedParameterSetInputsWithBaseSets = []

    for pedParameterSetInput in pedParameterSetInputs:
        ID = pedParameterSetInput.ID
        if pedParameterSetInput.baseSet is not None:
            pedParameterSetInputsWithBaseSets.append(pedParameterSetInput)
            continue

        paramDict = asdict(pedParameterSetInput)
        paramDict.pop('ID')
        pedParameterSets[ID] = PedParameterSet(ID, **paramDict)
        pedParameterSetsDicts[ID] = paramDict

    addPedParameterSetsWithBaseSet(pedParameterSets, pedParameterSetInputsWithBaseSets, pedParameterSetsDicts)

    return pedParameterSets

def addPedParameterSetsWithBaseSet(pedParameterSets, pedParameterSetInputsWithBaseSets, pedParameterSetsDicts):
    pedParameterSetInputsToCheck = copy(pedParameterSetInputsWithBaseSets)
    while len(pedParameterSetInputsToCheck) > 0:
        pedParameterSetInputsWithBaseSets = copy(pedParameterSetInputsToCheck)
        pedParameterSetInputsToCheck = []
        for pedParameterSetInput in pedParameterSetInputsWithBaseSets:
            if pedParameterSetInput.baseSet not in pedParameterSets:
                pedParameterSetInputsToCheck.append(pedParameterSetInput)
            else:
                addPedParameterSetWithBaseSet(pedParameterSets, pedParameterSetInput, pedParameterSetsDicts)
        if len(pedParameterSetInputsToCheck) == len(pedParameterSetInputsWithBaseSets):
            setStr = '('
            for pedParameterSetInput in pedParameterSetInputsToCheck:
                setStr += '{}, '.format(pedParameterSetInput.ID)
            setStr += ')'
            raise Exception('The base set(s) for pedParameterSet(s) {} does not exist!'.format(setStr))

def addPedParameterSetWithBaseSet(pedParameterSets, pedParameterSetInput, pedParameterSetsDicts):
        ID = pedParameterSetInput.ID

        paramDict = pedParameterSetsDicts[pedParameterSetInput.baseSet]

        paramAddDict = asdict(pedParameterSetInput)
        paramAddDict.pop('ID')
        paramAddDict.pop('baseSet')

        for key, value in paramAddDict.items():
            if value is None and paramDict[key] is not None:
                continue
            paramDict[key] = value

        pedParameterSets[ID] = PedParameterSet(ID, **paramDict)
        pedParameterSetsDicts[ID] = paramDict

# ===============================================================================
# ===============================================================================
@dataclass
class NomadSimulationInput():
    name: str
    label: str
    duration: float
    walkLevels: List[WalkLevelInput]
    demandManager: DemandManagerInput
    outputManager: OutputManagerInput
    pedParameterSets: List[ParameterSetInput]
    inIsolationTimeStep: float = None
    inRangeStepsPerInIsolationStep: int = None
    inCollisionStepsPerInIsolationStep: int = None
    gridCellSize: float = None
    pedManagerType: str = None
    accCalcFcnType: str = None
    logPd: str = None
    logName: str = None
    logStreamLevel: str = None
    nomadModelClass: str = None

    _hasBeenValidated: bool = False

    def validateInput(self):
        self.checkAndAssignObstacleIDs()
        self._hasBeenValidated = True

    @property
    def hasBeenValidated(self):
        return self._hasBeenValidated

    def checkAndAssignObstacleIDs(self):
        obstacleIDs = set()
        for walkLevelInput in self.walkLevels:
            for obstacleInput in walkLevelInput.obstacles:
                if obstacleInput.ID is None:
                    continue
                if obstacleInput.ID in obstacleIDs:
                    raise Exception('An obstacle with ID={} already exists!'.format(obstacleInput.ID))
                obstacleIDs.add(obstacleInput.ID)

        for walkLevelInput in self.walkLevels:
            obstacleIDcounter = 0
            for obstacleInput in walkLevelInput.obstacles:
                if obstacleInput.ID is None:
                    obstacleID = createObstacleID(walkLevelInput.ID, obstacleIDcounter)
                    while obstacleID in obstacleIDs:
                        obstacleIDcounter += 1
                        obstacleID = createObstacleID(walkLevelInput.ID, obstacleIDcounter)
                    obstacleInput.ID = obstacleID
                    obstacleIDcounter += 1

def createObstacleID(walkableLevelID, obstacleIDcounter):
    return '{}_obs_{:04d}'.format(walkableLevelID, obstacleIDcounter)

# ===============================================================================
# The functions that take an instance of the NomadSimulationInput data class and
# create a NomadModel instance based on its content
# ===============================================================================

def createNomadModelFromSimulationInput(simulationInput, nomadModelClass=NomadModel, pedClass=None, lrcmLoadFile=None):
    if not simulationInput.hasBeenValidated:
        simulationInput.validateInput()

    walkLevels, destinations = createWalkLevels(simulationInput.walkLevels)
    demandManager = createDemandManager(simulationInput, walkLevels, destinations)
    outputManager = createOutputManager(simulationInput)
    nomadParameterDict = createNomadParameterDict(simulationInput, pedClass)
    optionalArgs = getOptionalArgs(simulationInput, nomadModelClass, lrcmLoadFile)

    scenario = createNomadModel(simulationInput.name, simulationInput.label, simulationInput.duration,
                          walkLevels, demandManager, outputManager, nomadParameterDict,
                          **optionalArgs)

    return scenario

# ---------------------------------------------------------------------------------

def createNomadParameterDict(simulationInput, pedClass):
    parametersDict = {}
    if simulationInput.gridCellSize is not None:
        parametersDict[GRID_CELL_SIZE_NM] = simulationInput.gridCellSize
    if simulationInput.pedManagerType is not None:
        parametersDict[PED_MANAGER_NM] = simulationInput.pedManagerType
    if simulationInput.accCalcFcnType is not None:
        parametersDict[ACC_CALC_FCN_NM] = simulationInput.accCalcFcnType

    if pedClass is not None:
        parametersDict[PED_CLASS_NM] = pedClass

    return parametersDict

# ---------------------------------------------------------------------------------

OPTIONAL_ARGS = ('inIsolationTimeStep', 'inRangeStepsPerInIsolationStep',
    'inCollisionStepsPerInIsolationStep', 'logPd', 'logName', 'logStreamLevel', 'nomadModelClass')

def getOptionalArgs(simulationInput, nomadModelClass, lrcmLoadFile):
    optionalArgs = {'nomadModelClass':nomadModelClass}

    for optionalArg in OPTIONAL_ARGS:
        if getattr(simulationInput, optionalArg) is not None:
            optionalArgs[optionalArg] = getattr(simulationInput, optionalArg)

    if lrcmLoadFile is not None:
        optionalArgs['lrcmLoadFile'] = lrcmLoadFile

    return optionalArgs
