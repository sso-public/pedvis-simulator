""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

import collections
import logging
from pathlib import Path

from NOMAD.constants import PARAM_NMS
from NOMAD.input_manager import createWalkLevels, createDemandManager, \
    createNomadParameterDict, getOptionalArgs, createOutputManager, \
    BasicFileOutputManagerInput
from NOMAD.nomad_model import NomadModel, createNomadModel
from NOMAD.output_manager import exportTrajectoriesToMat, BasicFileOutputManager
from NOMAD.xml_scenario_input import parseXml
import numpy as np


def runGridSearch(xmlFlNm, pedParameterSearchGrid, convert2mat=False):
    simulationInput = parseXml(xmlFlNm)
    walkLevels, destinations = createWalkLevels(simulationInput.walkLevels)
    demandManager = createDemandManager(simulationInput, walkLevels, destinations)
    nomadParameterDict = createNomadParameterDict(simulationInput, None)
    optionalArgs = getOptionalArgs(simulationInput, NomadModel)

    if len(demandManager.pedParamaterSets) > 1:
        raise Exception('Only one pedestrian parameter set allowed!')

    logPd = None
    if 'logPd' in optionalArgs:
        logPd = optionalArgs['logPd']

    createLoggers(logPd)
    logger = logging.getLogger(__name__)
    logger.info('Starting grid search over {} entries'.format(pedParameterSearchGrid.gridElCont))

    for ind in pedParameterSearchGrid.getIterator():
        logger.info('Start {} = {}'.format(ind, pedParameterSearchGrid.getStringOfCurrentIteration(ind)))
        pedParameterSearchGrid.updatePedParameters(demandManager.pedParamaterSets[0], ind)
        outputManager = pedParameterSearchGrid.createOutputManager(simulationInput, ind)
        nomadModel = createNomadModel(simulationInput.name, simulationInput.label, simulationInput.duration,
                              walkLevels, demandManager, outputManager, nomadParameterDict,
                              **optionalArgs)
        nomadModel.start()

        if convert2mat and isinstance(outputManager, BasicFileOutputManager):
            exportTrajectoriesToMat(outputManager.scenarioFilename)


class PedParameterSearchGrid():

    def __init__(self, **kwargs):
        self.gridDimensionCnt = len(kwargs)
        self.paramNames = []
        self.baseFileName = None
        self.valueArrays = []
        for paramName, valueArray in kwargs.items():
            if paramName not in PARAM_NMS:
                raise Exception('The parameter name "{}" is not known!'.format(paramName))
            if not isinstance(valueArray, (collections.Sequence, np.ndarray)):
                raise Exception('The value array of "{}" is not a valid array!'.format(paramName))

            try :
                valueArray = np.array(valueArray, dtype=float)
            except ValueError:
                raise Exception('The contents of the value array of "{}" are not valid!'.format(paramName))

            self.paramNames.append(paramName)
            self.valueArrays.append(valueArray)

        if len(self.paramNames) == 1:
            self.grid = self.valueArrays
        elif len(self.paramNames) == 2:
            meshGrid = np.array(np.meshgrid(*self.valueArrays))
            self.grid = meshGrid.T.reshape(-1, 2)
        else:
            meshGrid = np.array(np.meshgrid(*self.valueArrays[:2]))
            baseGrid = meshGrid.T.reshape(-1, 2)
            for valueArray in self.valueArrays[2:]:
                baseLength = len(baseGrid)
                grid = np.zeros((baseLength*len(valueArray),baseGrid.shape[1]+1), dtype=float)
                endInd = 0
                for value in valueArray:
                    startInd = endInd
                    endInd = startInd + baseLength
                    grid[startInd:endInd,-1] = value
                    grid[startInd:endInd,:-1] = baseGrid

                baseGrid = grid

            self.grid = grid

        self.gridElCont = len(self.grid)

    def getIterator(self):
        return range(self.gridElCont)

    def updatePedParameters(self, pedParameterSet, ind):
        paramValues = self.grid[ind]
        for ii in range(self.gridDimensionCnt):
            pedParameterSet.updateParamValue(self.paramNames[ii], paramValues[ii])

    def createOutputManager(self, simulationInput, ind):
        if isinstance(simulationInput.outputManager, BasicFileOutputManagerInput):
            if self.baseFileName is None:
                self.baseFileName = simulationInput.outputManager.baseFilename
            simulationInput.outputManager.baseFilename = self.createBaseFileName(ind)

        return createOutputManager(simulationInput)

    def createBaseFileName(self, ind):
        return '{}_{}'.format(self.baseFileName, self.getStringOfCurrentIteration(ind))

    def getStringOfCurrentIteration(self, ind):
        itString = ''
        paramValues = self.grid[ind]
        for ii in range(len(self.paramNames)):
            itString = '{}_{}={:.3f}'.format(itString, self.paramNames[ii], paramValues[ii])

        return itString

def createLoggers(logDir, name='NOMAD_model_calibration', streamLevel=logging.DEBUG):
    fileLoggers = [{'ID':'fileDebug', 'level':logging.DEBUG, 'file':'debug.log'},
              {'ID':'fileInfo', 'level':logging.INFO, 'file':'info.log'}]

    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    if logDir is not None:
        logDir = Path(logDir)
        if not logDir.is_dir():
            logDir.mkdir()

    configDict = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'default': {
                'format': '%(asctime)s.%(msecs)03d - %(name)s - %(levelname)s - %(message)s',
                'datefmt': '%Y/%m/%d %H:%M:%S',
                'style': '%',
            },
        },
    }

    handlers = []
    handlersDict = {}

    if logDir is not None:
        for fileLogger in fileLoggers:
            logFileName = logDir.joinpath('{}_{}'.format(name, fileLogger['file']))
            handlersDict[fileLogger['ID']] = {
                'class': 'logging.handlers.RotatingFileHandler',
                'level': fileLogger['level'],
                'formatter': 'default',
                'filename': logFileName,
                'maxBytes': 1024 * 1024 * 30,
                'backupCount': 2
                }
            handlers.append(fileLogger['ID'])

    streamID = 'stream'
    handlersDict[streamID] = {
        'class': 'logging.StreamHandler',
        'level': streamLevel,
        'formatter': 'default',
        }
    handlers.append(streamID)

    configDict['handlers'] = handlersDict
    configDict['root'] = {
        'level': streamLevel,
        'handlers': handlers,
        }

    logging.config.dictConfig(configDict)