""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

from dataclasses import dataclass

from NOMAD.constants import NAME_FIELD, PREFERRED_SPEED, MIN_FIELD, MAX_FIELD, \
    VALUE_FIELD, TAU, A_0, R_0, A_1, R_1, KAPPA_0, KAPPA_1, A_W, D_SHY, R_INFL, \
    IE_F, IE_B, C_PLUS_0, C_MINUS_0, T_A, RADIUS, NOISE_FACTOR, PARAM_NMS, \
    PARAM_FIELDNAMES, DISTR_FIELD, DISTR_ARGS_FIELD


DEFAULTS = (
    {NAME_FIELD:PREFERRED_SPEED, MIN_FIELD:0.8,    MAX_FIELD:2.0,   VALUE_FIELD:1.34},
    {NAME_FIELD:TAU,             MIN_FIELD:0.1,    MAX_FIELD:2.0,   VALUE_FIELD:0.5},
    {NAME_FIELD:A_0,             MIN_FIELD:0.01,   MAX_FIELD:20,    VALUE_FIELD:0.7},
    {NAME_FIELD:R_0,             MIN_FIELD:0.01,   MAX_FIELD:20,    VALUE_FIELD:0.5},
    {NAME_FIELD:A_1,             MIN_FIELD:0.1,    MAX_FIELD:20,    VALUE_FIELD:0.85},
    {NAME_FIELD:R_1,             MIN_FIELD:0.01,   MAX_FIELD:20,    VALUE_FIELD:0.6},
    {NAME_FIELD:KAPPA_0,         MIN_FIELD:1,      MAX_FIELD:1e6,   VALUE_FIELD:1e5},
    {NAME_FIELD:KAPPA_1,         MIN_FIELD:1,      MAX_FIELD:1e3,   VALUE_FIELD:10},
    {NAME_FIELD:A_W,             MIN_FIELD:0.1,    MAX_FIELD:20,    VALUE_FIELD:10},
    {NAME_FIELD:D_SHY,           MIN_FIELD:0.1,    MAX_FIELD:2.0,   VALUE_FIELD:0.2},
    {NAME_FIELD:R_INFL,          MIN_FIELD:0.5,    MAX_FIELD:6.0,   VALUE_FIELD:3},
    {NAME_FIELD:IE_F,            MIN_FIELD:0.5,    MAX_FIELD:6.0,   VALUE_FIELD:3},
    {NAME_FIELD:IE_B,            MIN_FIELD:0.5,    MAX_FIELD:6.0,   VALUE_FIELD:1},
    {NAME_FIELD:C_PLUS_0,        MIN_FIELD:0.1,    MAX_FIELD:2.0,   VALUE_FIELD:0.25},
    {NAME_FIELD:C_MINUS_0,       MIN_FIELD:0.1,    MAX_FIELD:2.0,   VALUE_FIELD:0.85},
    {NAME_FIELD:T_A,             MIN_FIELD:0.01,   MAX_FIELD:2.0,   VALUE_FIELD:0.25},
    {NAME_FIELD:RADIUS,          MIN_FIELD:0.2,    MAX_FIELD:0.3,   VALUE_FIELD:0.25},
    {NAME_FIELD:NOISE_FACTOR,    MIN_FIELD:0.0025, MAX_FIELD:0.005, VALUE_FIELD:0.001}
    )

def getDefaultValue(paramName):
    for paramDict in DEFAULTS:
        if paramDict[NAME_FIELD] == paramName:
            return paramDict[VALUE_FIELD]

@dataclass(frozen=True)
class PedParameter():
    name: str
    min: float
    max: float
    value: float = None
    numpyDistr: callable = None
    numpyDistrArgs: tuple = None

    def __post_init__(self):
        if self.value is None and self.numpyDistr is None:
            raise Exception('{}: Both the value and the distr fields are None!'.format(self.name))

        if self.value is not None and self.numpyDistr is not None:
            raise Exception('{}: Both the value and the distr fields are not defined!'.format(self.name))

        if self.numpyDistr is not None and self.numpyDistrArgs is None:
            raise Exception('{}: The distr fields is defined but the distrArgs is not!'.format(self.name))

    def setValue(self, value):
        super().__setattr__('value', value)

    def getValue(self):
        if self.value is not None:
            return self.value
        else:
            return self.getValueFromDistribution()

    def getValueFromDistribution(self):
        while True:
            value = self.numpyDistr(*self.numpyDistrArgs)
            if value < self.min:
                continue
            elif value > self.max:
                continue
            return value

    def getMaxValue(self):
        if self.numpyDistr is not None:
            return self.max
        else:
            return self.value

class PedParameterSet():
    '''
    classdocs
    '''

    def __init__(self, ID, *args, **kwargs):
        '''
        Constructor
        '''
        self._ID = ID
        self._uniqueCopy = False

        if len(args) and isinstance(args[0], PedParameterSet):
            self._uniqueCopy = True
            baseParamSet = args[0]
            for paramName in PARAM_NMS:
                setattr(self, '_' + paramName, getattr(baseParamSet, '_' + paramName).getValue())
            return

        for paramDict in DEFAULTS:
            if paramDict[NAME_FIELD] in kwargs and kwargs[paramDict[NAME_FIELD]] is not None:
                for key, value in kwargs[paramDict[NAME_FIELD]].items():
                    if key == NAME_FIELD:
                        raise Exception('Param={} :The name of a parameter cannot be changed!'.format(paramDict[NAME_FIELD]))
                    if key not in PARAM_FIELDNAMES:
                        raise Exception('Param={} :The key {} is not a valid PedParameter class field!'.format(paramDict[NAME_FIELD], key))
                    if key == DISTR_FIELD and value is not None:
                        if DISTR_ARGS_FIELD not in kwargs[paramDict[NAME_FIELD]]:
                            raise Exception('Param={} :The parameter contain a {} field but no {} field!'.format(paramDict[NAME_FIELD], DISTR_FIELD, DISTR_ARGS_FIELD))
                        paramDict[VALUE_FIELD] = None

                    paramDict[key] = value

            param = PedParameter(**paramDict)
            setattr(self, '_' + param.name, param)

    def _getValue(self, paramName):
        if self._uniqueCopy:
            return getattr(self, '_' + paramName)
        else:
            return getattr(self, '_' + paramName).getValue()

    def _setValue(self, paramName, value):
        if self._uniqueCopy:
            setattr(self, '_' + paramName, value)
        else:
            getattr(self, '_' + paramName).setValue(value)


    def getMaxParamValue(self, paramName):
        return getattr(self, '_' + paramName).getMaxValue()

    def getUniqueCopy(self):
        return PedParameterSet(self.ID, self)

    def updateParamValue(self, paramName, value):
        self._setValue(paramName, value)

    @property
    def ID(self):
        return self._ID

    @property
    def preferredSpeed(self):
        return self._getValue(PREFERRED_SPEED)

    @property
    def tau(self):
        return self._getValue(TAU)

    @property
    def a_0(self):
        return self._getValue(A_0)

    @property
    def r_0(self):
        return self._getValue(R_0)

    @property
    def a_1(self):
        return self._getValue(A_1)

    @property
    def r_1(self):
        return self._getValue(R_1)

    @property
    def kappa_0(self):
        return self._getValue(KAPPA_0)

    @property
    def kappa_1(self):
        return self._getValue(KAPPA_1)

    @property
    def a_W(self):
        return self._getValue(A_W)

    @property
    def d_shy(self):
        return self._getValue(D_SHY)

    @property
    def r_infl(self):
        return self._getValue(R_INFL)

    @property
    def ie_f(self):
        return self._getValue(IE_F)

    @property
    def ie_b(self):
        return self._getValue(IE_B)

    @property
    def cPlus_0(self):
        return self._getValue(C_PLUS_0)

    @property
    def cMinus_0(self):
        return self._getValue(C_MINUS_0)

    @property
    def t_A(self):
        return self._getValue(T_A)

    @property
    def radius(self):
        return  self._getValue(RADIUS)

    @property
    def noiseFactor(self):
        return  self._getValue(NOISE_FACTOR)