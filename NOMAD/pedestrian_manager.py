""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

from dataclasses import dataclass, field
import itertools
import logging

from NOMAD.activities import TimeBasedActivity, Waypoint, ActivityPattern, Sink, \
    OutsideOfSimulationActivity
from NOMAD.constants import IN_COLLISION_STATE, IN_RANGE_STATE, PED_STATIC_STATE, \
                PED_MOVING_STATE, PED_OUTSIDE_OF_SIM_STATE, PED_STATIC_NON_INTERACTING_STATE
from NOMAD.nomad_pedestrian_parameters import PedParameterSet
from NOMAD.pedestrian import Pedestrian as PedestrianAttr, ATTR_PED_CLASSES
from NOMAD.vectors import Vector2D
from NOMAD.walk_level import WalkLevel


class PedestrianManager():
    '''
    Base class
    '''

    logger = logging.getLogger(__name__)

    def __init__(self, timeInfo, parameters):
        '''
        Constructor
        '''
        self.movingPedestrians = {}
        self.staticPedestrians = {} # All pedestrians that are still in the simulation but not active
        self.outOfSimulationPedestrians = {} # All pedestrians that are temporarily outside of the simulation but can reappear again
        self.staticNonInteractingPedestrians = {} # All pedestrians that are still in the simulation but not active or visible to other peds
        self.pedestrianPerformingTimeBasedActivities = {}
        self._pedIDcouter = 0
        self.pedsAdded = []

        self.timeInfo = timeInfo
        self.parameters = parameters
        
        self.pedClass = ATTR_PED_CLASSES[parameters.PED_CLASS]

    def initialize(self):
        self.movingPedestrians = {}
        self.staticPedestrians = {} # All pedestrians that are still in the simulation but not active
        self.outOfSimulationPedestrians = {} # All pedestrians that are temporarily outside of the simulation but can reappear again
        self.staticNonInteractingPedestrians = {} # All pedestrians that are still in the simulation but not active or visible to other peds
        self.pedestrianPerformingTimeBasedActivities = {}
        self._pedIDcouter = 0
        self.pedsAdded = []
         
    def addPedestrians(self, peds2add):
        self.pedsAdded = []
        for pedInfo in peds2add:
                ped = self.createPedestrian(self._pedIDcouter, pedInfo, self.timeInfo.currentTime)
                self.pedsAdded.append(ped)
                self._pedIDcouter += 1

        return self.pedsAdded

    def createPedestrian(self, ID, pedInfo, currentTime):
        kwargs = pedInfo.getGroupIDKwarg()
        ped = self.pedClass(ID, pedInfo.paramSet, pedInfo.activityPattern, self.pedClass.ACC_CALC_FCNS[self.parameters.ACC_CALC_FCN], **kwargs)
        ped.initialize(Vector2D(pedInfo.initialPos), pedInfo.initialWalkLevel, currentTime)
        ped.startTime = self.timeInfo.currentTime
        return ped

    def createPedestrianOutsideOfSimulation(self, pedInfo):
        ped = self.createPedestrian(self._pedIDcouter, pedInfo, self.timeInfo.currentTime)
        self._pedIDcouter += 1
        self.makePedOutsideOfSim(ped)
        return ped

    def removePedestrians(self, peds2Remove):
        prevTime = self.timeInfo.getPreviousTime()
        for ped in peds2Remove:
            ped.gridCell.removePed(ped)
            ped.endTime = prevTime
            ped.activityLog[-1].setEndTime(prevTime)
            self._popFromPedDict(ped)

    def makePedMoving(self, ped):
        self._popFromPedDict(ped)
        self.movingPedestrians[ped.ID] = ped
        ped.state = PED_MOVING_STATE

    def makePedStatic(self, ped):
        self._popFromPedDict(ped)
        self.staticPedestrians[ped.ID] = ped
        ped.state = PED_STATIC_STATE

    def makePedOutsideOfSim(self, ped):
        self._popFromPedDict(ped)
        self.outOfSimulationPedestrians[ped.ID] = ped
        ped.state = PED_OUTSIDE_OF_SIM_STATE

    def makePedStaticNonInteracting(self, ped):
        self._popFromPedDict(ped)
        self.staticNonInteractingPedestrians[ped.ID] = ped
        ped.state = PED_STATIC_NON_INTERACTING_STATE

    def _popFromPedDict(self, ped):
        if ped.state == PED_MOVING_STATE:
            self.movingPedestrians.pop(ped.ID)
        elif ped.state == PED_STATIC_STATE:
            self.staticPedestrians.pop(ped.ID)
        elif ped.state == PED_OUTSIDE_OF_SIM_STATE:
            self.outOfSimulationPedestrians.pop(ped.ID)
        elif ped.state == PED_STATIC_NON_INTERACTING_STATE:
            self.staticNonInteractingPedestrians.pop(ped.ID)
        elif ped.state is None:
            return
        else:
            raise Exception(f'Unknown ped state {ped.state}')

    def updatePedsPerformingActivities(self, pedsLeavingTheirActivity):
        pedsLeavingTheirActivity += self.pedestrianPerformingTimeBasedActivities.get(self.timeInfo.timeInd, [])

        # Collect those that still need to be placed and place them if possible
        peds2placePerDestination = {}
        for ped in pedsLeavingTheirActivity:
            if isinstance(ped.curActivity, OutsideOfSimulationActivity) and ped.state == PED_OUTSIDE_OF_SIM_STATE:
                if ped.curActivity.destination not in peds2placePerDestination:
                    peds2placePerDestination[ped.curActivity.destination] = []
                peds2placePerDestination[ped.curActivity.destination].append(ped)

        for destination, peds2place in peds2placePerDestination.items():
            pedsNotPlaced = destination.setPlacementLocationForPeds(peds2place)
            nextTimeInd = self.timeInfo.getNextTimeInd()
            # Any ped that cannot be placed should be placed next time step if possible
            for ped in pedsNotPlaced:
                pedsLeavingTheirActivity.remove(ped)
                if nextTimeInd not in self.pedestrianPerformingTimeBasedActivities:
                    self.pedestrianPerformingTimeBasedActivities[nextTimeInd] = []
                self.pedestrianPerformingTimeBasedActivities[nextTimeInd].append(ped)    

        pedsInteractingAgain = []
        for ped in pedsLeavingTheirActivity:
            ped.curActivity.actionOnLeavingActivity(ped, self.timeInfo.currentTime)
 
            oldState, _ = self.changePedState(ped, PED_MOVING_STATE)
            if oldState in (PED_OUTSIDE_OF_SIM_STATE, PED_STATIC_NON_INTERACTING_STATE):
                pedsInteractingAgain.append(ped)
                
        return pedsInteractingAgain, pedsLeavingTheirActivity

    def calcNewPedestrianPositions(self, pedsLeavingTheirActivity):
        self.updatePedestrianIsolationStates(pedsLeavingTheirActivity)

        for ped in self.pedsAdded:  # Because of broadcast
            self.makePedMoving(ped)

        pedsInIsolation, pedsInRange, pedsInCollision = self.getPedsIsolationStateLists()

        self.calcNextPedestrianPositions(pedsInIsolation, pedsInRange, pedsInCollision)

        return self.movingPedestrians.values()

    def updatePedestrianIsolationStates(self, pedsLeavingTheirActivity):
        for ped in self.pedsAdded:
            ped.broadcastIsolation(self.timeInfo.currentTime, self.timeInfo.inIsolationTimeStep, self.parameters)
            PedestrianAttr.updateObsIsolationTime(ped, self.timeInfo.currentTime, self.timeInfo.inIsolationTimeStep, self.parameters)

        for ped in self.movingPedestrians.values():
            ped.updateIsolationState(self.timeInfo.currentTime, self.timeInfo.inIsolationTimeStep, self.parameters, forceUpdate=ped in pedsLeavingTheirActivity)

    def getPedsIsolationStateLists(self):
        pedsInIsolation = []
        pedsInRange = []
        pedsInCollision = []
        for ped in self.movingPedestrians.values():
            if ped.pedIsolationState == IN_COLLISION_STATE or ped.obsIsolationState == IN_COLLISION_STATE:
                pedsInCollision.append(ped)
            elif ped.pedIsolationState == IN_RANGE_STATE or ped.obsIsolationState == IN_RANGE_STATE:
                pedsInRange.append(ped)
            else:
                pedsInIsolation.append(ped)

        return pedsInIsolation, pedsInRange, pedsInCollision

    def updatePedsReachingTheirActivities(self, pedsThatReachedTheirDest):
        newStaticNonInteractingPeds = []
        newOutsideOfSimulationPeds = []
        newStaticPeds = []
        for ped in pedsThatReachedTheirDest:
            if isinstance(ped.curActivity, Waypoint):
                ped.curActivity.actionOnLeavingActivity(ped, self.timeInfo.currentTime)
                continue
            if isinstance(ped.curActivity, TimeBasedActivity):
                endTimeInd = self.timeInfo.getTimeInd(ped.curActivity.getEndTime(self.timeInfo.currentTime))
                if endTimeInd <= self.timeInfo.timeInd:
                    ped.curActivity.actionOnLeavingActivity(ped, self.timeInfo.currentTime)
                    continue
                if endTimeInd not in self.pedestrianPerformingTimeBasedActivities:
                    self.pedestrianPerformingTimeBasedActivities[endTimeInd] = []
                self.pedestrianPerformingTimeBasedActivities[endTimeInd].append(ped)                 

            ped.curActivity.actionOnReachingActivity(ped, self.timeInfo.currentTime)
            if isinstance(ped.curActivity, Sink):
                continue

            _, newState = self.changePedState(ped, ped.curActivity.pedStateWhilstPerformingActivity) 
            if newState == PED_OUTSIDE_OF_SIM_STATE:
                newOutsideOfSimulationPeds.append(ped)
            elif newState == PED_STATIC_NON_INTERACTING_STATE:
                newStaticNonInteractingPeds.append(ped)
            elif newState == PED_STATIC_STATE:
                newStaticPeds.append(ped)
                                                                        
        return newStaticNonInteractingPeds, newOutsideOfSimulationPeds, newStaticPeds
    
    def changePedState(self, ped, newState):
        if ped.state == newState:
            return ped.state, ped.state
        oldState = ped.state
        if newState == PED_MOVING_STATE:
            self.makePedMoving(ped)
        elif newState == PED_STATIC_STATE:
            self.makePedStatic(ped)
        elif newState == PED_OUTSIDE_OF_SIM_STATE:
            self.makePedOutsideOfSim(ped)
        elif newState == PED_STATIC_NON_INTERACTING_STATE:
            self.makePedStaticNonInteracting(ped)
        else:
            raise Exception(f'Unknown ped state {newState}')
        
        return oldState, ped.state
        
    def calcNextPedestrianPositions(self, pedsInIsolation, pedsInRange, pedsInCollision):
        for ped in pedsInIsolation:
            ped.calcNextPosition(self.timeInfo.inIsolationTimeStep, 1, self.parameters)

        if len(pedsInCollision) == 0 and len(pedsInRange) == 0:
            self.updatePedestrianPositions(pedsInIsolation, True)
            return

        if len(pedsInCollision) == 0:
            for ii in range(1,self.timeInfo.inRangeStepsPerInIsolationStep+1):
                for ped in pedsInRange:
                    ped.calcNextPosition(self.timeInfo.inRangeTimeStep, self.timeInfo.inRangeStepsPerInIsolationStep, self.parameters)
                self.updatePedestrianPositions(pedsInRange, ii==1)

            self.updatePedestrianPositions(pedsInIsolation, True)
            return

        inRangeStep = self.timeInfo.inRangeStep
        endInd = self.timeInfo.inCollisionStepsPerInIsolationStep
        for stepNr in range(1, endInd+1):
            for ped in pedsInCollision:
                ped.calcNextPosition(self.timeInfo.inCollisionTimeStep, self.timeInfo.inCollisionStepsPerInIsolationStep, self.parameters)

            if stepNr > inRangeStep or stepNr == self.timeInfo.inCollisionStepsPerInIsolationStep:
                for ped in pedsInRange:
                    ped.calcNextPosition(self.timeInfo.inRangeTimeStep, self.timeInfo.inRangeStepsPerInIsolationStep, self.parameters)
                self.updatePedestrianPositions(pedsInRange, stepNr==1)
                inRangeStep += self.timeInfo.inRangeStep
            else:
                for ped in pedsInRange:
                    ped.clearOtherPedListsAndSets()
            self.updatePedestrianPositions(pedsInCollision, stepNr==1)

        self.updatePedestrianPositions(pedsInIsolation, True)

    def updatePedestrianPositions(self, pedestrians, updatePrev):
        for ped in pedestrians:
            ped.updatePosition(updatePrev)

    def __repr__(self):
        return 'PedestrianManagerAttr with pedClass = {}'.format(self.pedClass.getLabel())

    @property
    def movingPedestriansList(self):
        return list(self.movingPedestrians.values())

    @property
    def movingPedestriansIterable(self):
        return self.movingPedestrians.values()

    @property
    def staticPedestriansIterable(self):
        return self.staticPedestrians.values()

    @property
    def pedestrianIterable(self):
        return itertools.chain(self.movingPedestrians.values(), self.staticPedestrians.values(),
                               self.outOfSimulationPedestrians.values(), self.staticNonInteractingPedestrians.values())

    @property
    def pedestrianList(self):
        return list(self.pedestrianIterable())

@dataclass
class PedCreationInfo():
    paramSet: PedParameterSet
    activityPattern: ActivityPattern
    initialPos: tuple = field(init=False)
    initialWalkLevel: WalkLevel = field(init=False)
    groupID: str = field(init=False)

    def getGroupIDKwarg(self):
        if hasattr(self, 'groupID'):
            return {'groupID':self.groupID}
        else:
            return {}