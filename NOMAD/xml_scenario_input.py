""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

from pathlib import Path
from xml.dom.minidom import parse

from NOMAD.input_manager import createNomadModelFromSimulationInput, \
    NomadSimulationInput, LineObstacleInput, PolygonObstacleInput, \
    CircleObstacleInput, EllipseObstacleInput, WalkLevelInput, \
    WalkableAreaInput, ParameterSetInput, PedParameterInput, \
    DemandManagerInput, ConstantDemandPatternInput, \
    VariableDemandPatternInput, BasicFileOutputManagerInput, InMemoryOutputManagerInput, \
    DummyOutputManagerInput , SimpleRandomLineSourceInput, \
    RectangleSourceInput, LineDestinationInput, PolygonDestinationInput, \
    SinkInput, WaypointInput, PointDestinationInput, \
    CentroidPolygonDestinationInput, FixedWaitingTimeActivityInput, \
    DiscreteDemandPatternInput, RestaurantSchedulerInput, DistrValueInput, \
    FixedWaitingTimeNonInteractingActivityInput, EndAtTimeWaitingActivityInput, \
    DebugOutputManagerInput, ListActivityPatternInput, \
    RestaurantStaffSchedulerInput, SpawnableLineDestinationInput
from NOMAD.nomad_model import NomadModel
import NOMAD.xml_functions as xml


def createNomadModelFromInputXml(xmlFlNm, nomadModelClass=NomadModel, pedClass=None, lrcmLoadFile=None):
    simulationInput = parseXml(xmlFlNm)
    return createNomadModelFromSimulationInput(simulationInput, nomadModelClass, pedClass, lrcmLoadFile)

def parseXml(xmlFlNm):
    DOMTree = parse(str(xmlFlNm))
    scenXml = DOMTree.documentElement

    xmlFlNm = Path(xmlFlNm)
    pd = xmlFlNm.parent
    infraXmlFlNm = xml.getFlNm(scenXml, 'infrastructureXmlFlNm', pd=pd)
    walkLevels = parseInfraXml(infraXmlFlNm)
    pedParamSetsXmlFlNm = xml.getFlNm(scenXml, 'pedParameterSetsXmlFlNm', pd=pd)
    pedParameterSets = parsePedParameterSetsXml(pedParamSetsXmlFlNm)

    scenInput = parseScenario(scenXml, pd)
    scenInput['walkLevels'] = walkLevels
    scenInput['pedParameterSets'] = pedParameterSets
    scenInput['demandManager'] = parseDemandManager(scenXml)
    scenInput['outputManager'] = parseOutputManager(scenXml, pd)

    nomadSimulationInput = NomadSimulationInput(**scenInput)

    return nomadSimulationInput

# ===============================================================================

def getObstacleID(obstacleXml):
    ID = None
    if xml.hasAttribute(obstacleXml, 'ID'):
        ID = xml.getAttribute(obstacleXml, 'ID')

    return ID

def getSeeThrough(obstacleXml):
    if xml.hasElement(obstacleXml, 'seeThrough'):
        return xml.getBoolean(obstacleXml, 'seeThrough')
    else:
        return None

def parseLineObstacle(obstacleXml):
    return parsePolygonLikeObstacle(obstacleXml, LineObstacleInput)

def parsePolygonObstacle(obstacleXml):
    return parsePolygonLikeObstacle(obstacleXml, PolygonObstacleInput)

def parsePolygonLikeObstacle(obstacleXml, creationFcn):
    ID, seeThrough = parseObstacleBase(obstacleXml)
    coords = xml.getCoordinateTupleEntry(obstacleXml, 'coords')

    return creationFcn(ID, seeThrough, coords)

def parseObstacleBase(obstacleXml):
    ID = getObstacleID(obstacleXml)
    seeThrough = getSeeThrough(obstacleXml)

    return ID, seeThrough

def parseObstacleCicularBase(obstacleXml):
    ID, seeThrough = parseObstacleBase(obstacleXml)
    centerCoord = xml.getCoordinateTupleEntry(obstacleXml, 'centerCoord')
    return ID, seeThrough, centerCoord

def parseCircleObstacle(obstacleXml):
    ID, seeThrough, centerCoord = parseObstacleCicularBase(obstacleXml)
    radius = xml.getFloat(obstacleXml, 'radius')
    return CircleObstacleInput(ID, seeThrough, centerCoord, radius)

def parseEllipseObstacle(obstacleXml):
    ID, seeThrough, centerCoord = parseObstacleCicularBase(obstacleXml)
    semiAxesValues = xml.getCoordinateTupleEntry(obstacleXml, 'semiAxesValues')
    return EllipseObstacleInput(ID, seeThrough, centerCoord, semiAxesValues)

OBSTACLE_TYPES = {
    'lineObstacle':parseLineObstacle,
    'polygonObstacle':parsePolygonObstacle,
    'circleObstacle':parseCircleObstacle,
    'ellipseObstacle':parseEllipseObstacle
    }

# ===============================================================================

def parseLineDestination(destinationXml):
    ID, coords, groupID, kwargs = parsDestinationBase(destinationXml)
    return LineDestinationInput(ID, coords, groupID, **kwargs)

def parsePointDestination(destinationXml):
    ID, coords, groupID, kwargs = parsDestinationBase(destinationXml)
    return PointDestinationInput(ID, coords, groupID, **kwargs)

def parsePolygonDestination(destinationXml):
    ID, coords, groupID, kwargs = parsDestinationBase(destinationXml)
    return PolygonDestinationInput(ID, coords, groupID, **kwargs)

def parseCentroidPolygonDestination(destinationXml):
    ID, coords, groupID, kwargs = parsDestinationBase(destinationXml)
    return CentroidPolygonDestinationInput(ID, coords, groupID, **kwargs)

def parseSpawnableLineDestination(destinationXml):
    ID, coords, groupID, kwargs = parsDestinationBase(destinationXml)
    return SpawnableLineDestinationInput(ID, coords, groupID, **kwargs)

IS_VIRTUAL_OBS_KEY = 'isVirtualObstacle'

def parsDestinationBase(destinationXml):
    ID = xml.getAttribute(destinationXml, 'ID')
    coords = xml.getCoordinateTupleEntry(destinationXml, 'coords')

    groupID = None
    if xml.hasAttribute(destinationXml, 'groupID'):
        groupID = xml.getAttribute(destinationXml, 'groupID')

    kwargs = {}

    if xml.hasElement(destinationXml, IS_VIRTUAL_OBS_KEY):
        kwargs[IS_VIRTUAL_OBS_KEY] = xml.getBoolean(destinationXml, IS_VIRTUAL_OBS_KEY)

    return ID, coords, groupID, kwargs

DESTINATION_TYPES = {
    'lineDestination':parseLineDestination,
    'pointDestination':parsePointDestination,
    'polygonDestination':parsePolygonDestination,
    'centroidPolygonDestination':parseCentroidPolygonDestination,
    'spawnableLineDestination':parseSpawnableLineDestination
    }

# ===============================================================================

# ===============================================================================

def parseSimpleRandomLineSource(sourceXml):
    ID, coords = parsSourceBase(sourceXml)
    return SimpleRandomLineSourceInput(ID, coords)

def parseRectangleSource(sourceXml):
    ID, coords = parsSourceBase(sourceXml)
    directionVec = xml.getCoordinateTupleEntry(sourceXml, 'directionVec')
    return RectangleSourceInput(ID, coords, directionVec)

def parsSourceBase(sourceXml):
    ID = xml.getAttribute(sourceXml, 'ID')
    coords = xml.getCoordinateTupleEntry(sourceXml, 'coords')

    return ID, coords

SOURCE_TYPES = {
    'simpleRandomLineSource':parseSimpleRandomLineSource,
    'rectangleSource':parseRectangleSource
    }

# ===============================================================================

def parseInfraXml(infraXmlFlNm):
    DOMTree = parse(str(infraXmlFlNm))
    infraXml = DOMTree.documentElement
    walkLevelXmls = xml.getElements(infraXml, 'walkLevel')
    walkLevels = []
    for walkLevelXml in walkLevelXmls:
        walkLevels.append(parseWalkLevel(walkLevelXml))

    return walkLevels

def parseWalkLevel(walkLevelXml):
    ID = xml.getAttribute(walkLevelXml, 'ID')
    # TODO: Elevation
    walkableAreas = parseWalkableAreas(walkLevelXml)
    obstacles = parseObstacles(walkLevelXml)
    destinations = parseDestinations(walkLevelXml)
    sources = parseSources(walkLevelXml)

    return WalkLevelInput(ID, walkableAreas, obstacles, destinations, sources)

def parseWalkableAreas(walkLevelXml):
    walkableAreas = []
    walkableAreaXmls = xml.getElements(walkLevelXml, 'walkableArea')
    for walkableAreaXml in walkableAreaXmls:
        ID = xml.getAttribute(walkableAreaXml, 'ID')
        coords = xml.getCoordinateTupleEntry(walkableAreaXml, 'coords')
        walkableAreas.append(WalkableAreaInput(ID, coords))

    return walkableAreas

def parseObstacles(walkLevelXml):
    obstacles = []
    for obstacleKey, parseFcn in OBSTACLE_TYPES.items():
        obstacleXmls = xml.getElements(walkLevelXml, obstacleKey)
        for obstacleXml in obstacleXmls:
            obstacles.append(parseFcn(obstacleXml))

    return obstacles

def parseDestinations(walkLevelXml):
    destinationsXml = xml.getSingleElement(walkLevelXml, 'destinations')
    destinations = []
    for destinationKey, parseFcn in DESTINATION_TYPES.items():
        destinationXmls = xml.getElements(destinationsXml, destinationKey)
        for destinationXml in destinationXmls:
            destinations.append(parseFcn(destinationXml))

    return destinations

def parseSources(walkLevelXml):
    sourcesXml = xml.getSingleElement(walkLevelXml, 'sources')
    sources = []
    for sourceKey, parseFcn in SOURCE_TYPES.items():
        sourceXmls = xml.getElements(sourcesXml, sourceKey)
        for sourceXml in sourceXmls:
            sources.append(parseFcn(sourceXml))

    return sources

# ===============================================================================

PARAMETER_NAMES = ('preferredSpeed', 'tau', 'a_0', 'r_0', 'a_1', 'r_1', 'kappa_0',
    'kappa_1', 'a_W', 'd_shy', 'r_infl', 'ie_f', 'ie_b', 'cPlus_0', 'cMinus_0',
    't_A', 'radius', 'noise')

def parsePedParameter(pedParamSetXml, paramName):
    if not xml.hasElement(pedParamSetXml, paramName, False):
        return
    paramInput = xml.getDistrValue(pedParamSetXml, paramName)

    return ParameterSetInput(**paramInput)

def parsePedParameterSetsXml(pedParamSetsXmlFlNm):
    DOMTree = parse(str(pedParamSetsXmlFlNm))
    pedParamSetsXml = DOMTree.documentElement
    pedParamSetXmls = xml.getElements(pedParamSetsXml, 'pedParameterSet')
    pedParameterSets = []
    for pedParamSetXml in pedParamSetXmls:
        pedParameterSets.append(parsePedParamaterSet(pedParamSetXml))

    return pedParameterSets

def parsePedParamaterSet(pedParamSetXml):
    ID = xml.getAttribute(pedParamSetXml, 'ID')
    params = {}
    if xml.hasAttribute(pedParamSetXml, 'baseSet'):
        params['baseSet'] = xml.getAttribute(pedParamSetXml, 'baseSet')

    for paramName in PARAMETER_NAMES:
        params[paramName] = parsePedParameter(pedParamSetXml, paramName)

    return PedParameterInput(ID, **params)

# ===============================================================================

def parseScenario(scenXml, mainPd):
    SCEN_ELEMENTS = (
        xml.ElInfo('name', xml.STRING_TYPE),
        xml.ElInfo('label', xml.STRING_TYPE),
        xml.ElInfo('duration', xml.FLOAT_TYPE),
        xml.ElInfo('inIsolationTimeStep', xml.FLOAT_TYPE, isOptional=True),
        xml.ElInfo('inRangeStepsPerInIsolationStep', xml.INT_TYPE, isOptional=True),
        xml.ElInfo('inCollisionStepsPerInIsolationStep', xml.INT_TYPE, isOptional=True),
        xml.ElInfo('gridCellSize', xml.FLOAT_TYPE, isOptional=True),
        xml.ElInfo('pedManagerType', xml.STRING_TYPE, isOptional=True),
        xml.ElInfo('accCalcFcnType', xml.STRING_TYPE, isOptional=True),
        xml.ElInfo('logPd', xml.PD_TYPE, isOptional=True, kwargs={'mainPd':mainPd, 'createIfNonExisting':True}),
        xml.ElInfo('logName', xml.STRING_TYPE, isOptional=True),
        xml.ElInfo('logStreamLevel', xml.LOGGER_LEVEL_TYPE, isOptional=True),
    )

    return xml.getElementDict(scenXml, SCEN_ELEMENTS)

def parseDemandManager(scenXml):
    demandManagerXml = xml.getSingleElement(scenXml, 'demandManager')
    activities = parseActivities(demandManagerXml)

    activityPatterns = None
    demandPatterns = None
    scheduler = None

    hasScheduler = False
    for schedulerType, parseFcn in SCHEDULER_TYPES.items():
        if xml.hasElement(demandManagerXml, schedulerType):
            schedulerXml = xml.getSingleElement(demandManagerXml, schedulerType)
            scheduler = parseFcn(schedulerXml)
            hasScheduler = True

    if not hasScheduler:
        activityPatterns = parseActivityPatterns(demandManagerXml)
        demandPatterns = parseDemandPatterns(demandManagerXml)

    return DemandManagerInput(activities, activityPatterns, demandPatterns, scheduler)

RESTAURANT_SCHEDULER_ELEMENTS = (
    xml.ElInfo('demandPattern', xml.NUMERIC_TUPLE_TYPE),
    xml.ElInfo('sittingDestinationGroupIDs', xml.TUPLE_TYPE),
    xml.ElInfo('visitDurationDistr', xml.DISTR_VALUE_TYPE),
    xml.ElInfo('inGroupEntryDistr', xml.DISTR_VALUE_TYPE),
    xml.ElInfo('guestPedParamSetDistr', xml.TUPLE_TYPE),
    xml.ElInfo('useEntranceTimeSlot', xml.BOOL_TYPE),
    xml.ElInfo('entranceTimeSlotDuration', xml.FLOAT_TYPE, isOptional=True),
    xml.ElInfo('toiletDestinationIDs', xml.TUPLE_TYPE, isOptional=True),
    xml.ElInfo('toiletVisitProbability', xml.FLOAT_TYPE, isOptional=True),
    xml.ElInfo('toiletVisitDurationDistr', xml.DISTR_VALUE_TYPE, isOptional=True),
    xml.ElInfo('coatRackDestinationID', xml.STRING_TYPE, isOptional=True),
    xml.ElInfo('coatRackVisitDurationDistr', xml.DISTR_VALUE_TYPE, isOptional=True),
    xml.ElInfo('registerDestinationID', xml.STRING_TYPE, isOptional=True),
    xml.ElInfo('registerVisitDurationDistr', xml.DISTR_VALUE_TYPE, isOptional=True),
    xml.ElInfo('useTablesOnlyOnce', xml.BOOL_TYPE, isOptional=True),
    xml.ElInfo('useTablesOnlyOnce', xml.BOOL_TYPE, isOptional=True),
    xml.ElInfo('staffScheduler', xml.XML_SUB_PART, isOptional=True),
    )

RESTAURANT_STAFF_SCHEDULER_ELEMENTS = (
    xml.ElInfo('baseAreaID', xml.STRING_TYPE),
    xml.ElInfo('staffCount', xml.INT_TYPE),
    xml.ElInfo('pedParamSetID', xml.STRING_TYPE),
    xml.ElInfo('servingsCount', xml.INT_TYPE, isOptional=True),
    xml.ElInfo('neighborhoods', xml.TUPLE_TYPE, isOptional=True),
    )

def parseRestaurantStaffScheduler(staffSchedulerXml):
    inputDict = xml.getElementDict(staffSchedulerXml, RESTAURANT_STAFF_SCHEDULER_ELEMENTS)
    return RestaurantStaffSchedulerInput(**inputDict)

def parseRestaurantScheduler(schedulerXml):
    sourceID, sinkID = parseSchedulerBase(schedulerXml)
    inputDict = xml.getElementDict(schedulerXml, RESTAURANT_SCHEDULER_ELEMENTS)

    inputDict['visitDurationDistr'] = DistrValueInput(**inputDict['visitDurationDistr'])
    inputDict['inGroupEntryDistr'] = DistrValueInput(**inputDict['inGroupEntryDistr'])
    if 'toiletVisitDurationDistr' in inputDict:
        inputDict['toiletVisitDurationDistr'] = DistrValueInput(**inputDict['toiletVisitDurationDistr'])

    if 'coatRackVisitDurationDistr' in inputDict:
        inputDict['coatRackVisitDurationDistr'] = DistrValueInput(**inputDict['coatRackVisitDurationDistr'])

    if 'registerVisitDurationDistr' in inputDict:
        inputDict['registerVisitDurationDistr'] = DistrValueInput(**inputDict['registerVisitDurationDistr'])

    if 'staffScheduler' in inputDict:
        inputDict['staffScheduler'] = parseRestaurantStaffScheduler(inputDict['staffScheduler'])
    
    return RestaurantSchedulerInput(sourceID, sinkID, **inputDict)

def parseSchedulerBase(schedulerXml):
    sourceID = xml.getString(schedulerXml, 'sourceID')
    sinkID = xml.getString(schedulerXml, 'sinkID')

    return sourceID, sinkID

SCHEDULER_TYPES = {
    'restaurantScheduler':parseRestaurantScheduler,
    }

def parseSink(activityXml):
    ID, destinationID, groupID = parseActivityBase(activityXml)
    return SinkInput(ID, destinationID, groupID)

def parseWaypoint(activityXml):
    ID, destinationID, groupID = parseActivityBase(activityXml)
    return WaypointInput(ID, destinationID, groupID)

def parseFixedWaitingTimeActivity(activityXml):
    ID, destinationID, groupID = parseActivityBase(activityXml)
    name = xml.getString(activityXml, 'name')
    duration = xml.getFloat(activityXml, 'waitDuration')
    return FixedWaitingTimeActivityInput(ID, destinationID, groupID, name, duration)

def parseFixedWaitingTimeNonInteractingActivity(activityXml):
    ID, destinationID, groupID = parseActivityBase(activityXml)
    name = xml.getString(activityXml, 'name')
    duration = xml.getFloat(activityXml, 'waitDuration')
    return FixedWaitingTimeNonInteractingActivityInput(ID, destinationID, groupID, name, duration)

def parseEndAtTimeWaitingActivity(activityXml):
    ID, destinationID, groupID = parseActivityBase(activityXml)
    name = xml.getString(activityXml, 'name')
    endtime = xml.getFloat(activityXml, 'endtime')
    return EndAtTimeWaitingActivityInput(ID, destinationID, groupID, name, endtime)

def parseActivityBase(activityXml):
    ID = xml.getAttribute(activityXml, 'ID')
    destinationID = xml.getString(activityXml, 'destinationID')

    groupID = None
    if xml.hasAttribute(activityXml, 'groupID'):
        groupID = xml.getAttribute(activityXml, 'groupID')

    return ID, destinationID, groupID

ACTIVITY_TYPES = {
    'sink':parseSink,
    'waypoint':parseWaypoint,
    'fixedWaitingTimeActivity':parseFixedWaitingTimeActivity,
    'fixedWaitingTimeNonInteractingActivity':parseFixedWaitingTimeNonInteractingActivity,
    'endAtTimeWaitingActivity':parseEndAtTimeWaitingActivity,
    }

def parseActivities(demandManagerXml):
    activities = []
    activitiesXml = xml.getSingleElement(demandManagerXml, 'activities')
    for key, parseFcn in ACTIVITY_TYPES.items():
        activityXmls = xml.getElements(activitiesXml, key)
        for activityXml in activityXmls:
            activities.append(parseFcn(activityXml))

    return activities

def parseListActivityPattern(ID, activityPatternXml):
    pattern = xml.getTupleEntry(xml.getSingleElement(activityPatternXml, 'IDlist'))
    return ListActivityPatternInput(ID, pattern)

ACTIVITY_PATTERN_TYPES = {
    'listActivityPattern':parseListActivityPattern,
    }

def parseActivityPatterns(demandManagerXml):
    activityPatterns = []

    activityPatternsXml = xml.getSingleElement(demandManagerXml, 'activityPatterns')
    for key, parseFcn in ACTIVITY_PATTERN_TYPES.items():
        activityPatternXmls = xml.getElements(activityPatternsXml, key)
        for activityPatternXml in activityPatternXmls:
            ID = xml.getAttribute(activityPatternXml, 'ID')
            activityPatterns.append(parseFcn(ID, activityPatternXml))
    
    return activityPatterns


CONSTANT_DEMAND_PATTERN = 'constantDemandPattern'
VARIABLE_DEMAND_PATTERN = 'variableDemandPattern'
DISCRETE_DEMAND_PATTERN = 'discreteDemandPattern'

DEMAND_PATTERN_TYPES = (CONSTANT_DEMAND_PATTERN, VARIABLE_DEMAND_PATTERN, DISCRETE_DEMAND_PATTERN)

def parseDemandPatterns(demandManagerXml):
    demandPatterns = []

    demandPatternsXml = xml.getSingleElement(demandManagerXml, 'demandPatterns')
    for patternType in DEMAND_PATTERN_TYPES:
        demandPatternXmls = xml.getElements(demandPatternsXml, patternType)
        for demandPatternXml in demandPatternXmls:
            demandPatterns.append(parseDemandPattern(demandPatternXml, patternType))

    return demandPatterns

def parseDemandPattern(demandPatternXml, patternType):
        ID = xml.getAttribute(demandPatternXml, 'ID')
        sourceID = xml.getString(demandPatternXml, 'sourceID')
        activityPatternID = xml.getString(demandPatternXml, 'activityPatternID')
        parameterSetID = xml.getString(demandPatternXml, 'parameterSetID')

        optKwargs = {}
        if xml.hasElement(demandPatternXml, 'startTime'):
            optKwargs['startTime'] = xml.getFloat(demandPatternXml, 'startTime')
        if xml.hasElement(demandPatternXml, 'endTime'):
            optKwargs['endTime'] = xml.getFloat(demandPatternXml, 'endTime')

        if patternType == CONSTANT_DEMAND_PATTERN:
            flowPerSecond = xml.getFloat(demandPatternXml, 'flowPerSecond')
            return ConstantDemandPatternInput(ID, sourceID, activityPatternID, parameterSetID,
                                               flowPerSecond, **optKwargs)
        elif patternType == VARIABLE_DEMAND_PATTERN:
            timeFlowArray = xml.getNumericTuple(demandPatternXml, 'timeFlowArray')
            return VariableDemandPatternInput(ID, sourceID, activityPatternID, parameterSetID,
                                               timeFlowArray, **optKwargs)
        elif patternType == DISCRETE_DEMAND_PATTERN:
            discretePattern = xml.getNumericTuple(demandPatternXml, 'discretePattern')
            return DiscreteDemandPatternInput(ID, sourceID, activityPatternID, parameterSetID,
                                               discretePattern)
        else:
            raise Exception('Unknown demand pattern type "{}"'.format(patternType))

def parseOutputManager(scenXml, pd):
    if xml.hasElement(scenXml, 'basicFileOutputManager') or xml.hasElement(scenXml, 'debugOutputManager'):
        if xml.hasElement(scenXml, 'debugOutputManager'):
            outputManagerXml = xml.getSingleElement(scenXml, 'debugOutputManager')
        else:
            outputManagerXml = xml.getSingleElement(scenXml, 'basicFileOutputManager')
        outputPd = xml.getPd(outputManagerXml, 'outputPd', mainPd=pd, createIfNonExisting=True)
        baseFilename = xml.getString(outputManagerXml, 'baseFilename')
        if xml.hasElement(outputManagerXml, 'bufferSize', recursive=False):
            bufferSize = xml.getInt(outputManagerXml, 'bufferSize')
        else:
            bufferSize = None

        if xml.hasElement(scenXml, 'debugOutputManager'):
            return DebugOutputManagerInput(outputPd, baseFilename, bufferSize)
        return BasicFileOutputManagerInput(outputPd, baseFilename, bufferSize)
    elif xml.hasElement(scenXml, 'inMemoryOutputManager'):
        outputManagerXml = xml.getSingleElement(scenXml, 'inMemoryOutputManager')
        if xml.hasElement(outputManagerXml, 'saveTimeStep'):
            args = (xml.getFloat(outputManagerXml, 'saveTimeStep'),)
        else:
            args = ()

        return InMemoryOutputManagerInput(*args)
    elif xml.hasElement(scenXml, 'dummyOutputManager'):
        return DummyOutputManagerInput()
