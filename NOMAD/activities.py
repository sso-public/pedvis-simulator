""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""
 
from copy import copy

from shapely.geometry.linestring import LineString
from shapely.geometry.multipolygon import MultiPolygon
from shapely.geometry.point import Point
from shapely.geometry.polygon import Polygon
import shapely.ops

from NOMAD.constants import PED_STATIC_STATE, PED_STATIC_NON_INTERACTING_STATE, \
    PED_MOVING_STATE, PED_OUTSIDE_OF_SIM_STATE, WALKING_ACTIVITY_NAME
from NOMAD.general_functions import doListCeck, SpawnableLine
from NOMAD.shapely_subclass import ShapelyGeomClassWrapper
from NOMAD.vectors import Vector2D


# ================================================================================================
# ================================================================================================
def createDestinationFromGeometry(ID, geometry, *args, **kwargs):
    kwargs['geometry'] = geometry
    if isinstance(geometry, Point):
        return PointDestination(ID, *args, **kwargs)
    elif isinstance(geometry, LineString):
        return LineDestination(ID, *args, **kwargs)
    elif isinstance(geometry, Polygon):
        return PolygonDestination(ID, *args, **kwargs)
    elif isinstance(geometry, MultiPolygon):
        return MultiPolygonDestination(ID, *args, **kwargs)
    else:
        shapelyType = geometry.wkt
        raise TypeError(f'Unsupported shapely type "{shapelyType}"')

# ================================================================================================
# ================================================================================================
class Destination(ShapelyGeomClassWrapper):

    def __init__(self, ID, groupID=None, isVirtualObstacle=False, geometry=None):
        '''
        Constructor
        '''
        ShapelyGeomClassWrapper.__init__(self, ID)
        self._localRouteChoiceManager = None
        self._groupID = groupID
        self.isVirtualObstacle = isVirtualObstacle
        self._geometry = geometry

    @property
    def groupID(self):
        return self._groupID

    def setLocalRouteChoiceManager(self, localRouteChoiceManager):
        self._localRouteChoiceManager = localRouteChoiceManager

    def getDesiredDirectionForPedVec(self, ped):
        return Vector2D(self._localRouteChoiceManager.getDesiredDirectionForPed(ped))

    def getDesiredDirectionForPed(self, ped):
        return self._localRouteChoiceManager.getDesiredDirectionForPed(ped)

    def hasBeenReachedByPed(self, ped):
        # Either the current position point lies within the activity
        # or the line between the the previous and current position crosses the activity
        if Point(ped.pos).within(self._geometry):
            return True

        posLine = LineString((ped.prevPos, ped.pos))
        if posLine.crosses(self._geometry):
            return True

        return False

    def getActivityLocation(self, ped):
        raise NotImplementedError('Implement function')

    def getCenterPoint(self):
        return self._geometry.centroid

    def getClosestPoint(self, x, y):
        return shapely.ops.nearest_points(self._geometry, Point((x,y)))

class LineDestination(Destination):

    def __init__(self, ID, lineCoords=None, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, *args, **kwargs)
        if lineCoords is not None:
            self._geometry = LineString(lineCoords)
            
        self._maxPedDistPerStep = None

    def setMaxPedDistPerStep(self, maxPedDistPerStep):
        self._maxPedDistPerStep = maxPedDistPerStep

    def hasBeenReachedByPed(self, ped):
        # Either the current position point lies within the activity
        # or the line between the the previous and current position crosses the activity
        return Point(ped.pos).distance(self._geometry) <= self._maxPedDistPerStep

    def getPolygonRepresentation(self, buffer):
        leftOffset = self._geometry.parallel_offset(buffer, 'left')
        rightOffset = self._geometry.parallel_offset(buffer, 'right')
        return Polygon(list(leftOffset.coords) + list(rightOffset.coords))

    def getActivityLocation(self, ped):
        closestPoints = shapely.ops.nearest_points(self._geometry, Point(ped.pos))
        return list(closestPoints[0].coords)[0]


class PointDestination(Destination):

    def __init__(self, ID, pointCoords=None, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, *args, **kwargs)
        if pointCoords is not None:
            self._geometry = Point(pointCoords)
        self.pointCoords = list( self._geometry.coords)[0]

    def setMaxPedDistPerStep(self, maxPedDistPerStep):
        self._maxPedDistPerStep = maxPedDistPerStep

    def hasBeenReachedByPed(self, ped):
        # Either the current position point lies within the activity
        # or the line between the the previous and current position crosses the activity
        return Point(ped.pos).distance(self._geometry) <= self._maxPedDistPerStep

    def getActivityLocation(self, ped):  # @UnusedVariable
        return self.pointCoords

class PolygonDestination(Destination):

    def __init__(self, ID, polygonCoords=None, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, *args, **kwargs)
        if polygonCoords is not None:
            self._geometry = Polygon(polygonCoords, None)

    def getActivityLocation(self, ped):
        return ped.pos

class CentroidPolygonDestination(PolygonDestination):

    def __init__(self, ID, polygonCoords=None, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, polygonCoords, *args, **kwargs)
        self.center = self._geometry.centroid.coords[0]

    def getActivityLocation(self, ped):  # @UnusedVariable
        return self.center

class MultiPolygonDestination(Destination):

    def __init__(self, ID, multiPolygonCoords=None, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, *args, **kwargs)
        if multiPolygonCoords is not None:
            polygons = []
            for polygonCoords in multiPolygonCoords:
                polygons.append(Polygon(polygonCoords, None))
            self._geometry = MultiPolygon(polygons)

    def getActivityLocation(self, ped):
        return ped.pos
    
# ================================================================================================
# ================================================================================================
class SpawnableLineDestination(LineDestination, SpawnableLine):
    
    def __init__(self, ID, lineCoords=None, *args, **kwargs):
        LineDestination.__init__(self, ID, lineCoords, *args, **kwargs)
        SpawnableLine.__init__(self, ID, lineCoords)
        self._placementLocations = None

    def setPlacementLocationForPeds(self, peds2place):
        self._placementLocations = {}
        locations, maxPeds2placeCount = self.getPlacementLocations(len(peds2place))
        for ii in range(maxPeds2placeCount):
            self._placementLocations[peds2place[ii]] = locations[ii]
        
        return peds2place[maxPeds2placeCount:]

    def getPlacementLocationOfPed(self, ped):
        return Vector2D(self._placementLocations[ped])

    def getPlacementLocationsInSegments(self, lineSegmentArray, placementCount, maxPedDiameter):
        return self.getRandomPlacementLocationsInSegments(lineSegmentArray, placementCount, maxPedDiameter)
  
# ================================================================================================
# ================================================================================================

class Activity():
    '''
    classdocs
    '''

    __slots__ = ['_ID', 'destination', '_name', '_groupID']

    def __init__(self, ID, destination, name, groupID=None):
        '''
        Constructor
        '''
        self._ID = ID
        self.destination = destination
        self._name = name
        self._groupID = groupID        
        
    @property
    def ID(self):
        return self._ID

    @property
    def groupID(self):
        return self._groupID

    @property
    def name(self):
        return self._name

    @property
    def pedStateWhilstPerformingActivity(self):
        raise NotImplementedError('Implement function')

    # --------------------------------------------------------------------
    # Actions
    def actionOnReachingActivity(self, ped, currentTime):
        ped.addNewActivityToLog(currentTime, self._name, self)
        self.actionAfterReachingActivity(ped)

    def actionAfterReachingActivity(self, ped):
        raise NotImplementedError('Implement function')

    def actionOnLeavingActivity(self, ped, currentTime):
        self.actionBeforeLeavingActivity(ped)
        ped.setNextActivity(currentTime)

    def actionBeforeLeavingActivity(self, ped):
        raise NotImplementedError('Implement function')

    def actionWhilstPerformingActivity(self, ped):
        raise NotImplementedError('Implement function')

    # --------------------------------------------------------------------
    # Routing
    def getClosestPoint(self, x, y):
        return self.destination.getClosestPoint(x, y)

    def getDesiredDirectionForPedVec(self, ped):
        return self.destination.getDesiredDirectionForPedVec(ped)

    def getDesiredDirectionForPed(self, ped):
        return self.destination.getDesiredDirectionForPed(ped)

# ================================================================================================
# ================================================================================================

class Sink(Activity):

    __slots__ = ['pedsThatReachedThis']

    def __init__(self, ID, destination, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, destination, 'sink', *args, **kwargs)
        self.pedsThatReachedThis = []

    def actionAfterReachingActivity(self, ped):
        self.pedsThatReachedThis.append(ped)

    def empty(self):
        ped2remove = copy(self.pedsThatReachedThis)
        self.pedsThatReachedThis = []
        return ped2remove

    @property
    def pedStateWhilstPerformingActivity(self):
        return None
# ================================================================================================
# ================================================================================================

class Waypoint(Activity):

    def __init__(self, ID, destination, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, destination, 'waypoint', *args, **kwargs)

    def actionAfterReachingActivity(self, ped):
        pass

    def actionBeforeLeavingActivity(self, ped):
        pass

    @property
    def pedStateWhilstPerformingActivity(self):
        return PED_MOVING_STATE
# ================================================================================================
# ================================================================================================

class TimeBasedActivity(Activity):

    def __init__(self, ID, destination, name, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, destination, name, *args, **kwargs)

    def getEndTime(self, currentTime):
        raise NotImplementedError('Implement function')

class EventBasedActivity(Activity):

    def __init__(self, ID, destination, name):
        '''
        Constructor
        '''
        super().__init__(ID, destination, name)

# ================================================================================================
# ================================================================================================

class StaticInteractingActivity(Activity):

    def __init__(self, ID, destination, name, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, destination, name, *args, **kwargs)

    @property
    def pedStateWhilstPerformingActivity(self):
        return PED_STATIC_STATE

    def actionAfterReachingActivity(self, ped):
        pass

    def actionBeforeLeavingActivity(self, ped):
        pass

    def actionWhilstPerformingActivity(self, ped):
        pass

class StaticNonInteractingActivity(Activity):

    def __init__(self, ID, destination, name, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, destination, name, *args, **kwargs)
    
    @property
    def pedStateWhilstPerformingActivity(self):
        return PED_STATIC_NON_INTERACTING_STATE

    def actionAfterReachingActivity(self, ped):
        pass

    def actionBeforeLeavingActivity(self, ped):
        pass
    
    def actionWhilstPerformingActivity(self, ped):
        pass

class DynamicInteractingActivity(Activity):

    def __init__(self, ID, destination, name, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, destination, name, *args, **kwargs)

    @property
    def pedStateWhilstPerformingActivity(self):
        return PED_MOVING_STATE

    def actionAfterReachingActivity(self, ped):
        pass

    def actionOnLeavingActivity(self, ped):
        pass

    def actionWhilstPerformingActivity(self, ped):
        pass

class OutsideOfSimulationActivity(Activity):

    def __init__(self, ID, destination, name, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, destination, name, *args, **kwargs)

    @property
    def pedStateWhilstPerformingActivity(self):
        return PED_OUTSIDE_OF_SIM_STATE

    def actionAfterReachingActivity(self, ped):
        pass

    def actionOnLeavingActivity(self, ped, currentTime):
        super().actionOnLeavingActivity(ped, currentTime)
        if ped.state == PED_OUTSIDE_OF_SIM_STATE:
            self.spawnPed(ped)

    def actionBeforeLeavingActivity(self, ped):
        pass

    def actionWhilstPerformingActivity(self, ped):
        pass
    
    def spawnPed(self, ped):
        pos = self.destination.getPlacementLocationOfPed(ped)
        ped.setInitialPosAndVel(pos)

# ================================================================================================
# ================================================================================================

# All activities have two superclasses which have to be inherited in a specific order, namely:
# 1) Either the TimeBasedActivity OR the EventBasedActivity
# 2) Either the StaticInteractingActivity OR the StaticNonInteractingActivity OR the DynamicInteractingActivity
# For example
# class SomeActivity(EventBasedActvity, StaticInteractingActivity):
# class SomeActivity(TimeBasedActivity, DynamicInteractingActivity):

class FixedWaitingTimeActivity(StaticInteractingActivity, TimeBasedActivity):

    def __init__(self, ID, destination, name, activityDuration, *args, **kwargs):
        super().__init__(ID, destination, name, *args, **kwargs)
        self.duration = activityDuration

    def getEndTime(self, currentTime):
        return currentTime + self.duration

    def actionAfterReachingActivity(self, ped):
        super().actionAfterReachingActivity(ped)
        # Move to the center point. Can later be extended to show more natural behavior
        ped.pos = self.destination.getActivityLocation(ped)
        ped.vel = (0,0)

class EndAtTimeWaitingActivity(StaticInteractingActivity, TimeBasedActivity):

    def __init__(self, ID, destination, name, endTime, *args, **kwargs):
        super().__init__(ID, destination, name, *args, **kwargs)
        self.endTime = endTime

    def getEndTime(self, _):
        return self.endTime

    def actionAfterReachingActivity(self, ped):
        super().actionAfterReachingActivity(ped)
        # Move to the center point. Can later be extended to show more natural behavior
        ped.pos = self.destination.getActivityLocation(ped)
        ped.vel = (0,0)

class FixedWaitingTimeNonInteractingActivity(StaticNonInteractingActivity, TimeBasedActivity):

    def __init__(self, ID, destination, name, activityDuration, *args, **kwargs):
        super().__init__(ID, destination, name, *args, **kwargs)
        self.duration = activityDuration

    def getEndTime(self, currentTime):
        return currentTime + self.duration

    def actionAfterReachingActivity(self, ped):
        super().actionAfterReachingActivity(ped)
        # Move to the center point. Can later be extended to show more natural behavior
        ped.pos = self.destination.getActivityLocation(ped)
        ped.vel = (0,0)

class SchedulerEndAtTimeWaitingActivity(EndAtTimeWaitingActivity):

    def __init__(self, ID, destination, name, endTime, dynamicActivityScheduler, *args, **kwargs):
        super().__init__(ID, destination, name, endTime, *args, **kwargs)
        self._scheduler = dynamicActivityScheduler

    def actionAfterReachingActivity(self, ped):
        super().actionAfterReachingActivity(ped)
        self._scheduler.pedArrivedAtActivity(self, ped)
        


class FixedTimeOutsideOfSimulationActivity(OutsideOfSimulationActivity, TimeBasedActivity):

    def __init__(self, ID, destination, name, activityDuration, *args, **kwargs):
        super().__init__(ID, destination, name, *args, **kwargs)
        self._name = name
        self.duration = activityDuration
        
    def getEndTime(self, currentTime):
        return currentTime + self.duration

class EventOutsideOfSimulationActivity(OutsideOfSimulationActivity, EventBasedActivity):

    def __init__(self, ID, destination, name, *args, **kwargs):
        super().__init__(ID, destination, name, *args, **kwargs)
        self._name = name
        
class InitialEventOutsideOfSimulationActivity(EventOutsideOfSimulationActivity):

    def __init__(self, ID, destination, name, *args, **kwargs):
        super().__init__(ID, destination, name, *args, **kwargs)

    def getDesiredDirectionForPedVec(self, _):
        return Vector2D(0,0)

    def getDesiredDirectionForPed(self, _):
        return (0,0)   
      
# ================================================================================================
# ================================================================================================

class ActivityPattern():

    def __init__(self, ID):
        '''
        Constructor
        '''
        self._ID = ID

    @property
    def ID(self):
        return self._ID

    def getNextActivity(self, ped):
        raise NotImplementedError('Implement method')

    def getInitialActivity(self, ped):
        # Return the activity and the activity name
        raise NotImplementedError('Implement method')
    

class ListActivityPattern(ActivityPattern):
    
    def __init__(self, ID, activityList):
        '''
        Constructor
        '''
        super().__init__(ID)
        self._activityList = doListCeck(activityList)
        self._activityIndPerPed = {}

    def getNextActivity(self, ped):
        newActivityInd = self._activityIndPerPed[ped] + 1
        self._activityIndPerPed[ped] = newActivityInd
        
        return self._activityList[newActivityInd] 
    
    def getInitialActivity(self, ped):
        newActivityInd = 0
        self._activityIndPerPed[ped] = newActivityInd
        return  self._activityList[newActivityInd], WALKING_ACTIVITY_NAME 
    
class SchedulerActivityPattern(ActivityPattern):
    
    def __init__(self, ID, scheduler):
        '''
        Constructor
        '''
        super().__init__(ID)
        self._scheduler = scheduler

    def getNextActivity(self, ped):
        return self._scheduler.getNextActivityForPed(ped)
    
    def getInitialActivity(self, ped):
        return self._scheduler.getInitialActivityForPed(ped)
    
class ActivityPatternScheduler():
    
    def __init__(self, ID):
        '''
        Constructor
        '''
        self._ID = ID

    @property
    def ID(self):
        return self._ID

    def getNextActivityForPed(self, ped):
        raise NotImplementedError('Implement method') 
    