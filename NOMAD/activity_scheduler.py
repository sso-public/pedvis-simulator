""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

from _collections import defaultdict
from copy import copy
from dataclasses import dataclass, field
import logging
from math import inf, nan

from shapely.ops import unary_union

import NOMAD
from NOMAD.activities import EndAtTimeWaitingActivity, \
    FixedWaitingTimeNonInteractingActivity, FixedWaitingTimeActivity, \
    createDestinationFromGeometry, ListActivityPattern, ActivityPatternScheduler, \
    SchedulerEndAtTimeWaitingActivity, SchedulerActivityPattern, \
    InitialEventOutsideOfSimulationActivity, EventOutsideOfSimulationActivity, \
    FixedTimeOutsideOfSimulationActivity
from NOMAD.demand_manager import DiscreteDemandPattern
from NOMAD.general_functions import getAnalyticalMean
from NOMAD.pedestrian_manager import PedCreationInfo
from NOMAD.walk_level import getObstacle
import numpy as np


logger = logging.getLogger(__name__)

STAFF_GROUP_NAME = 'staff'

SITTING_ACTIVITY_NM = 'sitting'
TOILET_ACTIVITY_NM = 'toilet'
HANGING_COAT_ACTIVITY_NM = 'hangingCoat'
COLLECTING_COAT_ACTIVITY_NM = 'collectingCoat'
PAY_AT_REGISTER_ACTIVITY_NM = 'payingAtRegister'
TAKING_ORDER_ACTIVITY_NM = 'takingOrder'
COLLECTING_ORDER_ACTIVITY_NM = 'collectingOrder'
CHECKING_OUT_ACTIVITY_NM = 'checkingOut'
SERVING_ORDER_ACTIVITY_NM = 'servingOrder'
CLEANING_ACTIVITY_NM = 'cleaning'
WAIT_AT_BASE_AREA_ACTIVITY_NM = 'waitAtBaseArea'
INIT_WAIT_AT_BASE_AREA_ACTIVITY_NM = 'initWaitAtBaseArea'

SERVING_ACTIVITY_BUFFER = 0.5 #m

SERVE_ACTIVITY = 'serveActivity'
TAKE_ORDERS_ACTIVITY = 'takeOrdersActivity'
CHECK_OUT_ACTIVITY = 'checkOutActivity'
CLEAN_ACTIVITY = 'cleanActivity'

TAKE_ORDERS_ACTIVITY_DURATION = 2*60 # seconds
CHECK_OUT_ACTIVITY_DURATION = 60 # seconds
CHECK_OUT_ACTIVITY_BUFFER = 3*60 # seconds
WALKING_BUFFER = 45 # seconds
CLEAN_ACTIVITY_DURATION = 2.5*60 # seconds
SERVE_ORDER_ACTIVITY_DURATION = 0.5*60 # seconds
COLLECT_ORDER_ACTIVITY_DURATION = 0.5*60 # seconds
SERVE_ACTIVITY_START_BUFFER = 5*60 # seconds
SERVE_ACTIVITY_END_BUFFER = 3*60 # seconds
SERVE_ACTIVITY_BUFFER = 2*60 # seconds
SERVE_ACTIVITY_COUNT = 2

INTER_GROUP_ENTRY_BUFFER = 5 #seconds
INTRA_GROUP_ENTRY_BUFFER = 1.5 #seconds

INTRA_TABLE_GROUP_BUFFER = CLEAN_ACTIVITY_DURATION #seconds

TIME_PERIOD_BUFFER = 60 # s

BASE_PRIORITY = 2

MAX_QUEUE_LENGTH = 2
MAX_QUEUE_TIME = 2*60 # [s]

def createRestaurantScheduler(schedulerInput, destinations, pedParameterSets, sources, activities, walkLevels):
    restaurantScheduler = RestaurantScheduler(schedulerInput, destinations, pedParameterSets, sources, activities, walkLevels)
    restaurantScheduler._createTables2destinations()
    return restaurantScheduler

class RestaurantScheduler():

    def __init__(self, schedulerInput, destinations, pedParameterSets, sources, activities, walkLevels):
        self.schedulerInput = schedulerInput
        self.destinations = destinations
        self.pedParameterSets = pedParameterSets
        self.sources = sources
        self.walkLevels = walkLevels
        self.sink = activities[self.schedulerInput.sinkID]
        
        self.staffDynamicScheduler = None        
       
        self.firstSittingActivityType = EndAtTimeWaitingActivity
        self.firstSittingActivityArgs = ()
               
    def _createTables2destinations(self):
        self.tables2destinations = self._getTables()
               
    def initialize(self):        
        self.groups, _ = self._createGroups()
  
        if self.schedulerInput.staffScheduler is not None:
            staffSchedulerInput = self.schedulerInput.staffScheduler
            baseDestinationWalkLevel = None           
            for walkLevel in self.walkLevels.values():
                if walkLevel.hasDestination(staffSchedulerInput.baseAreaID):
                    baseDestinationWalkLevel = walkLevel
                    break            
                                
            servingDestinations = createServingDestinations(self.tables2destinations, self.walkLevels)
            baseDestination = self.destinations[staffSchedulerInput.baseAreaID]
            staffPedParameterSet = self.pedParameterSets[staffSchedulerInput.pedParamSetID]
            
            self.staffDynamicScheduler = StaffDynamicScheduler('staffScheduler', baseDestination, 
                                                               baseDestinationWalkLevel, servingDestinations, 
                                                               staffSchedulerInput.staffCount, staffPedParameterSet, 
                                                               staffSchedulerInput.servingsCount, staffSchedulerInput.neighborhoods,
                                                               self.tables2destinations, self.groups)
            self.firstSittingActivityType = SchedulerEndAtTimeWaitingActivity
            self.firstSittingActivityArgs = (self.staffDynamicScheduler, )     
    
            
    def createDemandPatterns(self):        
        customerDemandPatterns = self.createCustomerDemandPatterns()
        
        return customerDemandPatterns
    
    def _getTables(self):
        tables2destinations = {}
        for groupID in self.schedulerInput.sittingDestinationGroupIDs:
            tables2destinations[groupID] = []
    
        for destination in self.destinations.values():
            if destination.groupID in tables2destinations:
                tables2destinations[destination.groupID].append(destination)
    
        return tables2destinations
  
    def _createGroups(self):
        groups, groupsPerTable = self._createGroupsAndAssignToTable()
        
        for groupsAtTable in groupsPerTable.values():
            self._assignStartAndEndTimeBoundariesPerTable(groupsAtTable)

        # Set the actual start and end times    
        for group in groups:
            maxVisitDuration = group.endTimeBoundary - group.startTimeBoundary - INTRA_TABLE_GROUP_BUFFER
            visitDuration = min(getDistrValue(self.schedulerInput.visitDurationDistr), maxVisitDuration)
            if visitDuration < maxVisitDuration:
                group.startTime = min(group.startTimeBoundary + NOMAD.NOMAD_RNG.uniform(0, maxVisitDuration - visitDuration), 
                                      group.tEndPeriod - TIME_PERIOD_BUFFER) 
            else:
                group.startTime = group.startTimeBoundary
            group.endTime = group.startTime + visitDuration
            
        return groups, groupsPerTable
    
    def _createGroupsAndAssignToTable(self):
        groups = []
        
        table2size = {groupID:len(value) for groupID, value in self.tables2destinations.items()}

        endTime2table = defaultdict(list)
        endTime2table[0] = list(self.tables2destinations.keys())
        groupInd = 0
        self.meanVisitDuration = getMeanVisitDuration(self.schedulerInput)

        groupsPerTable = {tableID:[] for tableID in self.tables2destinations.keys()}
        
        demandPattern = sortDemandPattern(self.schedulerInput.demandPattern)
        
        for timePeriodInfo in demandPattern:
            tStartPeriod = timePeriodInfo[0]
            tEndPeriod = timePeriodInfo[1]
            groupCount = timePeriodInfo[2]
            
            # Get table becoming free in this period
            freeTables = defaultdict(list)
            for time, tableIDs in endTime2table.items():
                if time < tEndPeriod:
                    freeTables[time] = copy(tableIDs)
                  
            for ii in range(groupCount):
                if len(freeTables) == 0:
                    logger.warning(f'Cannot find a free table for {groupCount-ii} of the {groupCount} groups entering at between {tStartPeriod} and {tEndPeriod}')
                    break
                
                # Sort the times that a table frees up and select a table from the first on
                firstFreeingUpTime = min(freeTables.keys())
                tableIDs = freeTables[firstFreeingUpTime]
                
                if len(tableIDs) == 1:
                    tableID = freeTables[firstFreeingUpTime][0]
                else:
                    tableID = tableIDs[NOMAD.NOMAD_RNG.integers(0, len(tableIDs))]
                    
                earliestStartTime = max(firstFreeingUpTime, tStartPeriod)
                earliestEndTime = earliestStartTime + self.meanVisitDuration
                freeTables[firstFreeingUpTime].remove(tableID)
                endTime2table[firstFreeingUpTime].remove(tableID)
                
                if len(freeTables[firstFreeingUpTime]) == 0:
                    freeTables.pop(firstFreeingUpTime)
                if len(endTime2table[firstFreeingUpTime]) == 0:
                    endTime2table.pop(firstFreeingUpTime)                    
                if not self.schedulerInput.useTablesOnlyOnce:
                    if earliestEndTime < tEndPeriod:
                        freeTables[earliestEndTime].append(tableID) # For this loop
                    endTime2table[earliestEndTime].append(tableID)  # For the next loop
                                
                groupID = f'Group_{groupInd:04d}'
                groupInd += 1 
                groupInfo = GroupInfo(groupID, earliestStartTime, earliestEndTime, table2size[tableID], tableID, tStartPeriod, tEndPeriod)           
                groups.append(groupInfo)
                groupsPerTable[tableID].append(groupInfo)

        return groups, groupsPerTable
    
    def _assignStartAndEndTimeBoundariesPerTable(self, groupsAtTable):
        if len(groupsAtTable) == 0:
            return
        gapIndices = _getGapsBetweenTableOccupancy(groupsAtTable)
        if len(gapIndices) == 0:
            for group in groupsAtTable:
                group.startTimeBoundary = group.earliestStartTime
                group.endTimeBoundary = group.earliestEndTime
            return
    
        previousGapIndex = -1
        for gapIndex in gapIndices:
            if gapIndex == len(groupsAtTable) - 1:
                gapSize = groupsAtTable[-1].tEndPeriod - groupsAtTable[-1].earliestEndTime
            elif groupsAtTable[gapIndex].earliestEndTime <= groupsAtTable[gapIndex].tEndPeriod:
                gapSize = groupsAtTable[gapIndex].tEndPeriod - groupsAtTable[gapIndex].earliestEndTime
            else:
                gapSize = groupsAtTable[gapIndex+1].earliestStartTime - groupsAtTable[gapIndex].earliestEndTime
                                
            groupCount = gapIndex - previousGapIndex
            gapSizePerGroup = gapSize/groupCount
            baseGroup = groupsAtTable[previousGapIndex + 1]
            baseGroup.startTimeBoundary = baseGroup.earliestStartTime 
            baseGroup.endTimeBoundary = baseGroup.earliestEndTime + gapSizePerGroup
            
            for ii in range(1,groupCount):
                baseGroup = groupsAtTable[previousGapIndex + ii]
                nextGroup = groupsAtTable[previousGapIndex + ii + 1]
                newStartTimeBoundary = baseGroup.endTimeBoundary
                if newStartTimeBoundary >= nextGroup.tEndPeriod:
                    gapReduction = newStartTimeBoundary - nextGroup.tEndPeriod + TIME_PERIOD_BUFFER
                    newStartTimeBoundary -= gapReduction
                    baseGroup.endTimeBoundary = newStartTimeBoundary
                    gapSizePerGroup += gapReduction/(groupCount - ii)
                    logger.debug(f'Group "{nextGroup.ID}" has a start time boundary after the end of its time period!')
                    
                nextGroup.startTimeBoundary = newStartTimeBoundary
                nextGroup.endTimeBoundary = newStartTimeBoundary + self.meanVisitDuration + gapSizePerGroup
   
            previousGapIndex = gapIndex
            
        if gapIndices[-1] != len(groupsAtTable) - 1:
            for ii in range(previousGapIndex + 1, len(groupsAtTable)):
                groupsAtTable[ii].startTimeBoundary = groupsAtTable[ii].earliestStartTime
                groupsAtTable[ii].endTimeBoundary = groupsAtTable[ii].earliestEndTime
    
    # ================================================================================================
    # ================================================================================================
        
    def createCustomerDemandPatterns(self):
        demandPatterns = {}

        pedParameterSetIDs = []
        pedParameterSetProbabilities = []

        for entry in self.schedulerInput.guestPedParamSetDistr:
            pedParameterSetIDs.append(entry[0])
            pedParameterSetProbabilities.append(float(entry[1]))

        pedParameterSetProbabilities = np.array(pedParameterSetProbabilities)/np.sum(pedParameterSetProbabilities)

        if self.schedulerInput.toiletDestinationIDs is not None:
            self.freeToiletIDs = list(self.schedulerInput.toiletDestinationIDs)
        else:
            self.freeToiletIDs = []
        self.occupiedToilets = {}

        groupsPayAtRegister = self.schedulerInput.registerDestinationID is not None
        
        entryTimes = self.calcEntryTimes()

        for group in self.groups:
            chairIndices = NOMAD.NOMAD_RNG.permutation(range(group.size))
            for ii in range(group.size):
                entryTime = entryTimes[group.ID][ii]
                
                # Create activity pattern (Chair, Sink)
                # Create sitting activity
                activityList = self.getActivityList(chairIndices, entryTime, group, ii, (groupsPayAtRegister and ii == 0))
                activityPatternID = 'AP_{}_{}'.format(group.ID, ii)
                activityPattern = ListActivityPattern(activityPatternID, activityList)
                # Create discrete demand pattern
                demandPatternID = 'DP_{}_{}'.format(group.ID, ii)
                discretePattern = [[entryTime, 1]]
                pedParameterSet = self.pedParameterSets[NOMAD.NOMAD_RNG.choice(pedParameterSetIDs, 1, p=pedParameterSetProbabilities)[0]]
                demandPattern = DiscreteDemandPattern(demandPatternID, self.sources[self.schedulerInput.sourceID],
                                                      pedParameterSet, activityPattern, discretePattern, groupID=group.ID)
                demandPatterns[demandPatternID] = demandPattern

        return demandPatterns

    def calcEntryTimes(self):
        entryTimes = {}
        entryTimesArray = []
        entryTimesGroupIDs = []
        for group in self.groups:
            entryTimes[group.ID] = []
            for _ in range(group.size):
                entryTimesArray.append(group.startTime + getDistrValue(self.schedulerInput.inGroupEntryDistr))
                entryTimesGroupIDs.append(group.ID)
                
        entryTimesArray = np.array(entryTimesArray)        
        sortedInd = np.argsort(entryTimesArray)
        entryTimesArray = entryTimesArray[sortedInd]
        entryTimesGroupIDs = [entryTimesGroupIDs[ind] for ind in sortedInd]
        
        entryTimes[entryTimesGroupIDs[0]].append(entryTimesArray[0])
        for ii in range(1,len(entryTimesGroupIDs)):
            entryTimeDiff = entryTimesArray[ii] - entryTimesArray[ii-1]
            if entryTimesGroupIDs[ii] != entryTimesGroupIDs[ii-1]:
                minDiff = INTER_GROUP_ENTRY_BUFFER
            else:
                minDiff = INTRA_GROUP_ENTRY_BUFFER
                
            if entryTimeDiff < minDiff:
                toAdd = minDiff - entryTimeDiff
                entryTimesArray[ii:] = entryTimesArray[ii:] + toAdd   
                    
            entryTimes[entryTimesGroupIDs[ii]].append(entryTimesArray[ii])
        return entryTimes

    def getActivityList(self, chairIndices, entryTime, group, ind, payAtRegister=False):
        activityList = []

        useCoatRack = self.schedulerInput.coatRackDestinationID is not None
        if useCoatRack:
            coatHangingActivityID = 'Hanging_coat_{}_{}'.format(group.ID, ind)
            coatRackDestination = self.destinations[self.schedulerInput.coatRackDestinationID]
            coatRackActivityDuration = getDistrValue(self.schedulerInput.coatRackVisitDurationDistr)
            activityList.append(FixedWaitingTimeActivity(coatHangingActivityID, coatRackDestination,
                                                         HANGING_COAT_ACTIVITY_NM, coatRackActivityDuration))

        sittingActivityID = 'Sitting_{}_{}'.format(group.ID, ind)
        sittingDestination = self.tables2destinations[group.tableID][chairIndices[ind]]

        if useToilet(self.schedulerInput):
            self.addToiletVisit(activityList, entryTime, group, ind, sittingActivityID, sittingDestination)
        else:
            activityList.append(self.firstSittingActivityType(sittingActivityID, sittingDestination, SITTING_ACTIVITY_NM, group.endTime, *self.firstSittingActivityArgs))

        if payAtRegister:
            payAtRegisterActivityID = 'Pay_at_register_{}_{}'.format(group.ID, ind)
            registerDestination = self.destinations[self.schedulerInput.registerDestinationID]
            payActivityDuration = getDistrValue(self.schedulerInput.registerVisitDurationDistr)
            activityList.append(FixedWaitingTimeActivity(payAtRegisterActivityID, registerDestination,
                                                         PAY_AT_REGISTER_ACTIVITY_NM, payActivityDuration))

        if useCoatRack:
            coatHangingActivityID = 'Collecting_coat_{}_{}'.format(group.ID, ind)
            activityList.append(FixedWaitingTimeActivity(coatHangingActivityID, coatRackDestination,
                                                         COLLECTING_COAT_ACTIVITY_NM, coatRackActivityDuration))

        activityList.append(self.sink)

        return activityList

    def addToiletVisit(self, activityList, entryTime, group, ind, sittingActivityID, sittingDestination):
        # Create toilet activity
        visitDuration = getDistrValue(self.schedulerInput.toiletVisitDurationDistr)
        startAddFactor = 60
        endSubtractFactor = 300

        if entryTime + startAddFactor <= group.endTime - visitDuration - endSubtractFactor:
            endSubtractFactor = startAddFactor

        startTime = NOMAD.NOMAD_RNG.uniform(entryTime + startAddFactor, group.endTime - visitDuration - endSubtractFactor)
        toiletActivityID = 'Toilet_{}_{}'.format(group.ID, ind)
        toiletDestination, updatedStartTime = self.getToiletDestination(startTime, visitDuration)

        if updatedStartTime > group.endTime:
            activityList.append(self.firstSittingActivityType(sittingActivityID, sittingDestination, SITTING_ACTIVITY_NM, group.endTime, *self.firstSittingActivityArgs))
        elif updatedStartTime + visitDuration + 20 >= group.endTime:
            activityList.append(self.firstSittingActivityType(sittingActivityID, sittingDestination, SITTING_ACTIVITY_NM, updatedStartTime, *self.firstSittingActivityArgs))
            activityList.append(FixedWaitingTimeNonInteractingActivity(toiletActivityID, toiletDestination, TOILET_ACTIVITY_NM, visitDuration))
        else:
            activityList.append(self.firstSittingActivityType(sittingActivityID, sittingDestination, SITTING_ACTIVITY_NM, updatedStartTime, *self.firstSittingActivityArgs))
            activityList.append(FixedWaitingTimeNonInteractingActivity(toiletActivityID, toiletDestination, TOILET_ACTIVITY_NM, visitDuration))
            activityList.append(EndAtTimeWaitingActivity(sittingActivityID, sittingDestination, SITTING_ACTIVITY_NM, group.endTime))

    def getToiletDestination(self, startTime, visitDuration):
        updateFreeDestination(self.freeToiletIDs, self.occupiedToilets, startTime)

        if len(self.freeToiletIDs) == 0:
            toiletDestinationID = None
            minEndTime = inf
            for toiletID, endTime in self.occupiedToilets.items():
                if endTime < minEndTime:
                    toiletDestinationID = toiletID
                    minEndTime = endTime
            startTime = minEndTime + 30
        else:
            toiletInd = NOMAD.NOMAD_RNG.integers(0, len(self.freeToiletIDs))
            toiletDestinationID = self.freeToiletIDs[toiletInd]
            self.freeToiletIDs.remove(toiletDestinationID)

        self.occupiedToilets[toiletDestinationID] = startTime + visitDuration

        return self.destinations[toiletDestinationID], startTime

# ================================================================================================
# ================================================================================================
class StaffDynamicScheduler(ActivityPatternScheduler):
    
    def __init__(self, ID, baseDestination, baseDestinationWalkLevel, servingDestinations, staffCount, pedParamSet, servingsCount, neighborhoodsInput, tables2destinations, groups):
        super().__init__(ID)
        self._baseDestination = baseDestination
        self._baseDestinationWalkLevel = baseDestinationWalkLevel
        self._servingDestinations = servingDestinations
        
        self._staffCount = staffCount
        self._pedParamSet = pedParamSet
        self._servingsCount = servingsCount
        
        self._tables2destinations = tables2destinations
        if not isinstance(groups, dict):
            self._groups = {group.ID: group for group in groups}
        else:
            self._groups = groups        
        
        self._createNeighborhoods(neighborhoodsInput)
                
        self._serverPool = None
        self._idleServers = None
        self._staff2neighborhoods = None 
        self._assignedActivities = None
        self._chainedActivities = None
        self._activeGroups = None        
        self._timeInfo = None        
        self._nextEventTimeInd = None  
        self._nextEventTimeIndices = None
        self._activityIndCounter = None
        self._maxQueueLengthIndices = None
        self._maxQueueTimeIndices = None
        
    def _createNeighborhoods(self, neighborhoodsInput):
        ind = 0
        neighborhoods = []
        if len(neighborhoodsInput) == 0:
            neighborhoodID = getNeighborhoodID(ind)
            neighborhoods.append(Neighborhood(neighborhoodID,
                              list(self._tables2destinations.keys()),
                              sum([len(val) for val in self._tables2destinations.values()])))
            table2neighborhood = {tableID: neighborhoods[0] for tableID in self._tables2destinations}
        else:
            
            table2neighborhood = {}
            for tableIDs in neighborhoodsInput:
                neighborhoodID = getNeighborhoodID(ind)
                neighborhood = Neighborhood(neighborhoodID, tableIDs,
                                            sum([len(self._tables2destinations[tableID]) for tableID in tableIDs]))
                neighborhoods.append(neighborhood)
                for tableID in tableIDs:
                    if tableID in table2neighborhood:
                        raise Exception(f'Table {tableID} is assigned to more than one neighborhood!') 
                    table2neighborhood[tableID] = neighborhood
                    
                ind += 1
        
        self._neighborhoodCount = len(neighborhoods)    
        self._neighborhoods = neighborhoods
        self._table2neighborhood = table2neighborhood
        
        self._neighborhoodIndex2neighborhood = {ii:neighborhoods[ii] for ii in range(self._neighborhoodCount)}
        self._neighborhood2Index = {neighborhood:maxQueueIndex for maxQueueIndex, neighborhood in self._neighborhoodIndex2neighborhood.items()}
        
    def initialize(self, pedestrianManager, timeInfo):
        self._serverPool = []
        self._idleServers = []
        
        self._assignedActivities = {}
        self._chainedActivities = {}
        
        self._activeGroups = [] 

        self._activityIndCounter = 0 
        self._nextEventTimeInd = None
        self._nextEventTimeIndices = set()

        self._initWaitAtBaseAreaActivity = InitialEventOutsideOfSimulationActivity('Init_wait_at_base', self._baseDestination, INIT_WAIT_AT_BASE_AREA_ACTIVITY_NM)
        self._waitAtBaseAreaActivity = EventOutsideOfSimulationActivity('Wait_at_base', self._baseDestination, WAIT_AT_BASE_AREA_ACTIVITY_NM)
  
        self._timeInfo = timeInfo
        self.createPedestrians(pedestrianManager)
        self._assignStaff2Neighborhoods()
        
        self._maxQueueLengthIndices = [inf for _ in range(self._neighborhoodCount)]
        self._maxQueueTimeIndices = [inf for _ in range(self._neighborhoodCount)]
        
    def getPedsAddedAtInit(self):
        return self._serverPool
        
    def createPedestrians(self, pedestrianManager):
        pedInfo = PedCreationInfo(self._pedParamSet, SchedulerActivityPattern('staff_activity_pattern', self))
        pedInfo.initialPos = (nan, nan)
        pedInfo.initialWalkLevel = self._baseDestinationWalkLevel
        pedInfo.groupID = STAFF_GROUP_NAME
        for _ in range(self._staffCount):
            ped = pedestrianManager.createPedestrianOutsideOfSimulation(pedInfo)
            self.addStaffMember(ped)
            
    def _assignStaff2Neighborhoods(self):
        self._staff2neighborhoods = defaultdict(list)
        if self._neighborhoodCount == 1:
            for ii in range(self._staffCount):
                self._staff2neighborhoods[self._serverPool[ii]].append(self._neighborhoods[0])
        elif self._neighborhoodCount == self._staffCount:
            for ii in range(self._staffCount):
                self._staff2neighborhoods[self._serverPool[ii]].append(self._neighborhoods[ii])
        elif self._neighborhoodCount < self._staffCount:
            sortedNeighborhoods = sorted(self._neighborhoods, key=lambda neighborhood: neighborhood.seatCount, reverse=True)
            neighborhoods2staffCount = {}
            for ii in range(self._neighborhoodCount):
                self._staff2neighborhoods[self._serverPool[ii]].append(sortedNeighborhoods[ii])
                neighborhoods2staffCount[sortedNeighborhoods[ii]] = 1
                
            while len(self._staff2neighborhoods) < self._staffCount:
                maxNeighborhoodSizeStaffRatio = 0
                largestNeighborhood = None
                for neighborhood in sortedNeighborhoods:
                    neighborhoodSizeStaffRatio = neighborhood.seatCount/neighborhoods2staffCount[neighborhood]
                    if neighborhoodSizeStaffRatio > maxNeighborhoodSizeStaffRatio:
                        maxNeighborhoodSizeStaffRatio = neighborhoodSizeStaffRatio
                        largestNeighborhood = neighborhood
            
                staffInd = len(self._staff2neighborhoods)
                neighborhoods2staffCount[largestNeighborhood] += 1
                self._staff2neighborhoods[self._serverPool[staffInd]].append(largestNeighborhood)                
        else:
            # self._neighborhoodCount > self._staffCount:
            for ii in range(self._staffCount):
                self._staff2neighborhoods[self._serverPool[ii]].append(self._neighborhoods[ii])
                
            servers2pickFrom = [server.ID for server in self._serverPool] 
            for ii in range(self._staffCount, self._neighborhoodCount):
                serverID = servers2pickFrom[NOMAD.NOMAD_RNG.integers(0, len(servers2pickFrom))]
                self._staff2neighborhoods[self._serverPool[serverID]].append(self._neighborhoods[ii])
                servers2pickFrom.remove(serverID)
                if len(servers2pickFrom) == 0:
                    servers2pickFrom = [server.ID for server in self._serverPool]
                     
        for ped, neighborhoods in self._staff2neighborhoods.items():
            for neighborhood in neighborhoods:
                neighborhood.staff.append(ped)
                        
    def addStaffMember(self, ped):
        self._serverPool.append(ped)
        self._idleServers.append(ped)
        
    def getNextActivityForPed(self, ped):
        if ped in self._assignedActivities:
            nextActivity = self._assignedActivities[ped]
            self._assignedActivities.pop(ped)
        elif ped in self._chainedActivities:
            activityList = self._chainedActivities[ped]
            nextActivity = activityList[0]
            activityList.remove(nextActivity)
            if len(activityList) == 0:
                self._chainedActivities.pop(ped)
        else:
            nextActivity = self._getActivityFromPool(ped)
            if nextActivity is None:
                self._idleServers.append(ped)
                self._setNextTimeInd(ped)
                return self._waitAtBaseAreaActivity
            
        return nextActivity        

    def getInitialActivityForPed(self, _):
        return self._initWaitAtBaseAreaActivity, INIT_WAIT_AT_BASE_AREA_ACTIVITY_NM

    def pedArrivedAtActivity(self, activity, ped):
        if ped.groupID in self._activeGroups:
            return
        
        self._activeGroups.append(ped.groupID)
        self._addTableActivitiesToPool(ped.groupID, activity.destination.groupID)
    
    def updateEventBasedActivities(self):
        if self._timeInfo.timeInd != self._nextEventTimeInd:
            return []
        
        if len(self._idleServers) == 0:
            # The first activities in the list will be picked up by the next server that
            # becomes available
            return [] 
    
        pedsLeavingTheirActivity = []
        # First assign idle peds to activities in their own neighborhood
        for ped in self._idleServers:
            # Because the _idleServers list is inherently sorted by idle time in descending order
            # the selected pedestrian is always the one whose been idle for the longest time 
            nextActivity = self._getActivityFromPool(ped, useCurrentTimeInd=True,
                                                     updateMaxQueueIndices=False)
            if nextActivity is not None:
                self._idleServers.remove(ped)
                self._assignedActivities[ped] = nextActivity
                pedsLeavingTheirActivity.append(ped)
        
        neigborhoodWithMaxQueue = self._updateMaxQueueIndices()
        
        while len(self._idleServers) > 0 and neigborhoodWithMaxQueue is not None:
            ped = self._idleServers[0]
            pooledActivity = neigborhoodWithMaxQueue.getFirstActivityFromPool(self._timeInfo,
                                                               useCurrentTimeInd=True)
            nextActivity = self._createActivity(pooledActivity, ped)
            neigborhoodWithMaxQueue = self._updateMaxQueueIndices()
            self._idleServers.remove(ped)
            self._assignedActivities[ped] = nextActivity
            pedsLeavingTheirActivity.append(ped)
            

        nextEventTimeIndices = {}
        for ped in self._idleServers:
            neighborhoods  = self._staff2neighborhoods[ped]
            for neighborhood in neighborhoods: 
                if neighborhood in nextEventTimeIndices:
                    continue
                firstActivityTimeInd = neighborhood.getFirstActivityTimeInd(self._timeInfo)
                if firstActivityTimeInd is not None:
                    nextEventTimeIndices[neighborhood] = firstActivityTimeInd
            
        self._updateNextEventTimeIndices(nextEventTimeIndices=nextEventTimeIndices.values()) 
        
        return pedsLeavingTheirActivity
    
    def _setNextTimeInd(self, ped=None):
        if ped is not None:
            minFirstActivityTimeInd = inf
            neighborhoods = self._staff2neighborhoods[ped]
            for neighborhood in neighborhoods:
                firstActivityTimeInd = neighborhood.getFirstActivityTimeInd(self._timeInfo)
                if firstActivityTimeInd is not None and firstActivityTimeInd < minFirstActivityTimeInd:
                    minFirstActivityTimeInd = firstActivityTimeInd
                    
            if minFirstActivityTimeInd != inf:
                self._updateNextEventTimeIndices(firstActivityTimeInd)
          
    def _updateNextEventTimeIndices(self, nextEventTimeInd=None, nextEventTimeIndices=None):
        if nextEventTimeIndices is not None:
            for timeIndex in nextEventTimeIndices:
                self._nextEventTimeIndices.add(timeIndex)
        if nextEventTimeInd is not None:
            self._nextEventTimeIndices.add(nextEventTimeInd)
        timeIndices2remove = []
        for timeInd in self._nextEventTimeIndices:
            if timeInd > self._timeInfo.timeInd:
                break
            # If the timeInd is equal or smaller than the current time index
            # remove it to keep the list clean
            timeIndices2remove.append(timeInd)
        
        for timeInd in timeIndices2remove:
            self._nextEventTimeIndices.remove(timeInd)
            
        if len(self._nextEventTimeIndices) == 0:
            self._nextEventTimeInd = max(self._timeInfo.getNextTimeInd(), min(min(self._maxQueueLengthIndices),
                                                                              min(self._maxQueueTimeIndices))) 
        else:
            self._nextEventTimeInd = max(self._timeInfo.getNextTimeInd(), min(min(self._nextEventTimeIndices),
                                                                              min(self._maxQueueLengthIndices),
                                                                              min(self._maxQueueTimeIndices))) 
        
    def _updateMaxQueueIndices(self):
        neigborhoodsWithQueues = []
        for ii in range(self._neighborhoodCount):
            neighborhood = self._neighborhoodIndex2neighborhood[ii]
            self._updateMaxQueueIndex(neighborhood, ii)            
            if self._maxQueueLengthIndices[ii] <= self._timeInfo.timeInd or \
                        self._maxQueueTimeIndices[ii] <= self._timeInfo.timeInd:
                gap = max(self._timeInfo.timeInd - self._maxQueueLengthIndices[ii], self._timeInfo.timeInd - self._maxQueueTimeIndices[ii])
                neigborhoodsWithQueues.append((neighborhood, gap))
        
        neigborhoodsWithQueues.sort(key=lambda el: el[1], reverse=True)
        
        if len(neigborhoodsWithQueues) > 0:
            return neigborhoodsWithQueues[0][0]
        else:
            return None
        
    def _updateMaxQueueIndex(self, neighborhood, neighborhoodIndex=None):   
        if neighborhoodIndex is None:
            neighborhoodIndex = self._neighborhood2Index[neighborhood]
         
        neighborhood.updateMaxQueueIndices(self._timeInfo)
        self._maxQueueLengthIndices[neighborhoodIndex] = neighborhood.maxQueueLengthInd
        self._maxQueueTimeIndices[neighborhoodIndex] = neighborhood.maxQueueTimeInd
    
    def _getActivityFromPool(self, ped, useCurrentTimeInd=False, updateMaxQueueIndices=True):         
        neighborhoods = self._staff2neighborhoods[ped]
            
        if len(neighborhoods) > 1:
            pooledActivity, neighborhood = Neighborhood.getFirstActivityFromMultiplePools(neighborhoods,
                                                                                          self._timeInfo,
                                                                                          useCurrentTimeInd)
        else:
            neighborhood = neighborhoods[0]
            pooledActivity = neighborhood.getFirstActivityFromPool(self._timeInfo,
                                                                   useCurrentTimeInd)
        
        if pooledActivity is None:
            return None
        
        if updateMaxQueueIndices:
            self._updateMaxQueueIndex(neighborhood)
                
        return self._createActivity(pooledActivity, ped)
        
    def _addActivitiesToPool(self, tableID, activities):
        neighborhood = self._table2neighborhood[tableID]
        
        neighborhood.addActivities2pool(activities)
        self._updateMaxQueueIndex(neighborhood)
        self._updateNextEventTimeIndices(self._timeInfo.timeInd + 1) # Ensure that if there are idle servers in the next step the ordering activity is picked up
        
    def _addTableActivitiesToPool(self, groupID, tableID):
        logger.debug(f'Adding activities for group {groupID} at table {tableID}')
        activities = []
        groupInfo = self._groups[groupID]
        nextTime = self._timeInfo.getNextTime()
        
        activities.append(PooledActivity(TAKE_ORDERS_ACTIVITY, nextTime,
                                           nextTime, TAKE_ORDERS_ACTIVITY_DURATION,
                                           tableID))
        
        checkOutActivityEndTime = groupInfo.endTime - WALKING_BUFFER - CHECK_OUT_ACTIVITY_DURATION
        checkOutActivityStartTime = checkOutActivityEndTime - CHECK_OUT_ACTIVITY_BUFFER
        activities.append(PooledActivity(CHECK_OUT_ACTIVITY, 
                                         checkOutActivityStartTime,
                                         checkOutActivityEndTime, 
                                         CHECK_OUT_ACTIVITY_DURATION, tableID))
        
        activities.append(PooledActivity(CLEAN_ACTIVITY, 
                                         groupInfo.endTime, groupInfo.endTime, 
                                         CLEAN_ACTIVITY_DURATION, tableID))
        
        firstSevingActivityStartTime = nextTime + TAKE_ORDERS_ACTIVITY_DURATION + SERVE_ACTIVITY_START_BUFFER
        lastServingActivityEndTime = checkOutActivityStartTime - SERVE_ACTIVITY_END_BUFFER - SERVE_ORDER_ACTIVITY_DURATION
        startTimeGap = (lastServingActivityEndTime - SERVE_ACTIVITY_BUFFER - firstSevingActivityStartTime)/SERVE_ACTIVITY_COUNT
        for ii in range(self._servingsCount):
            startTime = firstSevingActivityStartTime + startTimeGap*ii
            endTime = startTime + SERVE_ACTIVITY_BUFFER 
            activities.append(PooledActivity(SERVE_ACTIVITY,
                                             startTime, endTime,
                                             SERVE_ORDER_ACTIVITY_DURATION, tableID))
        
        self._addActivitiesToPool(tableID, activities)

    def _getDestination2DestinationDistances(self):
        destination2destinationDistances = {servingDestination.ID:{} for servingDestination in self._servingDestinations.values()}
        baseDestinationID = self._baseDestination.ID
        
        for servingDestination in self._servingDestinations.values():
            baseID = servingDestination.ID
            destination2destinationDistances[baseID][baseDestinationID] = getDistBetweenDestinations(servingDestination, self._baseDestination)
            for otherServingDestination in self._servingDestinations.values():
                if otherServingDestination.ID == baseID or otherServingDestination.ID in baseID:
                    continue
                dist = getDistBetweenDestinations(servingDestination, otherServingDestination)
                destination2destinationDistances[baseID][otherServingDestination.ID] = dist
                destination2destinationDistances[otherServingDestination.ID][baseID] = dist
        
        return destination2destinationDistances
        
    def _createActivity(self, pooledActivity, ped):
        if pooledActivity.activityType == SERVE_ACTIVITY:
            return self._createServingActivity(pooledActivity, ped)
        elif pooledActivity.activityType == TAKE_ORDERS_ACTIVITY:
            return self._createTakingOrdersActivity(pooledActivity)
        elif pooledActivity.activityType == CHECK_OUT_ACTIVITY:
            return self._createCheckOutActivity(pooledActivity)
        elif pooledActivity.activityType == CLEAN_ACTIVITY:
            return self._createCleaningActivity(pooledActivity)
        else:
            raise Exception(f'Unknown activity type "{pooledActivity.activityType}"')
        
    def _createServingActivity(self, pooledActivity, ped):
        servingActivity = FixedWaitingTimeActivity(self._createActivityID(pooledActivity.activityType, pooledActivity.tableID),
                                        self._servingDestinations[pooledActivity.tableID], 
                                        SERVING_ORDER_ACTIVITY_NM, SERVE_ORDER_ACTIVITY_DURATION)
        self._chainedActivities[ped] = [servingActivity]
        
        return FixedTimeOutsideOfSimulationActivity(self._createActivityID(pooledActivity.activityType, pooledActivity.tableID),
                                        self._baseDestination, COLLECTING_ORDER_ACTIVITY_NM, COLLECT_ORDER_ACTIVITY_DURATION)
        
    def _createTakingOrdersActivity(self, pooledActivity):
        return FixedWaitingTimeActivity(self._createActivityID(pooledActivity.activityType, pooledActivity.tableID),
                                        self._servingDestinations[pooledActivity.tableID], 
                                        TAKING_ORDER_ACTIVITY_NM, TAKE_ORDERS_ACTIVITY_DURATION)

    def _createCheckOutActivity(self, pooledActivity):
        return FixedWaitingTimeActivity(self._createActivityID(pooledActivity.activityType, pooledActivity.tableID),
                                        self._servingDestinations[pooledActivity.tableID], 
                                        CHECKING_OUT_ACTIVITY_NM, CHECK_OUT_ACTIVITY_DURATION)

    def _createCleaningActivity(self, pooledActivity):
        return FixedWaitingTimeActivity(self._createActivityID(pooledActivity.activityType, pooledActivity.tableID),
                                        self._servingDestinations[pooledActivity.tableID], 
                                        CLEANING_ACTIVITY_NM, CLEAN_ACTIVITY_DURATION)

        
    def _createActivityID(self, activityType, tableID):
        activityID = f'{activityType}_{tableID}_{self._activityIndCounter}'
        self._activityIndCounter += 1
        return activityID

# ================================================================================================
@dataclass
class Neighborhood():
    ID: str
    tables: list
    seatCount: int
    
    staff: list = field(default_factory=list)
    
    _activityPool: list = field(default_factory=list)
    _activityPoolByEndTime: list = field(default_factory=list)
    _activityPoolByStartTime: list = field(default_factory=list)
    _maxQueueLengthInd: int = 0
    _maxQueueTimeInd: int = 0
    
            
    def addActivities2pool(self, activities):
        self._activityPool += activities
        self._activityPool.sort(key=lambda pooledActivity: (pooledActivity.priorityClass, 
                                                            pooledActivity.endTime, 
                                                            pooledActivity.startTime))
        self._activityPoolByEndTime += activities
        self._activityPoolByEndTime.sort(key=lambda pooledActivity: pooledActivity.endTime)
        
        self._activityPoolByStartTime += activities
        self._activityPoolByStartTime.sort(key=lambda pooledActivity: pooledActivity.startTime)
        
        
    def getFirstActivityFromPool(self, timeInfo, useCurrentTimeInd, doRemove=True):
        selectedActivity = None
        
        if len(self._activityPool) == 0:
            return selectedActivity
        
        if useCurrentTimeInd:
            time = timeInfo.currentTime
        else:
            time = timeInfo.getNextTime()
        
        for activity in self._activityPool:
            if activity.startTime <= time:
                selectedActivity = activity
                break
            
        if selectedActivity is not None and doRemove:
            self.removeActivityFromPool(selectedActivity, timeInfo)
    
        return selectedActivity

    def removeActivityFromPool(self, activity, timeInfo):
        self._activityPool.remove(activity)
        self._activityPoolByEndTime.remove(activity)
        self._activityPoolByStartTime.remove(activity)
        self.updateMaxQueueIndices(timeInfo)

    def updateMaxQueueIndices(self, timeInfo):
        if len(self._activityPoolByEndTime) <= MAX_QUEUE_LENGTH:
            self._maxQueueLengthInd = inf
        else:
            self._maxQueueLengthInd = timeInfo.getTimeIndCeiled(self._activityPoolByEndTime[MAX_QUEUE_LENGTH].endTime)
        
        if len(self._activityPoolByEndTime) == 0:
            self._maxQueueTimeInd = inf
        else:
            self._maxQueueTimeInd = timeInfo.getTimeIndCeiled(self._activityPoolByEndTime[0].endTime + MAX_QUEUE_TIME) 
    
    def getFirstActivityTimeInd(self, timeInfo):
        if len(self._activityPool) == 0:
            return None
        
        return timeInfo.getTimeIndCeiled(self._activityPoolByStartTime[0].startTime)
    
    @property
    def maxQueueLengthInd(self):
        return self._maxQueueLengthInd

    @property
    def maxQueueTimeInd(self):
        return self._maxQueueTimeInd

    @staticmethod
    def getFirstActivityFromMultiplePools(neighborhoods, timeInfo, useCurrentTimeInd):
        selectedActivities = []
        selectedActivity2neighborhood = {}
        for neighborhood in neighborhoods:
            selectedActivity = neighborhood.getFirstActivityFromPool(timeInfo, useCurrentTimeInd,
                                                                     doRemove=False)
            if selectedActivity is not None:
                selectedActivities.append(selectedActivity)
                selectedActivity2neighborhood[selectedActivity] = neighborhood
    
        selectedActivity = None
        neighborhood = None
        if len(selectedActivities) > 0:
            selectedActivitiesSorted = sorted(selectedActivities, key=lambda pooledActivity: (pooledActivity.priorityClass, 
                                                            pooledActivity.endTime, 
                                                            pooledActivity.startTime))
            selectedActivity = selectedActivitiesSorted[0]
            neighborhood = selectedActivity2neighborhood[selectedActivity]
            neighborhood.removeActivityFromPool(selectedActivity, timeInfo)
        
        
        return selectedActivity, neighborhood
        
    def __hash__(self):
        return self.ID.__hash__()
        
    def __str__(self):
        return self.ID
        
# ================================================================================================
@dataclass(frozen=True)
class PooledActivity():
    activityType: str
    startTime: float
    endTime: float
    duration: float
    tableID: str
    priorityClass: int = BASE_PRIORITY

# ================================================================================================
# ================================================================================================
def _getGapsBetweenTableOccupancy(groupsAtTable):
    gapIndices = []
    for ii in range(len(groupsAtTable) - 1):
        if groupsAtTable[ii].earliestEndTime < groupsAtTable[ii+1].earliestStartTime: 
            gapIndices.append(ii)

    if groupsAtTable[-1].earliestEndTime < groupsAtTable[-1].tEndPeriod:
        gapIndices.append(len(groupsAtTable) - 1)

    return gapIndices

def getDistBetweenDestinations(dest_1, dest_2):
    return dest_1.distance(dest_2)
     
def getMeanVisitDuration(schedulerInput):
    if schedulerInput.visitDurationDistr.value is not None:
        return schedulerInput.visitDurationDistr.value
    else:
        return getAnalyticalMean(schedulerInput.visitDurationDistr.numpyDistr, 
                                 schedulerInput.visitDurationDistr.numpyDistrArgs)

def updateFreeDestination(freeIDs, occupied, curTime):
    IDs2pop = []
    for destinationID, endTime in occupied.items():
        if endTime < curTime:
            IDs2pop.append(destinationID)
            freeIDs.append(destinationID)
    for destinationID in IDs2pop:
        occupied.pop(destinationID)

def getDistrValue(distrValueInput):
    if distrValueInput.value is not None:
        return distrValueInput.value
    else:
        value = distrValueInput.numpyDistr(*distrValueInput.numpyDistrArgs)
        if distrValueInput.min is not None:
            value = max(distrValueInput.min, value)
        if distrValueInput.max is not None:
            value = min(distrValueInput.max, value)

        return value

def useToilet(schedulerInput):
    if schedulerInput.toiletDestinationIDs is None:
        return False

    if NOMAD.NOMAD_RNG.choice([0,1], p=[1-schedulerInput.toiletVisitProbability,schedulerInput.toiletVisitProbability]) == 1:
        return True
    else:
        return False

def createServingDestinations(tables2destinations, walkLevels):
    servingDestinations = {}
    for tableID, chairs in tables2destinations.items():
        # Get the base serving area which is an union of the buffered polygons of the table and chairs
        tableObstacle, walkLevel = getObstacle(tableID, walkLevels)
        servingAreaPolygons = [tableObstacle.buffer(SERVING_ACTIVITY_BUFFER)]
        for chair in chairs:
            servingAreaPolygons.append(chair.buffer(SERVING_ACTIVITY_BUFFER))
        servingArea = unary_union(servingAreaPolygons)
        
        # Crop the servingArea such that it is completely walkable by:
        # 1) Intersecting it with the walkable areas
        # 2) Cutting out the table and chairs
        for walkableArea in walkLevel.walkableAreas:
            servingArea = servingArea.intersection(walkableArea.geometry)
        
        servingArea = servingArea.difference(tableObstacle.geometry)
        for chair in chairs:
            servingArea = servingArea.difference(chair.geometry)

        servingDestination = createDestinationFromGeometry(getServingDestinationID(tableID), servingArea)
        walkLevel.addDestinations(servingDestination)
    
        servingDestinations[tableID] = servingDestination
    
    return servingDestinations

def getServingActivityID(tableID):
    return f'{tableID}_serving_act'

def getServingDestinationID(tableID):
    return f'{tableID}_serving_dest'

def sortDemandPattern(demandPattern):
    return sorted(demandPattern, key=lambda tup: (tup[0], tup[1], -tup[2]))

def getNeighborhoodID(ind):
    return f'neighborhood_{ind:02d}'

@dataclass
class GroupInfo():
    ID: str
    earliestStartTime: float
    earliestEndTime: float
    size: int
    tableID: str
    tStartPeriod: float
    tEndPeriod: float

    startTimeBoundary: float = None
    endTimeBoundary: float = None
    startTime: float = None
    endTime: float = None

