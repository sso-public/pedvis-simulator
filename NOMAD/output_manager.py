""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

from builtins import staticmethod
from collections import namedtuple
from datetime import datetime
import json
import logging
from pathlib import Path
import pickle
import struct

from shapely.geometry.multipolygon import MultiPolygon
import xlsxwriter

from NOMAD.activities import LineDestination, PolygonDestination, \
    MultiPolygonDestination
from NOMAD.activity_scheduler import TAKING_ORDER_ACTIVITY_NM, \
    COLLECTING_ORDER_ACTIVITY_NM, SERVING_ORDER_ACTIVITY_NM, \
    CHECKING_OUT_ACTIVITY_NM, CLEANING_ACTIVITY_NM
from NOMAD.demand_manager import LineSource, RectangleSource
from NOMAD.obstacles import LineObstacle, PolygonObstacle, CircleObstacle, \
    EllipseObstacle
import numpy as np


try:
    from scipy.io import savemat
except:
    pass

COMMENT_CHAR = '#'
COL_SEP_CHAR = ';'

TYPE = 'type'
INT_TYPE = 'int'
FLOAT_TYPE = 'float'
STR_TYPE = 'str'

TYPE_2_FCN = {
    INT_TYPE:int,
    FLOAT_TYPE:float,
    STR_TYPE:str
    }

COLUMN_NM = 'colNm'
PED_ATTR = 'pedAttr'
ACT_ATTR = 'actAttr'
ATTR_FCN = 'attrFcn'
CLASS_TYPE = 'classType'

PED_ID = 'ID'
TIME = 'time'

CLASS_TYPE_2_C_TYPE = {
    INT_TYPE:'I',
    FLOAT_TYPE:'d',
    }

def getXcoord(vector):
    return vector.x

def getYcoord(vector):
    return vector.y

class OutputManagerBase():

    def initialize(self, nomadModel):
        raise NotImplementedError('Implement')

    def finish(self):
        raise NotImplementedError('Implement')

    def addPeds(self, pedsAdded):
        raise NotImplementedError('Implement')

    def afterStarting(self, nomadModel):
        raise NotImplementedError('Implement')

    def afterTimeStep(self, nomadModel):
        raise NotImplementedError('Implement')

    def afterFinishing(self, nomadModel):
        raise NotImplementedError('Implement')

    @property
    def isDebug(self):
        return False
    
    @property
    def type(self):
        raise NotImplementedError('Implement')

class DummyOutputManager(OutputManagerBase):
    def initialize(self, _,):
        return

    def addPeds(self, _):
        return

    def finish(self):
        return

    def afterStarting(self, _):
        return

    def afterTimeStep(self, _):
        return

    def afterFinishing(self, _):
        return

    @property
    def type(self):
        return 'DummyOutputManager'

class InMemoryOutputManager(OutputManagerBase):

    def __init__(self, saveTimeStep=None):
        '''
        Constructor
        '''
        super().__init__()
        self.saveTimeStep = saveTimeStep
        self._notSavingEveryStep = False
        self._stepsTillNextSave = 0
        self._steps2skip = 0

        self.pedData = {}
        
    def initialize(self, nomadModel):
        if self.saveTimeStep is not None:
            ratioFactor = self.saveTimeStep/nomadModel.timeInfo.inIsolationTimeStep
            ratioFactorInt = round(ratioFactor)
            if abs(ratioFactor - ratioFactorInt) > 1e-12:
                raise Exception('The save time step in invalid. It should be a multiple of {}'.format(nomadModel.timeInfo.inIsolationTimeStep))

            if ratioFactorInt > 1:
                self._notSavingEveryStep = True
                self._stepsTillNextSave = 0
                self._steps2skip = ratioFactorInt - 1
                
        self.pedData = {}

    def finish(self):
        return

    def afterStarting(self, _):
        return
    
    def addPeds(self, pedsAdded):
        for ped in pedsAdded:
            self.pedData[ped.ID] = self.PedestrianData(ped)

    def afterTimeStep(self, nomadModel):
        for ped in nomadModel.pedsRemoved:
            self.pedData[ped.ID].activities = ped.activityLog
            self.pedData[ped.ID].trimArrays(ped.activityLog[-1].endTime)
            
        if self._notSavingEveryStep:
            if self._stepsTillNextSave > 0:
                self._stepsTillNextSave -= 1
                return

        # Do the actual saving
        time = nomadModel.timeInfo.getCurrentTime()
        for ped in nomadModel.movingPedestrians + nomadModel.newStaticNonInteractingPeds \
                    + nomadModel.newOutsideOfSimulationPeds + nomadModel.newStaticPeds:
            self.addPedTrajectoryData(ped, time)

        if self._notSavingEveryStep:
            self._stepsTillNextSave = self._steps2skip

    def addPedTrajectoryData(self, ped, time):
        self.pedData[ped.ID].addNextPositionAndVelocity(time, ped.pos_x, ped.pos_y,
                                                        ped.vel_x, ped.vel_y,
                                                        ped.velNorm_x, ped.velNorm_y)

    def afterFinishing(self, nomadModel):
        # Trim arrays and set end time of all peds still in the model
        for ped in nomadModel.pedestrians:
            self.pedData[ped.ID].activities = ped.activityLog
            self.pedData[ped.ID].trimArrays(ped.activityLog[-1].endTime)

    @property
    def type(self):
        return 'InMemoryOutputManager'

    class PedestrianData():

        INIT_ROW_COUNT = 1000

        def __init__(self, ped):
            self.groupID = ped.groupID
            self.startTime = ped.startTime
            self.endTime = None
            self.startPos = ped.spawningPos
            self.times = np.zeros(self.INIT_ROW_COUNT, dtype=float)
            self.positions = np.zeros((self.INIT_ROW_COUNT,2), dtype=float)
            self.velocities = np.zeros((self.INIT_ROW_COUNT,2), dtype=float)
            self.normalizedVelocities = np.zeros((self.INIT_ROW_COUNT,2), dtype=float)
            self.activities = []
            self._rowInd = 0
            self._rowCount = self.INIT_ROW_COUNT

        def addNextPositionAndVelocity(self, time, pos_x, pos_y, vel_x, vel_y, velNorm_x, velNorm_y):
            if self._rowInd >= self._rowCount:
                self.expandArrays()

            self.times[self._rowInd] = time
            self.positions[self._rowInd,:] = (pos_x, pos_y)
            self.velocities[self._rowInd,:] = (vel_x, vel_y)
            self.normalizedVelocities[self._rowInd,:] = (velNorm_x, velNorm_y)

            self._rowInd += 1

        def addStartActivity(self, time, activityType, position):
            pass

        def addEndActivity(self, time):
            pass

        def trimArrays(self, endTime):
            trimIndices = np.argwhere(self.times[1:] == 0)
            if len(trimIndices) == 0:
                return
            trimInd = trimIndices[0][0] + 1
            self.times = self.times[:trimInd]
            self.positions = self.positions[:trimInd]
            self.velocities = self.velocities[:trimInd]
            self.normalizedVelocities = self.normalizedVelocities[:trimInd]
            self.endTime = endTime

        def expandArrays(self):
            newRowCount = self._rowCount + self.INIT_ROW_COUNT

            times = np.zeros(newRowCount, dtype=float)
            positions = np.zeros((newRowCount,2), dtype=float)
            velocities = np.zeros((newRowCount,2), dtype=float)
            normalizedVelocities = np.zeros((newRowCount,2), dtype=float)

            times[:self._rowCount] = self.times
            positions[:self._rowCount,:] = self.positions[:,:]
            velocities[:self._rowCount,:] = self.velocities[:,:]
            normalizedVelocities[:self._rowCount,:] = self.normalizedVelocities[:,:]

            self.times = times
            self.positions = positions
            self.velocities = velocities
            self.normalizedVelocities = normalizedVelocities

            self._rowCount = newRowCount

    class PedestrianAcitivityData():

        def __init__(self):
            self.startTime = None
            self.endTime = None
            self.activityType = None

class BasicFileOutputManager(OutputManagerBase):
    '''
    classdocs
    '''
    BUFFER_SIZE = 2**23 # bytes 2^23 bytes = 8 MB
    TRAJ_EXTENSION = 'traj'
    DESC_EXTENSION = 'desc'
    SCEN_EXTENSION = 'scen'
    LRCM_EXTENSION = 'lrcm'

    PED_DESC_PATTERN = (
        {COLUMN_NM:'ID', PED_ATTR:'ID', TYPE:INT_TYPE},
        {COLUMN_NM:'startTime', PED_ATTR:'startTime', TYPE:FLOAT_TYPE},
        {COLUMN_NM:'endTime', PED_ATTR:'endTime', TYPE:FLOAT_TYPE},
        {COLUMN_NM:'radius', PED_ATTR:'radius', TYPE:FLOAT_TYPE},
        {COLUMN_NM:'groupID', PED_ATTR:'groupID', TYPE:STR_TYPE},
        {COLUMN_NM:'activityPatternID', PED_ATTR:'activityPattern.ID', TYPE:STR_TYPE},
        )

    ACTIVITY_LOG_PATTERN = (
        {COLUMN_NM:'startTime', ACT_ATTR:'startTime', TYPE:FLOAT_TYPE},
        {COLUMN_NM:'endTime', ACT_ATTR:'endTime', TYPE:FLOAT_TYPE},
        {COLUMN_NM:'activityType', ACT_ATTR:'activityType', TYPE:STR_TYPE},
        {COLUMN_NM:'activityID', ACT_ATTR:'activityID', TYPE:STR_TYPE},
        {COLUMN_NM:'destinationID', ACT_ATTR:'destinationID', TYPE:STR_TYPE},
        {COLUMN_NM:'pedState', ACT_ATTR:'pedState', TYPE:INT_TYPE},
        )

    PED_TRAJ_PATTERN = (
        {COLUMN_NM:PED_ID, CLASS_TYPE:INT_TYPE, PED_ATTR:'ID'},
        {COLUMN_NM:'pos_x', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'pos', ATTR_FCN:getXcoord},
        {COLUMN_NM:'pos_y', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'pos', ATTR_FCN:getYcoord},
        {COLUMN_NM:'vel_x', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'vel', ATTR_FCN:getXcoord},
        {COLUMN_NM:'vel_y', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'vel', ATTR_FCN:getYcoord},
        {COLUMN_NM:'velNorm_x', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'velNorm', ATTR_FCN:getXcoord},
        {COLUMN_NM:'velNorm_y', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'velNorm', ATTR_FCN:getYcoord},
        {COLUMN_NM:'acc_x', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'acc', ATTR_FCN:getXcoord},
        {COLUMN_NM:'acc_y', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'acc', ATTR_FCN:getYcoord},
        {COLUMN_NM:'pathForce_x', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'pathFollowingForce', ATTR_FCN:getXcoord},
        {COLUMN_NM:'pathForce_y', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'pathFollowingForce', ATTR_FCN:getYcoord},
        {COLUMN_NM:'pedForce_x', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'pedForce', ATTR_FCN:getXcoord},
        {COLUMN_NM:'pedForce_y', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'pedForce', ATTR_FCN:getYcoord},
        {COLUMN_NM:'obsForce_x', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'obsForce', ATTR_FCN:getXcoord},
        {COLUMN_NM:'obsForce_y', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'obsForce', ATTR_FCN:getYcoord},
        {COLUMN_NM:'pedIsolationState', CLASS_TYPE:INT_TYPE, PED_ATTR:'pedIsolationState'},
        {COLUMN_NM:'pedIsolationTime', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'pedIsolationTime'},
        {COLUMN_NM:'obsIsolationState', CLASS_TYPE:INT_TYPE, PED_ATTR:'obsIsolationState'},
        {COLUMN_NM:'obsIsolationTime', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'obsIsolationTime'}
        )

    PED_TRAJ_BIN_PATTERN = CLASS_TYPE_2_C_TYPE[FLOAT_TYPE] + ''.join(CLASS_TYPE_2_C_TYPE[entry[CLASS_TYPE]] for entry in PED_TRAJ_PATTERN)

    logger = logging.getLogger(__name__)

    def __init__(self, outputPd, baseFilename, bufferSize=BUFFER_SIZE):
        '''
        Constructor
        '''
        super().__init__()
        self.outputPd = outputPd
        self.baseFilename = baseFilename
        self.bufferSize = bufferSize
        self.includePedData = True
        self.descriptionFilename = Path('none')
        self.trajectoryFilename = Path('none')

    def createFiles(self):
        if self.includePedData:
            self.descriptionFile, self.descriptionFilename = BasicFileOutputManager.createFile(self.outputPd, self.baseFilename, self.DESC_EXTENSION, 't')
        self.scenarioFile, self.scenarioFilename = BasicFileOutputManager.createFile(self.outputPd, self.baseFilename, self.SCEN_EXTENSION, 't')
        if self.includePedData:
            self.trajectoryFile, self.trajectoryFilename = BasicFileOutputManager.createFile(self.outputPd, self.baseFilename, self.TRAJ_EXTENSION, 'b', self.bufferSize)
        self.lrcmFile, self.lrcmFilename = BasicFileOutputManager.createFile(self.outputPd, self.baseFilename, self.LRCM_EXTENSION, 'b')

    def initialize(self, _, includePedData=True):
        self.includePedData = includePedData

        self.createFiles()
        self.stepsSavedToTrajectoryFile = 0
        if self.includePedData:
            descHeaderStr = self.getDescFileHeaderString()
            self.descriptionFile.write(descHeaderStr)

    @staticmethod
    def getDescFileHeaderString():
        descFirstHeaderStr = COMMENT_CHAR + ' '

        descSecondHeaderStr = COMMENT_CHAR + ' '
        for columnInfo in BasicFileOutputManager.PED_DESC_PATTERN:
            descSecondHeaderStr += (columnInfo[COLUMN_NM] + COL_SEP_CHAR)
            descFirstHeaderStr += ' '*(len(columnInfo[COLUMN_NM]) + 1)

        baseStr = 'activity'
        appendixList = ['1', '2', '3', '4', 'n']
        for appendix in ['1', '2', '3', '4', 'n']:
            if appendix == appendixList[-1]:
                descFirstHeaderStr += ' ... '
                descSecondHeaderStr += ' ... '

            firstHeaderStrPart = '{}_{}'.format(baseStr, appendix)
            descFirstHeaderStr += firstHeaderStrPart
            secondHeaderStrPart = ''
            for columnInfo in BasicFileOutputManager.ACTIVITY_LOG_PATTERN:
                secondHeaderStrPart += (columnInfo[COLUMN_NM] + COL_SEP_CHAR)

            descSecondHeaderStr += secondHeaderStrPart
            descFirstHeaderStr += ' '*(len(secondHeaderStrPart) - len(firstHeaderStrPart))

        return descFirstHeaderStr + '\n' + descSecondHeaderStr + '\n'

    def finish(self):
        self.logger.debug('Closing data output files')
        if not self.includePedData:
            return

        self.descriptionFile.write(COMMENT_CHAR + 'Total step count: {}'.format(self.stepsSavedToTrajectoryFile))
        self.descriptionFile.flush()
        self.descriptionFile.close()

        self.trajectoryFile.flush()
        self.trajectoryFile.close()

    def addPeds(self, _):
        return

    def afterStarting(self, nomadModel):
        fillScenFile(self.scenarioFile, nomadModel)
        self.scenarioFile.close()
        fillLrcmFile(self.lrcmFile, nomadModel)
        self.lrcmFile.close()

    def afterTimeStep(self, nomadModel):
        for ped in nomadModel.pedsRemoved:
            self.addPed2DescFile(ped)

        for ped in nomadModel.movingPedestrians + nomadModel.newStaticNonInteractingPeds \
                    + nomadModel.newOutsideOfSimulationPeds + nomadModel.newStaticPeds:
            self.addPedTrajectoryData(ped, nomadModel.timeInfo.currentTime)

    def afterFinishing(self, nomadModel):
        for ped in nomadModel.pedestrians:
            self.addPed2DescFile(ped)

    def addPed2DescFile(self, ped):
        pedDescStr = ''
        for columnInfo in self.PED_DESC_PATTERN:
            pedDescStr += (str(getAttr(ped, columnInfo[PED_ATTR])) + COL_SEP_CHAR)

        for activityLogElement in ped.activityLog:
            for columnInfo in self.ACTIVITY_LOG_PATTERN:
                pedDescStr += (str(getAttr(activityLogElement, columnInfo[ACT_ATTR])) + COL_SEP_CHAR)

        pedDescStr += '\n'
        self.descriptionFile.write(pedDescStr)

    def addPedTrajectoryData(self, ped, time):
        pedTrajData = [time, ]
        for columnInfo in self.PED_TRAJ_PATTERN:
            attrValue = getattr(ped, columnInfo[PED_ATTR])
            if ATTR_FCN in columnInfo:
                if attrValue is not None:
                    attrValue = columnInfo[ATTR_FCN](attrValue)
                else:
                    attrValue = 0
            pedTrajData.append(attrValue)
        self.trajectoryFile.write(struct.pack(self.PED_TRAJ_BIN_PATTERN, *pedTrajData))
        self.stepsSavedToTrajectoryFile += 1

    @staticmethod
    def readLrcmDataFile(lrcmFlNm):
        lrcmData = {}
        with open(lrcmFlNm, "rb") as f:
            walkLevelCnt = pickle.load(f)
            for _ in range(walkLevelCnt):
                walkLevelID = pickle.load(f)
                lrcmData[walkLevelID] = {}
                destinationCnt = pickle.load(f)
                for _ in range(destinationCnt):
                    destinationID = pickle.load(f)

                    cellCnt = pickle.load(f)
                    x = np.zeros((1,cellCnt)).flatten()
                    y = np.zeros((1,cellCnt)).flatten()
                    u = np.zeros((1,cellCnt)).flatten()
                    v = np.zeros((1,cellCnt)).flatten()
                    for ii in range(cellCnt):
                        cellData = pickle.load(f)
                        x[ii] = cellData[0][0]
                        y[ii] = cellData[0][1]
                        u[ii] = cellData[1][0]
                        v[ii] = cellData[1][1]

                    lrcmData[walkLevelID][destinationID] = {'x':x, 'y':y, 'u':u, 'v':v}

        return lrcmData

    @staticmethod
    def readTrajactoryDataFile(flNm):
        with open(flNm, "rb") as f:
            dataRead = f.read()

        dataUnpackedIter = struct.iter_unpack(BasicFileOutputManager.PED_TRAJ_BIN_PATTERN, dataRead)
        trajData = {}
        for dataUnpackedIterEntry in dataUnpackedIter:
            time = dataUnpackedIterEntry[0]
            lineData = {}
            for ii in range(len(BasicFileOutputManager.PED_TRAJ_PATTERN)):
                lineData[BasicFileOutputManager.PED_TRAJ_PATTERN[ii][COLUMN_NM]] = dataUnpackedIterEntry[ii+1]
            pedID = lineData[PED_ID]
            if pedID not in trajData:
                trajData[pedID] = {columInfo[COLUMN_NM]:[] for columInfo in BasicFileOutputManager.PED_TRAJ_PATTERN if columInfo[COLUMN_NM] is not PED_ID}
                trajData[pedID][TIME] = []
            trajData[pedID][TIME].append(time)
            for nm, value in lineData.items():
                if nm == PED_ID:
                    continue
                trajData[pedID][nm].append(value)

        return trajData

    @staticmethod
    def readDescriptionDataFile(flNm):
        pedestrians = {}
        with open(flNm) as f:
            line = f.readline()
            while line != '':  # The EOF char is an empty string
                if line[0] == COMMENT_CHAR:
                    line = f.readline()
                    continue

                pedInfo = BasicFileOutputManager.readDescriptionDataFileLine(line)
                pedestrians[pedInfo.ID] = pedInfo
                line = f.readline()

        return pedestrians

    @staticmethod
    def readDescriptionDataFileLine(line):
        fields = line.split(COL_SEP_CHAR)
        if fields[-1] == '\n':
            fields = fields[:-1]
        pedInfo = {}
        fieldInd = 0
        for columnInfo in BasicFileOutputManager.PED_DESC_PATTERN:
            if fields[fieldInd] == 'None':
                pedInfo[columnInfo[COLUMN_NM]] = None
            else:
                pedInfo[columnInfo[COLUMN_NM]] = TYPE_2_FCN[columnInfo[TYPE]](fields[fieldInd])
            fieldInd += 1

        activityLog = []
        while fieldInd < len(fields):
            activityEl = {}
            for columnInfo in BasicFileOutputManager.ACTIVITY_LOG_PATTERN:
                if fields[fieldInd] == 'None':
                    activityEl[columnInfo[COLUMN_NM]] = None
                else:
                    activityEl[columnInfo[COLUMN_NM]] = TYPE_2_FCN[columnInfo[TYPE]](fields[fieldInd])
                fieldInd += 1
            activityLog.append(namedtuple('activity', sorted(activityEl))(**activityEl))

        pedInfo['activityLog'] = activityLog

        return namedtuple('pedInfo', sorted(pedInfo))(**pedInfo)

    @staticmethod
    def readScenarioDataFile(flNm):
        with open(flNm) as f:
            scenDataDict = json.load(f)

        return getNamedTupleIter(scenDataDict, 'scenData')

    @staticmethod
    def exportTrajDataToExcel(dataFlNm, xlsxFlNm):
        trajData = BasicFileOutputManager.readTrajactoryDataFile(dataFlNm)
        workbook = xlsxwriter.Workbook(xlsxFlNm)

        columns = [TIME] + [columInfo[COLUMN_NM] for columInfo in BasicFileOutputManager.PED_TRAJ_PATTERN if columInfo[COLUMN_NM] is not PED_ID]

        for pedID, pedTrajData in trajData.items():
            worksheet = workbook.add_worksheet('Ped {}'.format(pedID))
            col = 0
            for columnNm in columns:
                row = 0
                worksheet.write(row, col, columnNm)
                row += 1
                for value in pedTrajData[columnNm]:
                    worksheet.write(row, col, value)
                    row += 1
                col += 1

        workbook.close()

    ACTIVITY_SHEETS = [TAKING_ORDER_ACTIVITY_NM, COLLECTING_ORDER_ACTIVITY_NM, SERVING_ORDER_ACTIVITY_NM, CHECKING_OUT_ACTIVITY_NM, CLEANING_ACTIVITY_NM]
    
    @staticmethod
    def exportDescriptionDataToExcel(dataFlNm, xlsxFlNm):
        peds = BasicFileOutputManager.readDescriptionDataFile(dataFlNm)
        workbook = xlsxwriter.Workbook(xlsxFlNm)

        columns = [columInfo[COLUMN_NM] for columInfo in BasicFileOutputManager.PED_DESC_PATTERN] + [columInfo[COLUMN_NM] for columInfo in BasicFileOutputManager.ACTIVITY_LOG_PATTERN]
        worksheet = workbook.add_worksheet('Desc. data')
        row = 0
        for ped in peds.values():
            col = 0
            for columnNm in columns:
                worksheet.write(row, col, columnNm)
                col += 1
            row += 1
            
            col = 0  
            for columInfo in BasicFileOutputManager.PED_DESC_PATTERN:
                value = getattr(ped, columInfo[COLUMN_NM])
                BasicFileOutputManager.writeValue2worksheet(worksheet, row, col, value, 
                                                            providedType=columInfo[TYPE], valueNm=columInfo[COLUMN_NM])
                col += 1
            
            for activityEl in ped.activityLog:
                col = len(BasicFileOutputManager.PED_DESC_PATTERN)
                for columInfo in BasicFileOutputManager.ACTIVITY_LOG_PATTERN:
                    value = getattr(activityEl, columInfo[COLUMN_NM])
                    BasicFileOutputManager.writeValue2worksheet(worksheet, row, col, value, 
                                                                providedType=columInfo[TYPE], valueNm=columInfo[COLUMN_NM])
                    col += 1
                row += 1
            
            row += 1 # Empty row between peds
      
        columns = ['ID']  + [columInfo[COLUMN_NM] for columInfo in BasicFileOutputManager.ACTIVITY_LOG_PATTERN if columInfo[COLUMN_NM] != 'activityType'] 
        for activityType in BasicFileOutputManager.ACTIVITY_SHEETS:
            worksheet = workbook.add_worksheet(activityType)
            worksheet.write(0, 0, activityType)
            col = 0
            row = 1
            for columnNm in columns:
                worksheet.write(row, col, columnNm)
                col += 1
            
            row = 2
            col = 0
            for ped in peds.values():
                for activityEl in ped.activityLog:
                    if activityEl.activityType == activityType:
                        col = 1
                        worksheet.write(row, 0, ped.ID)
                        for columInfo in BasicFileOutputManager.ACTIVITY_LOG_PATTERN:
                            if columInfo[COLUMN_NM] == 'activityType':
                                continue
                            value = getattr(activityEl, columInfo[COLUMN_NM])
                            BasicFileOutputManager.writeValue2worksheet(worksheet, row, col, value, 
                                                                providedType=columInfo[TYPE], valueNm=columInfo[COLUMN_NM]) 
                            col += 1
                        row += 1
        
        workbook.close()

    @staticmethod
    def writeValue2worksheet(worksheet, row, col, value, providedType=None, valueNm=None):
        if value is None:
            worksheet.write_string(row, col, 'None')
        elif valueNm == 'ID':
            worksheet.write_string(row, col, str(value))
        elif providedType in (FLOAT_TYPE, INT_TYPE):
            worksheet.write_number(row, col, value)
        elif providedType == STR_TYPE:
            worksheet.write_string(row, col, value)
        else:
            worksheet.write(row, col, value)
    
    @staticmethod
    def getString(value):
        return str(value)

    @staticmethod
    def createFile(outputPd, filename, extension, fileType, buffer=-1):
        fullFilename = BasicFileOutputManager.createFilename(outputPd, filename, extension)
        return open(fullFilename, 'w'+fileType, buffering=buffer), fullFilename


    @staticmethod
    def createFilename(outputPd, filename, extension):
        outputPd = Path(outputPd)
        timeStampStr = datetime.strftime(datetime.now(), '%Y%m%d%H%M%S')
        return outputPd.joinpath(filename + '_' + timeStampStr + '.' + extension)

    @property
    def type(self):
        return 'BasicFileOutputManager'

NOMAD_MODEL_ATTR = ('name', 'label')

RCMD_INIT_TYPE = 'init'
RCMD_FINAL_TYPE = 'final'
CENTER_COORDS_TYPE = 'centerCoords'

class DebugOutputManager(BasicFileOutputManager):
    
    RCMD_EXTENSION = 'npz' # Numpy 
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.costMatrixFilename = Path('none')
        self.costMatrices2save = {}
    
    def initialize(self, *args, **kwargs):
        super().initialize(*args, **kwargs)
        if len(self.costMatrices2save) > 0:
            self.saveMatrices2file()
    
    def createFiles(self):
        super().createFiles()
        self.costMatrixFilename = BasicFileOutputManager.createFilename(self.outputPd, self.baseFilename, self.RCMD_EXTENSION)
    
    def addCostMatrix2save(self, costMatrix, destinationID, rcmdType):
        self.costMatrices2save[DebugOutputManager.getName(destinationID, rcmdType)] = costMatrix
    
    def addCenterCoordsMatrix(self, centerCoordsMatrix):
        self.costMatrices2save[CENTER_COORDS_TYPE] = centerCoordsMatrix
            
    def saveMatrices2file(self):
        np.savez(self.costMatrixFilename, **self.costMatrices2save)
        
    @staticmethod
    def readRcmdDataFile(rcmdFlNm):
        matricesLoaded =  np.load(rcmdFlNm)
        matrices = {RCMD_INIT_TYPE:{}, RCMD_FINAL_TYPE:{}}
        for name, matrix in matricesLoaded.items():
            if name == CENTER_COORDS_TYPE:
                centerCoords = matrix
            elif name.startswith(RCMD_INIT_TYPE):
                matrices[RCMD_INIT_TYPE][DebugOutputManager.stripType(name, RCMD_INIT_TYPE)] = matrix
            elif name.startswith(RCMD_FINAL_TYPE):
                matrices[RCMD_FINAL_TYPE][DebugOutputManager.stripType(name, RCMD_FINAL_TYPE)] = matrix
            else:
                raise Exception(f'Unknown rcmdType for name: {name}')        
    
        return matrices, centerCoords
    
    @staticmethod
    def getName(destinationID, rcmdType):
        if rcmdType == RCMD_INIT_TYPE:
            return f'{RCMD_INIT_TYPE}_{destinationID}'
        elif rcmdType == RCMD_FINAL_TYPE:
            return f'{RCMD_FINAL_TYPE}_{destinationID}'
        else:
            raise Exception(f'Unknown rcmdType: {rcmdType}') 
            
    @staticmethod
    def stripType(name, rcmdType):
        return name.replace(f'{rcmdType}_', '')

    @property
    def isDebug(self):
        return True
            
    @property
    def type(self):
        return 'DebugOutputManager'
            
def getAttr(baseObject, attrNm):
    if '.' in attrNm:
        attrNmBase, attrNmNext = attrNm.split('.', 1)
        return getAttr(getattr(baseObject, attrNmBase), attrNmNext)
    else:
        return getattr(baseObject, attrNm)


def fillLrcmFile(file, nomadModel):
    pickle.dump(len(nomadModel.walkLevels), file)
    for walkLevel in nomadModel.walkLevels:
        pickle.dump(walkLevel.ID, file)
        pickle.dump(len(walkLevel.destinations), file)
        for destination in walkLevel.destinations:
            pickle.dump(destination.ID, file)
            pickle.dump(len(destination._localRouteChoiceManager.centerCoords), file)
            centerCoords = destination._localRouteChoiceManager.centerCoords
            for key, desDir in destination._localRouteChoiceManager.desiredDirections.items():
                centerCoord = centerCoords[key]
                pickle.dump([centerCoord, desDir], file)


def fillScenFile(scenFile, nomadModel):
    # Name, Label, Duration, timeStep, timeStepCount
    scenDict = {}
    for attrNm in NOMAD_MODEL_ATTR:
        scenDict[attrNm] = getattr(nomadModel, attrNm)
    scenDict['timeStep'] = nomadModel.timeInfo.inIsolationTimeStep
    scenDict['duration'] = nomadModel.timeInfo.duration
    scenDict['endTimeInd'] = nomadModel.timeInfo.endTimeInd
    # Other output filenames (without pd)

    scenDict['descFile'] = nomadModel.outputManager.descriptionFilename.name
    scenDict['trajFile'] = nomadModel.outputManager.trajectoryFilename.name
    scenDict['lrcmFile'] = nomadModel.outputManager.lrcmFilename.name
    
    if hasattr(nomadModel.outputManager, 'costMatrixFilename'):
        scenDict['rcmdFile'] = nomadModel.outputManager.costMatrixFilename.name

    # Geometry
    scenDict['geometry'] = getGeometryDict(nomadModel)

    json.dump(scenDict, scenFile, indent=4)

def getGeometryDict(nomadModel):
    geomDict = {}
    walkLevels = {}
    extent = [0,0,0,0]
    for walkLevel in nomadModel.walkLevels:
        walkLevels[walkLevel.ID] = getWalkLevelDict(walkLevel)
        extent = [min(walkLevel.origin[0], extent[0]),
                  min(walkLevel.origin[1], extent[1]),
                  max(walkLevel.extent[0] + walkLevel.origin[0], extent[2]),
                  max(walkLevel.extent[1] + walkLevel.origin[1], extent[3])]

    geomDict['walkLevels'] = walkLevels
    geomDict['extent'] = extent
    return geomDict

def getWalkLevelDict(walkLevel):
    walkableAreas = {}
    for walkableArea in walkLevel.walkableAreas:
        walkableAreas[walkableArea.ID] = getWalkableAreaCoords(walkableArea)
    obstacles = {}
    for obstacle in walkLevel.obstacles:
        obstacles[obstacle.ID] = getObstacleDict(obstacle)
    destinations = {}
    for destination in walkLevel.destinations:
        destinations[destination.ID] = getDestinationDict(destination)
    sources = {}
    for source in walkLevel.sources:
        sources[source.ID] = getSourceDict(source)

    return {'walkableAreas': walkableAreas, 'obstacles':obstacles,
            'destinations':destinations, 'sources':sources}

def getObstacleDict(obstacle):
    if isinstance(obstacle, LineObstacle):
        return {'type':'line', 'coords':getLineCoordsXY(obstacle.geometry.coords)}
    elif isinstance(obstacle, PolygonObstacle):
        return {'type':'polygon', 'coords':list(obstacle.geometry.exterior.coords[:-1])}
    elif isinstance(obstacle, CircleObstacle):
        return {'type':'circle', 'centerCoord':obstacle.centerCoord, 'radius':obstacle.radius}
    elif isinstance(obstacle, EllipseObstacle):
        return {'type':'ellipse', 'centerCoord':obstacle.centerCoord, 'semiAxesValues':obstacle.semiAxesValues, 'rotation':obstacle.rotation}

def getDestinationDict(destination):
    if isinstance(destination, LineDestination):
        return {'type':'line', 'coords':getLineCoordsXY(destination.geometry.coords)}
    elif isinstance(destination, PolygonDestination):
        return {'type':'polygon', 'coords':list(destination.geometry.exterior.coords[:-1])}
    elif isinstance(destination, MultiPolygonDestination):
        return {'type':'multiPolygon', 'coords':[list(polygon.exterior.coords[:-1]) for polygon in destination.geometry]}
    
def getSourceDict(source):
    if isinstance(source, LineSource):
        return {'type':'line', 'coords':getLineCoordsXY(source.geometry.coords)}
    elif isinstance(source, RectangleSource):
        return {'type':'polygon', 'coords':list(source.geometry.exterior.coords[:-1])}

def getWalkableAreaCoords(walkableArea):
    coords = []
    if isinstance(walkableArea.geometry, MultiPolygon):
        for pol in walkableArea.geometry:
            coords.append(list(pol.exterior.coords[:-1]))
    else:
        coords.append(list(walkableArea.geometry.exterior.coords[:-1]))

    return coords

def getNamedTupleIter(dataDict, name):
    for key, value in dataDict.items():
            if isinstance(value, dict):
                dataDict[key] = getNamedTupleIter(value, key)

    return namedtuple(name, sorted(dataDict))(**dataDict)

def getLineCoordsXY(coords):
    return [[coords[0][0], coords[1][0]], [coords[0][1], coords[1][1]]]


def exportTrajectoriesToMat(scenFlNm):
    scenDataFilePath = Path(scenFlNm)
    matFlNm = scenDataFilePath.parent.joinpath(scenDataFilePath.stem + '.mat')
    scenData = BasicFileOutputManager.readScenarioDataFile(scenDataFilePath)
    trajDataFilePath = scenDataFilePath.parent.joinpath(scenData.trajFile)
    trajData = BasicFileOutputManager.readTrajactoryDataFile(trajDataFilePath)


    contentDict = {}
    for ID, pedTrajData in trajData.items():
        time = pedTrajData['time']
        contentMatrix = np.zeros((len(time),5), dtype=float)
        contentMatrix[:,0] = time
        contentMatrix[:,1] = pedTrajData['pos_x']
        contentMatrix[:,2] = pedTrajData['pos_y']
        contentMatrix[:,3] = pedTrajData['vel_x']
        contentMatrix[:,4] = pedTrajData['vel_y']

        contentDict['ped_{}'.format(ID)] = contentMatrix

    savemat(matFlNm, contentDict, format='5')


