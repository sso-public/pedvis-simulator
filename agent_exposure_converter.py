import csv
import json
import sys
import time


def convertAgentExposures(modelFileName, inputFileName, outputFileName):
    success = True
    
    try:
        with open(modelFileName, "r") as modelFile, open(inputFileName, "r", newline="") as inputFile, open(outputFileName, "w", newline="") as outputFile:
            model = json.loads(modelFile.read())
            
            # This is a direct port of the functionality implemented in state.php.
            positions = {}
            
            for agent in model["agents"]:
                name = agent["name"]
                
                positions[name] = {}
                
                hasLeave = False
                
                # Note that some of the action types are skipped because no movement occurs.
                for tickText in agent["script"]:
                    action = agent["script"][tickText]
                    
                    # Note that ticks are strings.
                    tick = int(tickText)
                    
                    if action["type"] == "enter":
                        previousTick = tick
                        
                        position = {
                            "x": action["x"],
                            "y": action["y"]
                        }
                        
                        positions[name][tick] = position.copy()
                    elif action["type"] == "move":
                        # Make sure the array is contiguous to faciliate easy look-ups of positions.
                        for inbetweenTick in range(previousTick + 1, tick):
                            positions[name][inbetweenTick] = position.copy()
                        
                        previousTick = tick
                        
                        position["x"] += action["x"]
                        position["y"] += action["y"]
                        
                        positions[name][tick] = position.copy()
                    
                    elif action["type"] == "leave":
                        # Make sure the array is contiguous to faciliate easy look-ups of positions.
                        for inbetweenTick in range(previousTick + 1, tick):
                            positions[name][inbetweenTick] = position.copy()
                        
                        hasLeave = True
                    
                # The list of positions may not be complete if an agent has not left the model.
                if not hasLeave:
                    # Make sure the array is contiguous to faciliate easy look-ups of positions.
                    for inbetweenTick in range(previousTick + 1, model["ticks"]):
                        positions[name][inbetweenTick] = position.copy()
            
            reader = csv.DictReader(inputFile)
            
            fieldnames = list(reader.fieldnames)
            fieldnames.insert(2, "X")
            fieldnames.insert(3, "Y")
            
            writer = csv.DictWriter(outputFile, fieldnames=fieldnames)
            
            writer.writeheader()
            
            for row in reader:
                name = row["Agent"]
                tick = int(row["Tick"])
                
                position = positions[name][tick]
                
                row["X"] = str(position["x"])
                row["Y"] = str(position["y"])
                
                writer.writerow(row)
    except Exception as exception:
        print(exception)
        
        success = False
    
    return success


if __name__ == "__main__":
    print("Converting exposures...")
    
    startTime = time.time()
    
    success = False
    
    if len(sys.argv) > 3:
        success = convertAgentExposures(sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        print("Please specify exactly three command-line arguments: the path of the model JSON file, the path of the agent exposure CSV file, and the path of the file to write the conversion to.")
    
    stopTime = time.time()
    
    if success:
        print("Exposures converted in {:4.3f} seconds.".format(stopTime - startTime))
    else:
        print("Exposures not converted.")
